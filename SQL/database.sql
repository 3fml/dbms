
-- #################################################################################################
-- ## Creazione dello schema e degli enum                                                         ##
-- #################################################################################################

-- Cancella ogni eventuale istanza pre-esistente della base di dati
--DROP DATABASE IF EXISTS FML;
DROP SCHEMA IF EXISTS FML CASCADE;
DROP TYPE IF EXISTS gender;
DROP TYPE IF EXISTS UserStatus;
DROP TYPE IF EXISTS Color;
DROP TYPE IF EXISTS Priority;
DROP TYPE IF EXISTS ResidencyDomicile;
DROP TYPE IF EXISTS RelativeOfType;
DROP TYPE IF EXISTS MeasurementsDescriptor;
DROP TYPE IF EXISTS AdditionalServices;
-- Crea il database

--CREATE DATABASE FML ENCODING 'UTF-8';
--COMMENT ON DATABASE FML IS 'Database della nostra applicazione.';

CREATE SCHEMA FML;
COMMENT ON SCHEMA FML IS 'Schema del gruppo FML per il database';


CREATE TYPE gender AS ENUM ('f','m');
CREATE TYPE UserStatus AS ENUM ('Vivo','Deceduto','Trasferito');
CREATE TYPE Color AS ENUM ('Rossa','Bianca','Elettronica');
CREATE TYPE Priority AS ENUM ('Bassa','Media','Alta','Urgente');
CREATE TYPE ResidencyDomicile AS ENUM ('Residenza','Domicilio');
CREATE TYPE RelativeOfType AS ENUM ('Padre','Madre','Figlio','Figlia','Fratello','Sorella','Zio','Zia','Marito','Moglie','Nipote','Nonno','Nonna');
CREATE TYPE MeasurementsDescriptor AS ENUM ('Peso','Altezza','BMI','Circonferenza Vita','Frequenza Cardiaca','Aritmia','Creatinina, Volume Filtrato Glomerulare (VFG)', 'Valore del Glucosio nel sangue', 'Pressione Massima','Pressione Minima', 'Emogasanalisi (SG) Venoso','INR','Tempo di Protrombina (PT)- Sangue');
CREATE TYPE AdditionalServices AS ENUM ('ADP','ADI','PIPP','UVMD');

--CREATE EXTENSION pgcrypto;

-- #################################################################################################
-- ## Creazione delle tabelle                                                                     ##
-- #################################################################################################

-- Versione 1.00


CREATE TABLE FML.Patient (
    FiscalCode          CHAR(16),
    Name                VARCHAR(30) NOT NULL,
    Surname             VARCHAR(30) NOT NULL,
    Gender              gender,--EVITEREI DI METTERE NOT NULL
    Telephone           VARCHAR(50),
    Cellphone           VARCHAR(50),
    email               VARCHAR(50),
    userStatus          UserStatus,
    Nationality         VARCHAR(50),
    DoB                 date,
    ULSS                VARCHAR(30),
    PRIMARY KEY (FiscalCode)
    );

CREATE TABLE FML.Exemption (
    ExemptionCode  VARCHAR(30),
    Description    text NOT NULL,
    PRIMARY KEY (ExemptionCode)
);


CREATE TABLE FML.Benefit (
    ExemptionCode  VARCHAR(30),
    FiscalCode     CHAR(16),
    PRIMARY KEY (ExemptionCode, FiscalCode),
    FOREIGN KEY (ExemptionCode) REFERENCES FML.Exemption(ExemptionCode) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.NoteR (
	NoteID SERIAL,
	Note VARCHAR(300),
	FiscalCode CHAR(16),
	PRIMARY KEY (NoteID),
	FOREIGN KEY(FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.JobE (
	Jobs VARCHAR(30),
	PRIMARY KEY(Jobs)
);

CREATE TABLE FML.JobR (
	Jobs VARCHAR(30),
	FiscalCode CHAR(16),
	PRIMARY KEY (Jobs,FiscalCode),
	FOREIGN KEY (Jobs) REFERENCES FML.JobE(Jobs) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.PhotoR (
	PhotoID Serial,
	PBlob bytea,
	FiscalCode CHAR(16),
	PRIMARY KEY (PhotoID),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

--DA TOGLIERE PERCHE' E' ENUMERATE -----
--CREATE TABLE FML.TypeOfRelation (
--	Type RelativeOfType,
--	PRIMARY KEY(Type)
--);

CREATE TABLE FML.RelativeOf (
	FiscalCode CHAR(16),
	FiscalCode2 CHAR(16),
	StartDate date,
	EndDate date,
	Type RelativeOfType,
	PRIMARY KEY (FiscalCode,FiscalCode2,Type),
	FOREIGN KEY(FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY(FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
-------------------da togliere poichè ho tolto la tabella TypeOfRelation---------------------
--	FOREIGN KEY(Type) REFERENCES FML.TypeOfRelation(Type) ON DELETE RESTRICT ON UPDATE CASCADE 
);


CREATE TABLE FML.Doctor (
	FiscalCode CHAR(16),
	VATnumber VARCHAR(15),
	ASL VARCHAR(30),
	District VARCHAR(30),
	RegionalCode VARCHAR(30),--DUBBIO
	OrderNo VARCHAR(30),--DUBBIO
	Name VARCHAR(30) NOT NULL,
	Surname VARCHAR(30) NOT NULL,
	Gender gender,--EVITEREI DI METTERE NOT NULL
	Telephone VARCHAR(30),
	Cellphone VARCHAR(12),--METTIAMO CHAR 10 ?
	Email VARCHAR(30),
	PatientNo INT DEFAULT 0,--NON METTEREI NOT NULL ANCHE SE SUL DATA DIC E' STATO MESSO
	PRIMARY KEY(FiscalCode)
);
COMMENT ON TABLE FML.Doctor IS 'Entità Dottore';

CREATE TABLE FML.SpecializationE (
	Specialization VARCHAR(60),
	PRIMARY KEY (Specialization)
);

CREATE TABLE FML.SpecializationR (
	Specialization VARCHAR(60),
	FiscalCode CHAR(16),
	PRIMARY KEY (Specialization,FiscalCode),
	FOREIGN KEY (Specialization) REFERENCES FML.SpecializationE(Specialization) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Certificate (
	CertificateID SERIAL,
	CertificateDate timestamp DEFAULT CURRENT_TIMESTAMP,
	Description VARCHAR(300) NOT NULL,--necessario? nel da.dic. c'è
	Document bytea,
	FiscalCode CHAR(16) NOT NULL,
	FiscalCode2 Char(16) NOT NULL,
	PRIMARY KEY (CertificateID),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Register (
	FiscalCode CHAR(16),
	FiscalCode2 CHAR(16),
	TimeNumber INT DEFAULT 1,
	StartDate date DEFAULT CURRENT_DATE,
	EndDate date DEFAULT NULL,
	PRIMARY KEY (FiscalCode, FIscalCode2, TimeNumber),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Nation (
        Name VARCHAR(50),
        PRIMARY KEY (Name)
);

CREATE TABLE FML.Province (
        ProvName VARCHAR(30),
        Abbreviation VARCHAR(5),
        Name VARCHAR(30),
        PRIMARY KEY (ProvName,Name),
        FOREIGN KEY (Name) REFERENCES FML.Nation(Name) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.City (
        CityName VARCHAR(30),
        ProvName VARCHAR(30) NOT NULL,
        Name     VARCHAR(30),
        MetropolCityCode VARCHAR(30),
        PRIMARY KEY (CityName),
        FOREIGN KEY (ProvName,Name) REFERENCES FML.Province(ProvName,Name) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Born (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        PRIMARY KEY (FiscalCode),
        FOREIGN KEY (CityName) REFERENCES FML.City(CityName) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.PatientLive (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        ResidencyDomicile ResidencyDomicile,
        Street VARCHAR(30),
        CivicNumber SMALLINT,
        PostalCode CHAR(5),
        PRIMARY KEY (FiscalCode,ResidencyDomicile),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

COMMENT ON TABLE FML.PatientLive IS 'Il codice postale italiano è un numero formato da 5 cifre e può cominciare con zero';

CREATE TABLE FML.DoctorLive (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        ResidencyDomicile ResidencyDomicile,
        Street VARCHAR(30),
        CivicNumber SMALLINT,
        PostalCode CHAR(5),
        PRIMARY KEY (FiscalCode,ResidencyDomicile),
        FOREIGN KEY (CityName) REFERENCES FML.City(CityName) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.ExamResults (
        ResultsID SERIAL,
        ExamDate date NOT NULL,
        Details VARCHAR(200),
        PdfDocument bytea,
        FiscalCode CHAR(16) NOT NULL,
        PRIMARY KEY (ResultsID),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Visit (
        CounterID SERIAL,
        VisitNotes VARCHAR(300),
        VisitTimestamp timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
        FiscalCode CHAR(16) NOT NULL,
        FiscalCode2 CHAR(16) NOT NULL,
        PRIMARY KEY (CounterID),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FIscalCode2) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Analyzed (
        ResultID INT,
        CounterID INT,
        PRIMARY KEY (ResultID),
        FOREIGN KEY (ResultID) REFERENCES FML.ExamResults(ResultsID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

--ELIMINATA BASTA UN ENUMERATE
--CREATE TABLE FML.Measurements (
--        Descriptor MeasurementsDescriptor,
--        PRIMARY KEY (ValueMeasured,Descriptor)
--);

CREATE TABLE FML.Measure (
        CounterID INT,
        ValueMeasured REAL NOT NULL,
        Descriptor MeasurementsDescriptor,
        PRIMARY KEY (CounterID,Descriptor),
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);
--ELIMINATA LA TEBELLA E CREATO UN ENUMERATE-------------
--CREATE TABLE FML.AdditionalServices (       
--		Description VARCHAR(60),
--        PRIMARY KEY (Description)
--);

CREATE TABLE FML.Execute (
        FiscalCode CHAR(16),
        FiscalCode2 CHAR(16),
        Description AdditionalServices NOT NULL,
        ExDate date DEFAULT CURRENT_DATE NOT NULL,
        PRIMARY KEY (FiscalCode,FiscalCode2, Description,ExDate),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);
--tolta la foreign key
--        FOREIGN KEY (Description) REFERENCES FML.AdditionalServices(Description) ON DELETE RESTRICT ON UPDATE CASCADE


CREATE TABLE FML.User (
        Username VARCHAR(128),
        Password text NOT NULL,
        Email VARCHAR(60) NOT NULL,
        FiscalCode CHAR(16),
        PRIMARY KEY (Username),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE FML.Pathology (
        ICD9Code VARCHAR(20),
        ICD9CodePath VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        PRIMARY KEY (ICD9Code),
        FOREIGN KEY (ICD9CodePath) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Acknowledge (
        CounterID INT,
        ICD9Code VARCHAR(30),
        PRIMARY KEY (CounterID,ICD9Code),
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (ICD9Code) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Prescription (
        CounterID SERIAL,
        Color Color NOT NULL,
        Notes VARCHAR(200),
        Priority Priority,
        VisitCounterID INT,
        PRIMARY KEY (CounterID),
        FOREIGN KEY (VisitCounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Due (
        ICD9Code VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (ICD9Code,CounterID),
        FOREIGN KEY (ICD9Code) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Medicine (
        AIC VARCHAR(10),
        Goup VARCHAR(100),
        NamePack VARCHAR(100) NOT NULL,
        RetailPrice REAL,
        ExFactoryPrice REAL,
        MaxPrice REAL,
        OwnerAIC VARCHAR(30),
        GroupCode VARCHAR(4),--bastano 3 cifre (4 per sicurezza)
        Transparency VARCHAR(10),--non servono a nulla 30 cifre
        Regionality VARCHAR(10),--anche qui troppe cifre
        OxigenM3 REAL,
        PRIMARY KEY (AIC)
);

CREATE TABLE FML.AtcCategory (
        AtcSubCode VARCHAR(30),
        AtcSubCodeAtc VARCHAR(30),
        Description VARCHAR (300) NOT NULL,
        PRIMARY KEY (AtcSubCode),
        FOREIGN KEY (AtcSubCodeAtc) REFERENCES FML.AtcCategory(AtcSubCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Contain (
        AIC VARCHAR(10),--bastano 10
        AtcSubCode VARCHAR(30),
        PRIMARY KEY (AIC),
        FOREIGN KEY (AIC) REFERENCES FML.Medicine(AIC) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (AtcSubCode) REFERENCES FML.AtcCategory(AtcSubCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Indicate1 (
        AIC VARCHAR(10),--bastano 10
        CounterID INT,
        Quantity SMALLINT,
        PRIMARY KEY (AIC,CounterID),
        FOREIGN KEY (AIC) REFERENCES FML.Medicine(AIC) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.CVP (
        Cvp VARCHAR(10),
        Ntr VARCHAR(10) NOT NULL,
        DescriptionNtr VARCHAR(300) NOT NULL,--da allungare penso SECONDO ME QUI POSSIAMO FARE A MENO DI METTERE NOT NULL
        Father VARCHAR(300),
        Children VARCHAR(300),
        ChildNo SMALLINT,
        Compatibility CHAR(1),--SONO SOLO LETTERE
        Weight SMALLINT NOT NULL,
        Prescribing CHAR(1),--LETTERA O NUMERO
        Incompatibility VARCHAR(30),
        Inclusion VARCHAR(30),
        Notes VARCHAR (200),
        UniquePrescription VARCHAR(30),
        EncounterNo VARCHAR(30),
        Access VARCHAR(30),
        Rate VARCHAR(30),
        Stamp VARCHAR(100) NOT NULL,
        Category VARCHAR(30),
        KrRules VARCHAR(30),
        Constraints VARCHAR(30),
        MMGPLS VARCHAR(30),
        AnnexNo VARCHAR(30),
        PRIMARY KEY (Cvp)
);

CREATE TABLE FML.Indicate2 (
        Cvp VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (Cvp,CounterID),
        FOREIGN KEY (Cvp) REFERENCES FML.CVP(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.DayService (
        Code VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Indicate4 (
        CounterID INT,
        Code VARCHAR(30),
        PRIMARY KEY (CounterID, Code),
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Code) REFERENCES FML.DayService(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.GroupOf2 (
        Code VARCHAR(30),
        Cvp VARCHAR(30),
        MaxMultiplicity VARCHAR(30),
        Quantity SMALLINT,
        PRIMARY KEY (Code,Cvp),
        FOREIGN KEY (Code) REFERENCES FML.DayService(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Cvp) REFERENCES FML.Cvp(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Cluster (
        Code VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        Weight SMALLINT NOT NULL,
        PrescriptionRules VARCHAR(200) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Indicate3 (
        Code VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (Code,CounterID),
        FOREIGN KEY (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.GroupOf1 (
        Code VARCHAR(30),
        Cvp VARCHAR(30),
        Quantity SMALLINT,
        PRIMARY KEY (Code,Cvp),
        FOREIGN KEY (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Cvp) REFERENCES FML.CVP(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Branch (
        Code VARCHAR(30),
        Description VARCHAR(30) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Belong1 (
        Code VARCHAR(30),
        CodeB VARCHAR(30),
        PRIMARY KEY(Code),
        FOREIGN KEy (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CodeB) REFERENCES FML.Branch(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Belong2 (
        Cvp VARCHAR(30),
        Code VARCHAR(30),
        PRIMARY KEY (Cvp,Code),
        FOREIGN KEY (Cvp) REFERENCES FML.Cvp(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Code) REFERENCES FML.Branch(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

--POPOLAMENTO TABELLE RELATION

DELETE FROM FML.Acknowledge;
DELETE FROM FML.Analyzed;
DELETE FROM FML.Belong1;
DELETE FROM FML.Belong2;
DELETE FROM FML.Benefit;
DELETE FROM FML.Born;
DELETE FROM FML.Certificate;
DELETE FROM FML.Contain;
DELETE FROM FML.DoctorLive;
DELETE FROM FML.Due;
DELETE FROM FML.Execute;
DELETE FROM FML.GroupOf1;
DELETE FROM FML.GroupOf2;
DELETE FROM FML.Indicate1;
DELETE FROM FML.Indicate2;
DELETE FROM FML.Indicate3;
DELETE FROM FML.Indicate4;
DELETE FROM FML.JobR;
DELETE FROM FML.NoteR;
DELETE FROM FML.PatientLive;
DELETE FROM FML.PhotoR;
DELETE FROM FML.Register;
DELETE FROM FML.RelativeOf;
DELETE FROM FML.SpecializationR;
DELETE FROM FML.User;

--INSERIMENTO DATI Acknowledge (CounterID, ICD9Code)


INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('1','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('2','0029');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('3','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('7','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('9','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('10','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('11','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('13','00321');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('14','0031');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('15','00320');
--INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('','');

--INSERIMENTO DATI Analyzed (ResultID, CounterID)


INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('1','7');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('2','9');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('3','10');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('4','3');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('5','4');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('6','8');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('7','6');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('8','1');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('9','2');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('10','5');
--INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('','');

--INSERIMENTO DATI Belong1 (Code, CodeB)


INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('001','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('002','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('004','B05');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('005','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('006','B01');
--INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('','');

--INSERIMENTO DATI Belong2 (Cvp, Code)


INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B03');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B04');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B03');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B06');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_3','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_3','B06');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91','B06');
--INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('','');

--INSERIMENTO DATI Benefit (ExemptionCode, FiscalCode)


INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E11','GLVMRC75B03F205G');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E02','FRTFNC92M57M082E');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E04','FTNCLD97B57A906R');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E06','LBRMRC67H03F205A');
--INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('','');

--INSERIMENTO DATI Born (CityName, FiscalCode)


INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Monselice','SVLMHL73B57F205M');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Este','GLVMRC75B03F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','RZZNNA01B57F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','MNNSLV01B57A944W');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Spinea','FTNCLD97B57A906R');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Creazzo','BCCSFN97M17A944T');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Villorba','FRTFNC92M57M082E');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Zevio','TGLGCM86D11A944K');
--INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('','');

--INSERIMENTO DATI Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode(Doctor), FiscalCode2(Patient))


INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('1','2016-2-18 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('2','2016-3-24 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('3','2016-5-08 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('4','2016-6-01 17:25:06','',null,'MRNTTR69H03D612V','FRTFNC92M57M082E');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('5','2016-9-23 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('6','2016-12-07 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('7','2017-3-19 17:25:06','',null,'CVSRND64M15D969Y','SVLMHL73B57F205M');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('8','2017-4-05 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('9','2017-8-07 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('10','2018-5-01 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
--INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('',null,null,null,'','');

--INSERIMENTO DATI Contain (AIC, AtcSubCode)


INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('034208013','A02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('039716182','A01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('038835144','B01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('029532013','B01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('039716170','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('038835043','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('026851028','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('024155057','B02AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('031220027','A01AA');
--INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('','');

--INSERIMENTO DATI DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)


INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','LBNLCN74D14D150X','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','CVSRND64M15D969Y','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Vittorio Veneto','BSTFRC76L57E897P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FRRMNL58D11L840P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Creazzo','CNTVCN62T18L840B','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','MRNTTR69H03D612V','Residenza',null,null,null);
--INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI Due (ICD9Code, CounterID)


INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0021','1');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0030','2');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0029','3');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0021','4');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0030','5');
--INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('','');

--INSERIMENTO DATI Execute (FiscalCode, FiscalCode2, Description, ExDate)


INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','2/7/2016');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','PIPP','3/8/2016');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','ADP','25/2/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('FRRMNL58D11L840P','LBRMRC67H03F205A','ADP','29/3/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('MRNTTR69H03D612V','FRTFNC92M57M082E','PIPP','22/4/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','PIPP','1/6/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','11/9/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','RZZNNA01B57F205G','ADP','19/10/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','UVMD','20/12/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','UVMD','21/12/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','3/2/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','MNNSLV01B57A944W','ADP','15/4/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','RZZNNA01B57F205G','UVMD','17/4/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('FRRMNL58D11L840P','LBRMRC67H03F205A','ADP','20/5/2018');
--INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('','','','');


--INSERIMENTO DATI GroupOf1 (Code, Cvp, Quantity)


INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1_2',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1_3',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('002','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('002','02.93.1_2',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('003','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('003','03.8_0',null);
--INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES (null,null,null);

--INSERIMENTO DATI GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity)


INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','03.91_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','03.91_4',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','03.91_5',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','03.91_6',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D05','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D06','02.93.1_3',null,null);
--INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES (null,null,null,null);

--INSERIMENTO DATI Indicate1 (AIC, CounterID, Quantity)


INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('039716182','2',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('039716182','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835144','2',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('029532013','5',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','7',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('029532013','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851028','6',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835043','6',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851028','11',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835043','10',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','8',null);
--INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES (null,null,null);


--INSERIMENTO DATI Indicate2 (Cvp, CounterID)


INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('02.93.1','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('02.93.1_3','2');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','7');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','10');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_3','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_3','5');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_5','6');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','2');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','4');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','10');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','5');
--INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('','');


--INSERIMENTO DATI Indicate3 (Code, CounterID)


INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','2');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','3');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','7');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('003','2');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('004','6');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('003','6');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('002','6');
--INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('','');

--INSERIMENTO DATI Indicate4 (CounterID, Code)


INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D02');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D03');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('2','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D05');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D06');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D02');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('4','D05');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('4','D03');
--INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('','');

--INSERIMENTO DATI JobR (Jobs, FiscalCode)


INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Geometra','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Muratore','GLVMRC75B03F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Architetto','SVLMHL73B57F205M');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Falegname','RZZNNA01B57F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Postino','MNNSLV01B57A944W');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FTNCLD97B57A906R');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Idraulico','BCCSFN97M17A944T');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FRTFNC92M57M082E');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','TGLGCM86D11A944K');
--INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('','');

--INSERIMENTO DATI NoteR (NoteID, Note, FiscalCode)


INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('1','Paziente da tenere sotto controllo','GLVMRC75B03F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('2','Controllare diabete spesso','RZZNNA01B57F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('3','Problemi familiari','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('4','Misurare sempre pressione','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('5','Sentire parenti','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('6','Burbero','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('7','Controllare spesso livello insulina','TGLGCM86D11A944K');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('8','Esami del sangue da fare','SVLMHL73B57F205M');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('9','Difficoltà nell''apprendimento','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('10','Problemi schiena','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('11','Contattare Familiari','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('12','Sentire precedente medico di base','FRTFNC92M57M082E');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('13','Trattamenti laser da fare','FRTFNC92M57M082E');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('','','');

--INSERIMENTO DATI PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)


INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','GLVMRC75B03F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Este','SVLMHL73B57F205M','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','RZZNNA01B57F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','MNNSLV01B57A944W','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FTNCLD97B57A906R','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','BCCSFN97M17A944T','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','FRTFNC92M57M082E','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Zevio','TGLGCM86D11A944K','Residenza',null,null,null);
--INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI PhotoR (PhotoID, PBlob, FiscalCode)


INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('1',null,'LBRMRC67H03F205A');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('2',null,'GLVMRC75B03F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('3',null,'SVLMHL73B57F205M');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('4',null,'RZZNNA01B57F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('5',null,'MNNSLV01B57A944W');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('6',null,'GLVMRC75B03F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('7',null,'FTNCLD97B57A906R');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('8',null,'BCCSFN97M17A944T');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('9',null,'FRTFNC92M57M082E');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('10',null,'TGLGCM86D11A944K');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('11',null,'LBRMRC67H03F205A');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('12',null,'GLVMRC75B03F205G');
--INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('',null,'');

--INSERIMENTO DATI Register (FiscalCode, FiscalCode2, TimeNumber, StartDate, EndDate)


INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','LBRMRC67H03F205A',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','GLVMRC75B03F205G',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','SVLMHL73B57F205M',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','RZZNNA01B57F205G',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','MNNSLV01B57A944W',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','FTNCLD97B57A906R',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','BCCSFN97M17A944T',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','FRTFNC92M57M082E',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM86D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM87D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM88D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM89D11A944K',DEFAULT,DEFAULT);
--INSERT INTO FML.Register (FiscalCode, FiscalCode2, TimeNumber, StartDate, EndDate) VALUES (null,null,null,null,'null');


--INSERIMENTO DATI RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type)


INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('GLVMRC75B03F205G','MNNSLV01B57A944W',null,null,'Fratello');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('RZZNNA01B57F205G','BCCSFN97M17A944T',null,null,'Madre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('LBRMRC67H03F205A','MNNSLV01B57A944W',null,null,'Padre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('FRTFNC92M57M082E','TGLGCM86D11A944K',null,null,'Zia');
--INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('',null,null,'','');

--INSERIMENTO DATI SpecializationR (Specialization, FiscalCode)


INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Allergologia','LBNLCN74D14D150X');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Cardiochirurgia','CVSRND64M15D969Y');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Andrologia','BSTFRC76L57E897P');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Chiroplastica','FRRMNL58D11L840P');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Chirurgia generale','CNTVCN62T18L840B');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Allergologia','MRNTTR69H03D612V');
--INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('','');

--INSERIMENTO DATI User (Username, Password, Email, FiscalCode)


INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('la29','34mte5','luc@libero.it','LBNLCN74D14D150X');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('arm44','ertino4','armcav@gmail.com','CVSRND64M15D969Y');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('fefe84','fede1985de','fed@libero.it','BSTFRC76L57E897P');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('vincenzoc','supersecret','vizo@libero.it','CNTVCN62T18L840B');

--INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES (,,,);












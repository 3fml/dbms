-- Commands for making the database coherent after the insertion of base data

ALTER SEQUENCE fml.visit_counterid_seq RESTART WITH 18; 
ALTER SEQUENCE fml.prescription_counterid_seq RESTART WITH 12;


--This function and the trigger related are going to check if the TimeNumber is correct during the insertion of a new Patient in FML.Register. 
--TimeNumber is set to 1 if it's the first time a patient registers to a doctor, but the second time (with the same doctor) needs to be set to 2 and so on. 

--This row deletes the function and the trigger
DROP FUNCTION IF EXISTS update_pat() CASCADE;

CREATE FUNCTION update_pat() RETURNS TRIGGER AS $pat_upd$
       DECLARE
       numeroPaziente INT;
   BEGIN
       SELECT COUNT(*)::INT INTO numeroPaziente
       FROM FML.REGISTER
       WHERE FiscalCode2 = NEW.FiscalCode2
       AND FiscalCode = NEW.FiscalCode;
       NEW.TimeNumber = numeroPaziente + 1;
       --The following line updates the correct Patient with TimeNumber-1 setting the end date of his registration to Current_date
       UPDATE FML.Register SET EndDate = CURRENT_DATE WHERE FiscalCode2 = NEW.FiscalCode2 AND FiscalCode = NEW.FiscalCode AND TimeNumber = numeroPaziente;
       RETURN NEW;
   END;
$pat_upd$ LANGUAGE plpgsql;


CREATE TRIGGER update_patient
   BEFORE INSERT ON FML.Register
   FOR EACH ROW
   EXECUTE PROCEDURE update_pat();



--This row deletes the function and the trigger
DROP FUNCTION IF EXISTS increment_numberPat() CASCADE;

CREATE FUNCTION increment_numberPat() RETURNS TRIGGER AS $pat_ins$
		 DECLARE 
		 totalPat INT;
	BEGIN
		SELECT COUNT(*)::INT INTO totalPat
		FROM FML.REGISTER
		WHERE FiscalCode = NEW.FiscalCode AND EndDate IS NULL;
		UPDATE FML.Doctor SET PatientNo = totalPat WHERE FiscalCode = NEW.FiscalCode;
		RETURN NEW;
	END;
$pat_ins$ LANGUAGE plpgsql;

CREATE TRIGGER increment_PatientNo
	AFTER INSERT ON FML.Register
	FOR EACH ROW
	EXECUTE PROCEDURE increment_numberPat();



	--This row deletes the function and the trigger
	--Useless Trigger because we never delete patients from FML.Register
/*DROP FUNCTION IF EXISTS increment_numberPat() CASCADE;

CREATE FUNCTION decrement_numberPat() RETURNS TRIGGER AS $pat_del$
		DECLARE 
		totalPat INT;
	BEGIN
		SELECT COUNT(*)::INT INTO totalPat
		FROM FML.REGISTER
		WHERE FiscalCode = NEW.FiscalCode;
		UPDATE FML.Doctor SET PatientNo = totalPat WHERE FiscalCode = NEW.FiscalCode;
		RETURN NEW;
	END;
$pat_del$ LANGUAGE plpgsql;

CREATE TRIGGER decrement_PatientNo
	AFTER DELETE ON FML.Register
	FOR EACH ROW
	EXECUTE PROCEDURE decrement_numberPat();*/
--FILE PER POPOLARE TUTTO IL DB
--INSERIMENTO DATI Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS)

DELETE FROM FML.City;
DELETE FROM FML.Province;
DELETE FROM FML.Nation;
DELETE FROM FML.JobE;
DELETE FROM FML.Medicine;
DELETE FROM FML.Cluster;
DELETE FROM FML.Branch;
DELETE FROM FML.DayService;
DELETE FROM FML.ExamResults;
DELETE FROM FML.Prescription;
DELETE FROM FML.Measure;
DELETE FROM FML.Visit;
DELETE FROM FML.AtcCategory;
DELETE FROM FML.CVP;
DELETE FROM FML.Doctor;
DELETE FROM FML.Patient;
DELETE FROM FML.SpecializationE;
DELETE FROM FML.Exemption;
DELETE FROM FML.Pathology;











--INSERIMENTO DATI Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS)


INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('LBRMRC67H03F205A','Marco','Alberti','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('GLVMRC75B03F205G','Marco','Galvari','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('SVLMHL73B57F205M','Michela','Savilli','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('RZZNNA01B57F205G','Anna','Rozzulla','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('MNNSLV01B57A944W','Silvia','Mannati','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FTNCLD97B57A906R','Claudia','Fatin','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('BCCSFN97M17A944T','Buccio','Stefano','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FRTFNC92M57M082E','Francesca','Fertona','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM86D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1986-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM87D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1987-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM88D11A944K','Giacomo','Tiglioso','m',null,null,null,null,null,'1988-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM89D11A944K','Giacomo','Tigliatto','m',null,null,null,null,null,'1989-04-11',null);

--INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('','','','',,,,,,,);

--INSERIMENTO DATI Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo)


INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('LBNLCN74D14D150X','IT49996995991',null,null,'V4764','15487','Luciano','Albino','m','0492568','3325698445','luc@libero.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CVSRND64M15D969Y','IT19939955999',null,null,'R3737','12368','Armando','Cavestro','m',04955587,3751426548,'armcav@gmail.com',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('BSTFRC76L57E897P','IT12339755990',null,null,'F4857','18725','Federica','Busetta','f',04988722,3256497595,'fefe@outlook.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('FRRMNL58D11L840P','IT74339754090',null,null,'T4846','14247','Manuela','Ferrari','f',0498887132,3295587864,'manfe@gmail.com',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CNTVCN62T18L840B','IT74329751091',0801,'VI','E8773','19822','Vincenzo','Cantone','m',049256687,3226486472,'vizo@libero.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('MRNTTR69H03D612V','IT34123758071',null,null,'A0628','16982','Ettore','Marino','m',049871654,3275511422,'mari@outlook.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('GCMMLO67H03D511V','IT34456290948',0800,'VI','A0682','16998','Giacomo','Melito','m',049458654,3202311422,'meli@outlook.it',DEFAULT);
--INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES (,,,,,,,,,,,,);

--INSERIMENTO DATI Nation


INSERT INTO FML.Nation VALUES ('Italia');
INSERT INTO FML.Nation VALUES ('Francia');
INSERT INTO FML.Nation VALUES ('Germania');
INSERT INTO FML.Nation VALUES ('Russia');
INSERT INTO FML.Nation VALUES ('Brasile');
INSERT INTO FML.Nation VALUES ('Corea del Nord');
INSERT INTO FML.Nation VALUES ('Cina');
INSERT INTO FML.Nation VALUES ('Ucraina');
INSERT INTO FML.Nation VALUES ('Spagna');
INSERT INTO FML.Nation VALUES ('Slovenia');
INSERT INTO FML.Nation VALUES ('Austria');
INSERT INTO FML.Nation VALUES ('Svizzera');
INSERT INTO FML.Nation VALUES ('Finlandia');
INSERT INTO FML.Nation VALUES ('Portogallo');

--INSERT INTO FML.Nation VALUES ('');

--INSERIMENTO DATI Province (ProvName, Abbreviation, Name)


INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Padova','PD','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Venezia','VE','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Rovigo','RO','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Verona','VR','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Treviso','TV','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Vicenza','VI','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Belluno','BL','Italia');
--INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('','','');

--INSERIMENTO DATI City (CityName, Provname, Name, MetropolCityCode)


INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Monselice','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Este','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Dolo','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Spinea','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Bassano del Grappa','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Vittorio Veneto','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Asiago','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Creazzo','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villadose','Rovigo','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Zevio','Verona','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villorba','Treviso','Italia',null);
--INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('','',''null);

--INSERIMENTO DATI ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode)


INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('1','14/1/2017',null,null,'SVLMHL73B57F205M');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('2','22/1/2017',null,null,'FTNCLD97B57A906R');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('3','3/5/2017',null,null,'GLVMRC75B03F205G');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('4','24/5/2017',null,null,'TGLGCM86D11A944K');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('5','5/6/2017',null,null,'TGLGCM86D11A944K');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('6','16/7/2017',null,null,'FTNCLD97B57A906R');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('7','2/9/2017',null,null,'LBRMRC67H03F205A');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('8','4/3/2018',null,null,'BCCSFN97M17A944T');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('9','23/6/2018',null,null,'MNNSLV01B57A944W');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('10','19/9/2018',null,null,'LBRMRC67H03F205A');
--INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES (null,null,null,null,null);

--INSERIMENTO DATI JobE (Jobs)


INSERT INTO FML.JobE (Jobs) VALUES ('Muratore');
INSERT INTO FML.JobE (Jobs) VALUES ('Geometra');
INSERT INTO FML.JobE (Jobs) VALUES ('Ingegnere');
INSERT INTO FML.JobE (Jobs) VALUES ('Architetto');
INSERT INTO FML.JobE (Jobs) VALUES ('Falegname');
INSERT INTO FML.JobE (Jobs) VALUES ('Dottore');
INSERT INTO FML.JobE (Jobs) VALUES ('Insegnante');
INSERT INTO FML.JobE (Jobs) VALUES ('Professore');
INSERT INTO FML.JobE (Jobs) VALUES ('Bidello');
INSERT INTO FML.JobE (Jobs) VALUES ('Idraulico');
INSERT INTO FML.JobE (Jobs) VALUES ('Impresario');
INSERT INTO FML.JobE (Jobs) VALUES ('Banchiere');
INSERT INTO FML.JobE (Jobs) VALUES ('Impiegato');
INSERT INTO FML.JobE (Jobs) VALUES ('Postino');
--INSERT INTO FML.JobE (Jobs) VALUES ('');


--INSERIMENTO DATI Visit (CounterID, CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2)


INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('1',null,'2016-05-12 17:25:06','LBRMRC67H03F205A','FRRMNL58D11L840P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('2',null,'2016-05-12 17:37:16','GLVMRC75B03F205G','LBNLCN74D14D150X');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('3',null,'2016-05-12 17:45:00','LBRMRC67H03F205A','FRRMNL58D11L840P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('4',null,'2016-05-12 17:58:12','FTNCLD97B57A906R','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('5',null,'2016-05-12 18:15:28','TGLGCM86D11A944K','CNTVCN62T18L840B');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('6',null,'2016-05-12 19:25:24','FTNCLD97B57A906R','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('7',null,'2016-05-12 19:39:38','BCCSFN97M17A944T','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('8',null,'2016-05-12 19:55:01','MNNSLV01B57A944W','BSTFRC76L57E897P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('9',null,'2016-05-15 15:05:21','TGLGCM86D11A944K','CNTVCN62T18L840B');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('10',null,'2016-05-15 16:37:19','SVLMHL73B57F205M','CVSRND64M15D969Y');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('11',null,'2016-05-15 16:37:19','SVLMHL73B57F205M','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('12','Sta così così','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('13','Non migliora','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('14','Sempre peggio','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('15','Forse è guarito','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('16','Falso allarme','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('17','Sono proprio un bravo dottore','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
--INSERT INTO FML.Visit (CounterID, CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES (null,null,null,null,null,null);


--INSERIMENTO DATI Measure (CounterID, ValueMeasured, Descriptor)


INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','74.0','Peso');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','175.2','Altezza');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','24.11','BMI');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('3','56.74','Circonferenza Vita');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','87','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','0','Aritmia');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','100','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','65','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','102','Valore del Glucosio nel sangue');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','98','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','55','Pressione Minima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','68','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','0.9','INR');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','59.8','Peso');
--INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('','','');

--INSERIMENTO DATI CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo)


INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO',null,'1',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALIC',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1_2','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C',null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO','2',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1_3','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C',null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE MIDOLLARE/SPINALE','3',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE MIDOLLARE/SPINALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.8_0','03.8','INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE. Iniezione endorachide di antiblastici','INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA',null,'1',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_2','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE CERVICALE','2',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE CERVICALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_3','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE LOMBARE','3',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE LOMBARE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_4','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE SACRALE','4',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE SACRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_5','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE TORACICO','5',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE ANESTETICO PER ANALGESIA PERIDURALE CANALE TORACICO',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_6','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO PER ANALGESIA:TRONCULARE (ANES)','6',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA TRONCULARE (ANES)',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.92_0','03.92','INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE. Iniezione intratecale [endorachide] di steroidi Escluso: Iniezione di liquido di contrasto per mielogramma,  Iniezione di farmaco citotossico nel canale vertebrale (03.8)','INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.93.1_0','03.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE.','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE',null,null,null,null,null);

--INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES (null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

--INSERIMENTO DATI AtcCathegory (AtcSubCode, AtcSubCodeAtc, Description)


INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A',null,'Alimentary tract and metabolism');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B',null,'Blood and blood forming organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('C',null,'Cardiovascular system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('D',null,'Dermatologicals');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('G',null,'Genito-urinary system and sex hormones');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('H',null,'Systemic hormonal preparations, excluding sex hormones and insulins');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('J',null,'Antiinfectives for systemic use');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('L',null,'Antineoplastic and immunomodulating agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('M',null,'Musculo-skeletal system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('N',null,'Nervous system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('P',null,'Antiparasitic products, insecticides and repellents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('R',null,'	Respiratory system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('S',null,'Sensory organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('V',null,'Various');

INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01','A','Stomatological preparations');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02','A','Drugs for acid related disorders');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01','B','Antithrombotic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02','B','Antihemorrhagics');


INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AA','A01','Caries prophylactic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AB','A01','Anti-infectives and antiseptics for local oral treatment');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AA','A02','Magnesium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AB','A02','Aluminium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AA','B01','Vitamin K antagonists');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AB','B01','Heparin group');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AA','B02','Amino acids');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AB','B02','Proteinase inhibitors');
--INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('',null,'');

--INSERIMENTO DATI Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID)


INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('1','Rossa',null,null,'5');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('2','Rossa',null,null,'7');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('3','Bianca',null,null,'3');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('4','Elettronica',null,null,'10');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('5','Bianca',null,null,'1');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('6','Rossa',null,null,'4');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('7','Bianca',null,null,'6');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('8','Bianca','Elimina latte dalla dieta',null,'13');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('9','Rossa','Cure Termali',null,'14');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('10','Bianca','Bicarbonato',null,'15');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('11','Elettronica','Limone e peperoncino prima di colazione','Urgente','16');
--INSERT INTO FML.Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID) VALUES (null,null,null,null,null);

--INSERIMENTO DATI DayService (Code, Description)

INSERT INTO FML.DayService (Code, Description) VALUES ('D01','DaySGroup1');
INSERT INTO FML.DayService (Code, Description) VALUES ('D02','DaySGroup2');
INSERT INTO FML.DayService (Code, Description) VALUES ('D03','DaySGroup3');
INSERT INTO FML.DayService (Code, Description) VALUES ('D04','DaySGroup4');
INSERT INTO FML.DayService (Code, Description) VALUES ('D05','DaySGroup5');
INSERT INTO FML.DayService (Code, Description) VALUES ('D06','DaySGroup6');
--INSERT INTO FML.DayService (Code, Description) VALUES ('','');

--INSERIMENTO DATI Branch (Code, Description)


INSERT INTO FML.Branch (Code, Description) VALUES ('B01','BranchGropup1');
INSERT INTO FML.Branch (Code, Description) VALUES ('B02','BranchGropup2');
INSERT INTO FML.Branch (Code, Description) VALUES ('B03','BranchGropup3');
INSERT INTO FML.Branch (Code, Description) VALUES ('B04','BranchGropup4');
INSERT INTO FML.Branch (Code, Description) VALUES ('B05','BranchGropup5');
INSERT INTO FML.Branch (Code, Description) VALUES ('B06','BranchGropup6');

--INSERT INTO FML.Branch (Code, Description) VALUES ('','');

--INSERIMENTO DATI CLUster (Code, Description, Weight, PrescriptionRules)


INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('001','Gruppo1','4','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('002','Gruppo2','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('003','Gruppo3','2','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('004','Gruppo4','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('005','Gruppo5','3','Entro 45 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('006','Gruppo6','3','Entro 45 giorni');


--INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('','','','');

--INSERIMENTO DATI Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3)

INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('034208013','ACAMPOSATO 333MG 84 UNITA'' USO ORALE','CAMPRAL*84 cpr riv gastrores 33 mg','33.67',null,null,'BRUNO FARMACEUTICI SpA','H0A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('039716182','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'TECNIMEDE SOC.TECNICO-MED.S.A.','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('044155024','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'DOC GENERICI Srl','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('038835144','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'MERCK SERONO SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('029532013','ACARBOSIO 100MG 40 UNITA'' USO ORALE','GLICOBASE*40 cpr 100 mg','8.04',null,null,'BAYER SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('026851016','ACARBOSIO 100MG 40 UNITA'' USO ORALE','GLICOBASE*40 cpr 100 mg','8.04',null,null,'BAYER SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('039716170','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 50 mg','5.63',null,null,'TECNIMEDE SOC.TECNICO-MED.S.A.','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('044155012','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 50 mg','5.63',null,null,'DOC GENERICI Srl','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('038835043','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARPHAGE*40 cpr 50 mg','5.63',null,null,'MERCK SERONO SpA','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('026851028','ACARBOSIO 50MG 40 UNITA'' USO ORALE','GLUCOBAY*40 cpr 50 mg','8.04',null,null,'BAYER SpA','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('024155057','ACEBUTOLOLO 400MG 30 UNITA'' USO ORALE','SECTRAL*30 cpr 400 mg','10.71',null,null,'SANOFI SpA','B4A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('031842026','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','KAFENAC*os sosp polv 30 bust 100 mg','8.09',null,null,'ALMIRALL S.A.','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('032773032','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','AIRTAL*os sosp polv 30 bust 100 mg','8.14',null,null,'ALMIRALL S.A.','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('031220027','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','GLADIO*os polv 30 bust 100 mg','8.33',null,null,'ABIOGEN PHARMA SpA','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('042403042','ACECLOFENAC 100MG 40 UNITA'' USO ORALE','ACECLOFENAC*40 cpr riv 100 mg','5.64',null,null,'ACCORD HEALTHCARE LIMITED','B5B',null,null,null);

--INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES (null,null,null,null,null,null,null,null,null,null,null);

--INSERIMENTO DATI SpecializationE (Specialization)


INSERT INTO FML.SpecializationE (Specialization) VALUES ('Allergologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anatomia Patologica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Andrologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anestesia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Angiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Audiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiochirurgia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chinesiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chiroplastica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia apparato digerente ed endoscopia dig.');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia della mano');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia generale');
--INSERT INTO FML.SpecializationE (Specialization) VALUES ('');

--INSERIMENTO DATI Exemption (ExemptionCode, Description)


INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E01','Malattia di Hansen');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E02','Malattia di Whipple');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E03','Malattia di Lyme');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E04','Tumore di Wilms');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E05','Retinoblastoma');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E06','Malattia di Cronkhite-Canada');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E07','Sindrome di Gardner');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E08','Poliposi Familiare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E09','Linfoangioleiomiomatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E10','Sindrome del nevo Basocellulare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E11','Neurofibromatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E12','Complesso Carney');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E13','Cancro non Poliposico Ereditario del Colon');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E14','Melanoma cutaneo familiare e/o multiplo');
--INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES (,);

--INSERIMENTO DATI Pathology (ICD9Code, ICD9CodePath, Description)
DELETE FROM FML.Pathology;

--PRIMA INSERISCO I QUATTRO 'PADRI'-------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Cholera',null,'001');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Paratyphoid',null,'002');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Salmonella',null,'003');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Shigella',null,'004');

--E POI INSERISCO I FIGLI-----------------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0010','Cholera','Cholera due to vibrio cholerae');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0011','Cholera','Cholera due to vibrio cholerae el tor');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0019','Cholera','Cholera unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0021','Paratyphoid','Paratyphoid A');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0022','Paratyphoid','Paratyphoid B');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0023','Paratyphoid','Paratyphoid C');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0029','Paratyphoid','Paratyphoid fever, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0030','Salmonella','Salmonella gastroenteritis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0031','Salmonella','Salmonella septicemia');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00320','Salmonella','Localized salmonella infection, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00321','Salmonella','meningitis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0041','Shigella','Shigella flexneri');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0043','Shigella','Shigella sonnei');
--INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES (null,null,'');


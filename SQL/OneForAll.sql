
-- #################################################################################################
-- ## Creazione dello schema e degli enum                                                         ##
-- #################################################################################################

-- Cancella ogni eventuale istanza pre-esistente della base di dati
--DROP DATABASE IF EXISTS FML;
DROP SCHEMA IF EXISTS FML CASCADE;
DROP TYPE IF EXISTS gender;
DROP TYPE IF EXISTS UserStatus;
DROP TYPE IF EXISTS Color;
DROP TYPE IF EXISTS Priority;
DROP TYPE IF EXISTS ResidencyDomicile;
DROP TYPE IF EXISTS RelativeOfType;
DROP TYPE IF EXISTS MeasurementsDescriptor;
DROP TYPE IF EXISTS AdditionalServices;
-- Crea il database

--CREATE DATABASE FML ENCODING 'UTF-8';
--COMMENT ON DATABASE FML IS 'Database della nostra applicazione.';

CREATE SCHEMA FML;
COMMENT ON SCHEMA FML IS 'Schema del gruppo FML per il database';


CREATE TYPE gender AS ENUM ('f','m');
CREATE TYPE UserStatus AS ENUM ('Vivo','Deceduto','Trasferito');
CREATE TYPE Color AS ENUM ('Rossa','Bianca','Elettronica');
CREATE TYPE Priority AS ENUM ('Bassa','Media','Alta','Urgente');
CREATE TYPE ResidencyDomicile AS ENUM ('Residenza','Domicilio');
CREATE TYPE RelativeOfType AS ENUM ('Padre','Madre','Figlio','Figlia','Fratello','Sorella','Zio','Zia','Marito','Moglie','Nipote','Nonno','Nonna');
CREATE TYPE MeasurementsDescriptor AS ENUM ('Peso','Altezza','BMI','Circonferenza Vita','Frequenza Cardiaca','Aritmia','Creatinina, Volume Filtrato Glomerulare (VFG)', 'Valore del Glucosio nel sangue', 'Pressione Massima','Pressione Minima', 'Emogasanalisi (SG) Venoso','INR','Tempo di Protrombina (PT)- Sangue');
CREATE TYPE AdditionalServices AS ENUM ('ADP','ADI','PIPP','UVMD');

--CREATE EXTENSION pgcrypto;

-- #################################################################################################
-- ## Creazione delle tabelle                                                                     ##
-- #################################################################################################

-- Versione 1.00


CREATE TABLE FML.Patient (
    FiscalCode          CHAR(16),
    Name                VARCHAR(30) NOT NULL,
    Surname             VARCHAR(30) NOT NULL,
    Gender              gender,--EVITEREI DI METTERE NOT NULL
    Telephone           VARCHAR(50),
    Cellphone           VARCHAR(50),
    email               VARCHAR(50),
    userStatus          UserStatus,
    Nationality         VARCHAR(50),
    DoB                 date,
    ULSS                VARCHAR(30),
    PRIMARY KEY (FiscalCode)
    );

CREATE TABLE FML.Exemption (
    ExemptionCode  VARCHAR(30),
    Description    text NOT NULL,
    PRIMARY KEY (ExemptionCode)
);


CREATE TABLE FML.Benefit (
    ExemptionCode  VARCHAR(30),
    FiscalCode     CHAR(16),
    PRIMARY KEY (ExemptionCode, FiscalCode),
    FOREIGN KEY (ExemptionCode) REFERENCES FML.Exemption(ExemptionCode) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.NoteR (
	NoteID SERIAL,
	Note VARCHAR(300),
	FiscalCode CHAR(16),
	PRIMARY KEY (NoteID),
	FOREIGN KEY(FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.JobE (
	Jobs VARCHAR(30),
	PRIMARY KEY(Jobs)
);

CREATE TABLE FML.JobR (
	Jobs VARCHAR(30),
	FiscalCode CHAR(16),
	PRIMARY KEY (Jobs,FiscalCode),
	FOREIGN KEY (Jobs) REFERENCES FML.JobE(Jobs) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.PhotoR (
	PhotoID Serial,
	PBlob bytea,
	FiscalCode CHAR(16),
	PRIMARY KEY (PhotoID),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

--DA TOGLIERE PERCHE' E' ENUMERATE -----
--CREATE TABLE FML.TypeOfRelation (
--	Type RelativeOfType,
--	PRIMARY KEY(Type)
--);

CREATE TABLE FML.RelativeOf (
	FiscalCode CHAR(16),
	FiscalCode2 CHAR(16),
	StartDate date,
	EndDate date,
	Type RelativeOfType,
	PRIMARY KEY (FiscalCode,FiscalCode2,Type),
	FOREIGN KEY(FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY(FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
-------------------da togliere poichè ho tolto la tabella TypeOfRelation---------------------
--	FOREIGN KEY(Type) REFERENCES FML.TypeOfRelation(Type) ON DELETE RESTRICT ON UPDATE CASCADE 
);


CREATE TABLE FML.Doctor (
	FiscalCode CHAR(16),
	VATnumber VARCHAR(15),
	ASL VARCHAR(30),
	District VARCHAR(30),
	RegionalCode VARCHAR(30),--DUBBIO
	OrderNo VARCHAR(30),--DUBBIO
	Name VARCHAR(30) NOT NULL,
	Surname VARCHAR(30) NOT NULL,
	Gender gender,--EVITEREI DI METTERE NOT NULL
	Telephone VARCHAR(30),
	Cellphone VARCHAR(12),--METTIAMO CHAR 10 ?
	Email VARCHAR(30),
	PatientNo INT DEFAULT 0,--NON METTEREI NOT NULL ANCHE SE SUL DATA DIC E' STATO MESSO
	PRIMARY KEY(FiscalCode)
);
COMMENT ON TABLE FML.Doctor IS 'Entità Dottore';

CREATE TABLE FML.SpecializationE (
	Specialization VARCHAR(60),
	PRIMARY KEY (Specialization)
);

CREATE TABLE FML.SpecializationR (
	Specialization VARCHAR(60),
	FiscalCode CHAR(16),
	PRIMARY KEY (Specialization,FiscalCode),
	FOREIGN KEY (Specialization) REFERENCES FML.SpecializationE(Specialization) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Certificate (
	CertificateID SERIAL,
	CertificateDate timestamp DEFAULT CURRENT_TIMESTAMP,
	Description VARCHAR(300) NOT NULL,--necessario? nel da.dic. c'è
	Document bytea,
	FiscalCode CHAR(16) NOT NULL,
	FiscalCode2 Char(16) NOT NULL,
	PRIMARY KEY (CertificateID),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Register (
	FiscalCode CHAR(16),
	FiscalCode2 CHAR(16),
	TimeNumber INT DEFAULT 1,
	StartDate date DEFAULT CURRENT_DATE,
	EndDate date DEFAULT NULL,
	PRIMARY KEY (FiscalCode, FIscalCode2, TimeNumber),
	FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Nation (
        Name VARCHAR(50),
        PRIMARY KEY (Name)
);

CREATE TABLE FML.Province (
        ProvName VARCHAR(30),
        Abbreviation VARCHAR(5),
        Name VARCHAR(30),
        PRIMARY KEY (ProvName,Name),
        FOREIGN KEY (Name) REFERENCES FML.Nation(Name) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.City (
        CityName VARCHAR(30),
        ProvName VARCHAR(30) NOT NULL,
        Name     VARCHAR(30),
        MetropolCityCode VARCHAR(30),
        PRIMARY KEY (CityName),
        FOREIGN KEY (ProvName,Name) REFERENCES FML.Province(ProvName,Name) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Born (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        PRIMARY KEY (FiscalCode),
        FOREIGN KEY (CityName) REFERENCES FML.City(CityName) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.PatientLive (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        ResidencyDomicile ResidencyDomicile,
        Street VARCHAR(30),
        CivicNumber SMALLINT,
        PostalCode CHAR(5),
        PRIMARY KEY (FiscalCode,ResidencyDomicile),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

COMMENT ON TABLE FML.PatientLive IS 'Il codice postale italiano è un numero formato da 5 cifre e può cominciare con zero';

CREATE TABLE FML.DoctorLive (
        CityName VARCHAR(30) NOT NULL,
        FiscalCode CHAR(16),
        ResidencyDomicile ResidencyDomicile,
        Street VARCHAR(30),
        CivicNumber SMALLINT,
        PostalCode CHAR(5),
        PRIMARY KEY (FiscalCode,ResidencyDomicile),
        FOREIGN KEY (CityName) REFERENCES FML.City(CityName) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.ExamResults (
        ResultsID SERIAL,
        ExamDate date NOT NULL,
        Details VARCHAR(200),
        PdfDocument bytea,
        FiscalCode CHAR(16) NOT NULL,
        PRIMARY KEY (ResultsID),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Visit (
        CounterID SERIAL,
        VisitNotes VARCHAR(300),
        VisitTimestamp timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
        FiscalCode CHAR(16) NOT NULL,
        FiscalCode2 CHAR(16) NOT NULL,
        PRIMARY KEY (CounterID),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FIscalCode2) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Analyzed (
        ResultID INT,
        CounterID INT,
        PRIMARY KEY (ResultID),
        FOREIGN KEY (ResultID) REFERENCES FML.ExamResults(ResultsID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

--ELIMINATA BASTA UN ENUMERATE
--CREATE TABLE FML.Measurements (
--        Descriptor MeasurementsDescriptor,
--        PRIMARY KEY (ValueMeasured,Descriptor)
--);

CREATE TABLE FML.Measure (
        CounterID INT,
        ValueMeasured REAL NOT NULL,
        Descriptor MeasurementsDescriptor,
        PRIMARY KEY (CounterID,Descriptor),
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);
--ELIMINATA LA TEBELLA E CREATO UN ENUMERATE-------------
--CREATE TABLE FML.AdditionalServices (       
--		Description VARCHAR(60),
--        PRIMARY KEY (Description)
--);

CREATE TABLE FML.Execute (
        FiscalCode CHAR(16),
        FiscalCode2 CHAR(16),
        Description AdditionalServices NOT NULL,
        ExDate date DEFAULT CURRENT_DATE NOT NULL,
        PRIMARY KEY (FiscalCode,FiscalCode2, Description,ExDate),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (FiscalCode2) REFERENCES FML.Patient(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);
--tolta la foreign key
--        FOREIGN KEY (Description) REFERENCES FML.AdditionalServices(Description) ON DELETE RESTRICT ON UPDATE CASCADE


CREATE TABLE FML.User (
        Username VARCHAR(128),
        Password text NOT NULL,
        Email VARCHAR(60) NOT NULL,
        FiscalCode CHAR(16),
        PRIMARY KEY (Username),
        FOREIGN KEY (FiscalCode) REFERENCES FML.Doctor(FiscalCode) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE FML.Pathology (
        ICD9Code VARCHAR(20),
        ICD9CodePath VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        PRIMARY KEY (ICD9Code),
        FOREIGN KEY (ICD9CodePath) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Acknowledge (
        CounterID INT,
        ICD9Code VARCHAR(30),
        PRIMARY KEY (CounterID,ICD9Code),
        FOREIGN KEY (CounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (ICD9Code) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Prescription (
        CounterID SERIAL,
        Color Color NOT NULL,
        Notes VARCHAR(200),
        Priority Priority,
        VisitCounterID INT,
        PRIMARY KEY (CounterID),
        FOREIGN KEY (VisitCounterID) REFERENCES FML.Visit(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Due (
        ICD9Code VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (ICD9Code,CounterID),
        FOREIGN KEY (ICD9Code) REFERENCES FML.Pathology(ICD9Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Medicine (
        AIC VARCHAR(10),
        Goup VARCHAR(100),
        NamePack VARCHAR(100) NOT NULL,
        RetailPrice REAL,
        ExFactoryPrice REAL,
        MaxPrice REAL,
        OwnerAIC VARCHAR(30),
        GroupCode VARCHAR(4),--bastano 3 cifre (4 per sicurezza)
        Transparency VARCHAR(10),--non servono a nulla 30 cifre
        Regionality VARCHAR(10),--anche qui troppe cifre
        OxigenM3 REAL,
        PRIMARY KEY (AIC)
);

CREATE TABLE FML.AtcCategory (
        AtcSubCode VARCHAR(30),
        AtcSubCodeAtc VARCHAR(30),
        Description VARCHAR (300) NOT NULL,
        PRIMARY KEY (AtcSubCode),
        FOREIGN KEY (AtcSubCodeAtc) REFERENCES FML.AtcCategory(AtcSubCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Contain (
        AIC VARCHAR(10),--bastano 10
        AtcSubCode VARCHAR(30),
        PRIMARY KEY (AIC),
        FOREIGN KEY (AIC) REFERENCES FML.Medicine(AIC) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (AtcSubCode) REFERENCES FML.AtcCategory(AtcSubCode) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Indicate1 (
        AIC VARCHAR(10),--bastano 10
        CounterID INT,
        Quantity SMALLINT,
        PRIMARY KEY (AIC,CounterID),
        FOREIGN KEY (AIC) REFERENCES FML.Medicine(AIC) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.CVP (
        Cvp VARCHAR(10),
        Ntr VARCHAR(10) NOT NULL,
        DescriptionNtr VARCHAR(300) NOT NULL,--da allungare penso SECONDO ME QUI POSSIAMO FARE A MENO DI METTERE NOT NULL
        Father VARCHAR(300),
        Children VARCHAR(300),
        ChildNo SMALLINT,
        Compatibility CHAR(1),--SONO SOLO LETTERE
        Weight SMALLINT NOT NULL,
        Prescribing CHAR(1),--LETTERA O NUMERO
        Incompatibility VARCHAR(30),
        Inclusion VARCHAR(30),
        Notes VARCHAR (200),
        UniquePrescription VARCHAR(30),
        EncounterNo VARCHAR(30),
        Access VARCHAR(30),
        Rate VARCHAR(30),
        Stamp VARCHAR(100) NOT NULL,
        Category VARCHAR(30),
        KrRules VARCHAR(30),
        Constraints VARCHAR(30),
        MMGPLS VARCHAR(30),
        AnnexNo VARCHAR(30),
        PRIMARY KEY (Cvp)
);

CREATE TABLE FML.Indicate2 (
        Cvp VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (Cvp,CounterID),
        FOREIGN KEY (Cvp) REFERENCES FML.CVP(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.DayService (
        Code VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Indicate4 (
        CounterID INT,
        Code VARCHAR(30),
        PRIMARY KEY (CounterID, Code),
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Code) REFERENCES FML.DayService(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.GroupOf2 (
        Code VARCHAR(30),
        Cvp VARCHAR(30),
        MaxMultiplicity VARCHAR(30),
        Quantity SMALLINT,
        PRIMARY KEY (Code,Cvp),
        FOREIGN KEY (Code) REFERENCES FML.DayService(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Cvp) REFERENCES FML.Cvp(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Cluster (
        Code VARCHAR(30),
        Description VARCHAR(200) NOT NULL,
        Weight SMALLINT NOT NULL,
        PrescriptionRules VARCHAR(200) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Indicate3 (
        Code VARCHAR(30),
        CounterID INT,
        PRIMARY KEY (Code,CounterID),
        FOREIGN KEY (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CounterID) REFERENCES FML.Prescription(CounterID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.GroupOf1 (
        Code VARCHAR(30),
        Cvp VARCHAR(30),
        Quantity SMALLINT,
        PRIMARY KEY (Code,Cvp),
        FOREIGN KEY (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Cvp) REFERENCES FML.CVP(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Branch (
        Code VARCHAR(30),
        Description VARCHAR(30) NOT NULL,
        PRIMARY KEY (Code)
);

CREATE TABLE FML.Belong1 (
        Code VARCHAR(30),
        CodeB VARCHAR(30),
        PRIMARY KEY(Code),
        FOREIGN KEy (Code) REFERENCES FML.Cluster(Code) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (CodeB) REFERENCES FML.Branch(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE FML.Belong2 (
        Cvp VARCHAR(30),
        Code VARCHAR(30),
        PRIMARY KEY (Cvp,Code),
        FOREIGN KEY (Cvp) REFERENCES FML.Cvp(Cvp) ON DELETE RESTRICT ON UPDATE CASCADE,
        FOREIGN KEY (Code) REFERENCES FML.Branch(Code) ON DELETE RESTRICT ON UPDATE CASCADE
);
-- Commands for making the database coherent after the insertion of base data

ALTER SEQUENCE fml.visit_counterid_seq RESTART WITH 18; 
ALTER SEQUENCE fml.prescription_counterid_seq RESTART WITH 12;


--This function and the trigger related are going to check if the TimeNumber is correct during the insertion of a new Patient in FML.Register. 
--TimeNumber is set to 1 if it's the first time a patient registers to a doctor, but the second time (with the same doctor) needs to be set to 2 and so on. 

--This row deletes the function and the trigger
DROP FUNCTION IF EXISTS update_pat() CASCADE;

CREATE FUNCTION update_pat() RETURNS TRIGGER AS $pat_upd$
       DECLARE
       numeroPaziente INT;
   BEGIN
       SELECT COUNT(*)::INT INTO numeroPaziente
       FROM FML.REGISTER
       WHERE FiscalCode2 = NEW.FiscalCode2
       AND FiscalCode = NEW.FiscalCode;
       NEW.TimeNumber = numeroPaziente + 1;
       --The following line updates the correct Patient with TimeNumber-1 setting the end date of his registration to Current_date
       UPDATE FML.Register SET EndDate = CURRENT_DATE WHERE FiscalCode2 = NEW.FiscalCode2 AND FiscalCode = NEW.FiscalCode AND TimeNumber = numeroPaziente;
       RETURN NEW;
   END;
$pat_upd$ LANGUAGE plpgsql;


CREATE TRIGGER update_patient
   BEFORE INSERT ON FML.Register
   FOR EACH ROW
   EXECUTE PROCEDURE update_pat();



--This row deletes the function and the trigger
DROP FUNCTION IF EXISTS increment_numberPat() CASCADE;

CREATE FUNCTION increment_numberPat() RETURNS TRIGGER AS $pat_ins$
		 DECLARE 
		 totalPat INT;
	BEGIN
		SELECT COUNT(*)::INT INTO totalPat
		FROM FML.REGISTER
		WHERE FiscalCode = NEW.FiscalCode AND EndDate IS NULL;
		UPDATE FML.Doctor SET PatientNo = totalPat WHERE FiscalCode = NEW.FiscalCode;
		RETURN NEW;
	END;
$pat_ins$ LANGUAGE plpgsql;

CREATE TRIGGER increment_PatientNo
	AFTER INSERT ON FML.Register
	FOR EACH ROW
	EXECUTE PROCEDURE increment_numberPat();



	--This row deletes the function and the trigger
	--Useless Trigger because we never delete patients from FML.Register
/*DROP FUNCTION IF EXISTS increment_numberPat() CASCADE;

CREATE FUNCTION decrement_numberPat() RETURNS TRIGGER AS $pat_del$
		DECLARE 
		totalPat INT;
	BEGIN
		SELECT COUNT(*)::INT INTO totalPat
		FROM FML.REGISTER
		WHERE FiscalCode = NEW.FiscalCode;
		UPDATE FML.Doctor SET PatientNo = totalPat WHERE FiscalCode = NEW.FiscalCode;
		RETURN NEW;
	END;
$pat_del$ LANGUAGE plpgsql;

CREATE TRIGGER decrement_PatientNo
	AFTER DELETE ON FML.Register
	FOR EACH ROW
	EXECUTE PROCEDURE decrement_numberPat();*/--FILE PER POPOLARE TUTTO IL DB
--INSERIMENTO DATI Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS)

DELETE FROM FML.City;
DELETE FROM FML.Province;
DELETE FROM FML.Nation;
DELETE FROM FML.JobE;
DELETE FROM FML.Medicine;
DELETE FROM FML.Cluster;
DELETE FROM FML.Branch;
DELETE FROM FML.DayService;
DELETE FROM FML.ExamResults;
DELETE FROM FML.Prescription;
DELETE FROM FML.Measure;
DELETE FROM FML.Visit;
DELETE FROM FML.AtcCategory;
DELETE FROM FML.CVP;
DELETE FROM FML.Doctor;
DELETE FROM FML.Patient;
DELETE FROM FML.SpecializationE;
DELETE FROM FML.Exemption;
DELETE FROM FML.Pathology;











--INSERIMENTO DATI Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS)


INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('LBRMRC67H03F205A','Marco','Alberti','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('GLVMRC75B03F205G','Marco','Galvari','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('SVLMHL73B57F205M','Michela','Savilli','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('RZZNNA01B57F205G','Anna','Rozzulla','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('MNNSLV01B57A944W','Silvia','Mannati','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FTNCLD97B57A906R','Claudia','Fatin','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('BCCSFN97M17A944T','Buccio','Stefano','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FRTFNC92M57M082E','Francesca','Fertona','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM86D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1986-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM87D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1987-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM88D11A944K','Giacomo','Tiglioso','m',null,null,null,null,null,'1988-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM89D11A944K','Giacomo','Tigliatto','m',null,null,null,null,null,'1989-04-11',null);

--INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('','','','',,,,,,,);

--INSERIMENTO DATI Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo)


INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('LBNLCN74D14D150X','IT49996995991',null,null,'V4764','15487','Luciano','Albino','m','0492568','3325698445','luc@libero.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CVSRND64M15D969Y','IT19939955999',null,null,'R3737','12368','Armando','Cavestro','m',04955587,3751426548,'armcav@gmail.com',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('BSTFRC76L57E897P','IT12339755990',null,null,'F4857','18725','Federica','Busetta','f',04988722,3256497595,'fefe@outlook.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('FRRMNL58D11L840P','IT74339754090',null,null,'T4846','14247','Manuela','Ferrari','f',0498887132,3295587864,'manfe@gmail.com',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CNTVCN62T18L840B','IT74329751091',0801,'VI','E8773','19822','Vincenzo','Cantone','m',049256687,3226486472,'vizo@libero.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('MRNTTR69H03D612V','IT34123758071',null,null,'A0628','16982','Ettore','Marino','m',049871654,3275511422,'mari@outlook.it',DEFAULT);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('GCMMLO67H03D511V','IT34456290948',0800,'VI','A0682','16998','Giacomo','Melito','m',049458654,3202311422,'meli@outlook.it',DEFAULT);
--INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES (,,,,,,,,,,,,);

--INSERIMENTO DATI Nation


INSERT INTO FML.Nation VALUES ('Italia');
INSERT INTO FML.Nation VALUES ('Francia');
INSERT INTO FML.Nation VALUES ('Germania');
INSERT INTO FML.Nation VALUES ('Russia');
INSERT INTO FML.Nation VALUES ('Brasile');
INSERT INTO FML.Nation VALUES ('Corea del Nord');
INSERT INTO FML.Nation VALUES ('Cina');
INSERT INTO FML.Nation VALUES ('Ucraina');
INSERT INTO FML.Nation VALUES ('Spagna');
INSERT INTO FML.Nation VALUES ('Slovenia');
INSERT INTO FML.Nation VALUES ('Austria');
INSERT INTO FML.Nation VALUES ('Svizzera');
INSERT INTO FML.Nation VALUES ('Finlandia');
INSERT INTO FML.Nation VALUES ('Portogallo');

--INSERT INTO FML.Nation VALUES ('');

--INSERIMENTO DATI Province (ProvName, Abbreviation, Name)


INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Padova','PD','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Venezia','VE','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Rovigo','RO','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Verona','VR','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Treviso','TV','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Vicenza','VI','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Belluno','BL','Italia');
--INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('','','');

--INSERIMENTO DATI City (CityName, Provname, Name, MetropolCityCode)


INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Monselice','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Este','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Dolo','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Spinea','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Bassano del Grappa','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Vittorio Veneto','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Asiago','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Creazzo','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villadose','Rovigo','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Zevio','Verona','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villorba','Treviso','Italia',null);
--INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('','',''null);

--INSERIMENTO DATI ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode)


INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('1','14/1/2017',null,null,'SVLMHL73B57F205M');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('2','22/1/2017',null,null,'FTNCLD97B57A906R');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('3','3/5/2017',null,null,'GLVMRC75B03F205G');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('4','24/5/2017',null,null,'TGLGCM86D11A944K');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('5','5/6/2017',null,null,'TGLGCM86D11A944K');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('6','16/7/2017',null,null,'FTNCLD97B57A906R');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('7','2/9/2017',null,null,'LBRMRC67H03F205A');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('8','4/3/2018',null,null,'BCCSFN97M17A944T');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('9','23/6/2018',null,null,'MNNSLV01B57A944W');
INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES ('10','19/9/2018',null,null,'LBRMRC67H03F205A');
--INSERT INTO FML.ExamResults (ResultsID, ExamDate, Details, PdfDocument, FiscalCode) VALUES (null,null,null,null,null);

--INSERIMENTO DATI JobE (Jobs)


INSERT INTO FML.JobE (Jobs) VALUES ('Muratore');
INSERT INTO FML.JobE (Jobs) VALUES ('Geometra');
INSERT INTO FML.JobE (Jobs) VALUES ('Ingegnere');
INSERT INTO FML.JobE (Jobs) VALUES ('Architetto');
INSERT INTO FML.JobE (Jobs) VALUES ('Falegname');
INSERT INTO FML.JobE (Jobs) VALUES ('Dottore');
INSERT INTO FML.JobE (Jobs) VALUES ('Insegnante');
INSERT INTO FML.JobE (Jobs) VALUES ('Professore');
INSERT INTO FML.JobE (Jobs) VALUES ('Bidello');
INSERT INTO FML.JobE (Jobs) VALUES ('Idraulico');
INSERT INTO FML.JobE (Jobs) VALUES ('Impresario');
INSERT INTO FML.JobE (Jobs) VALUES ('Banchiere');
INSERT INTO FML.JobE (Jobs) VALUES ('Impiegato');
INSERT INTO FML.JobE (Jobs) VALUES ('Postino');
--INSERT INTO FML.JobE (Jobs) VALUES ('');


--INSERIMENTO DATI Visit (CounterID, CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2)


INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('1',null,'2016-05-12 17:25:06','LBRMRC67H03F205A','FRRMNL58D11L840P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('2',null,'2016-05-12 17:37:16','GLVMRC75B03F205G','LBNLCN74D14D150X');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('3',null,'2016-05-12 17:45:00','LBRMRC67H03F205A','FRRMNL58D11L840P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('4',null,'2016-05-12 17:58:12','FTNCLD97B57A906R','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('5',null,'2016-05-12 18:15:28','TGLGCM86D11A944K','CNTVCN62T18L840B');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('6',null,'2016-05-12 19:25:24','FTNCLD97B57A906R','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('7',null,'2016-05-12 19:39:38','BCCSFN97M17A944T','MRNTTR69H03D612V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('8',null,'2016-05-12 19:55:01','MNNSLV01B57A944W','BSTFRC76L57E897P');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('9',null,'2016-05-15 15:05:21','TGLGCM86D11A944K','CNTVCN62T18L840B');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('10',null,'2016-05-15 16:37:19','SVLMHL73B57F205M','CVSRND64M15D969Y');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('11',null,'2016-05-15 16:37:19','SVLMHL73B57F205M','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('12','Sta così così','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('13','Non migliora','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('14','Sempre peggio','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('15','Forse è guarito','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('16','Falso allarme','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
INSERT INTO FML.Visit (CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('17','Sono proprio un bravo dottore','2016-05-15 16:37:19','TGLGCM86D11A944K','GCMMLO67H03D511V');
--INSERT INTO FML.Visit (CounterID, CounterID,VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES (null,null,null,null,null,null);


--INSERIMENTO DATI Measure (CounterID, ValueMeasured, Descriptor)


INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','74.0','Peso');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','175.2','Altezza');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','24.11','BMI');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('3','56.74','Circonferenza Vita');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','87','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','0','Aritmia');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','100','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','65','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','102','Valore del Glucosio nel sangue');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','98','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','55','Pressione Minima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','68','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','0.9','INR');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','59.8','Peso');
--INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('','','');

--INSERIMENTO DATI CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo)


INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO',null,'1',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALIC',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1_2','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C',null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO','2',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('02.93.1_3','02.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE ENCEFALICO. Non associabile a visita neurologica di controllo 89.01.C',null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE MIDOLLARE/SPINALE','3',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE MIDOLLARE/SPINALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.8_0','03.8','INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE. Iniezione endorachide di antiblastici','INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI FARMACI CITOTOSSICI NEL CANALE VERTEBRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA',null,'1',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_2','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE CERVICALE','2',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE CERVICALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_3','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE LOMBARE','3',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE LOMBARE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_4','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE SACRALE','4',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA PERIDURALE CANALE SACRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_5','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA: PERIDURALE CANALE TORACICO','5',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE ANESTETICO PER ANALGESIA PERIDURALE CANALE TORACICO',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.91_6','03.91','INIEZIONE DI ANESTETICO NEL CANALE VERTEBRALE PER ANALGESIA. Iniezione peridurale Escluso: il caso in cui lnull anestesia sia effettuata per intervento',null,'INIEZIONE DI ANESTETICO PER ANALGESIA:TRONCULARE (ANES)','6',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ANESTETICO PER ANALGESIA TRONCULARE (ANES)',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.92_0','03.92','INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE. Iniezione intratecale [endorachide] di steroidi Escluso: Iniezione di liquido di contrasto per mielogramma,  Iniezione di farmaco citotossico nel canale vertebrale (03.8)','INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'INIEZIONE DI ALTRI FARMACI NEL CANALE VERTEBRALE',null,null,null,null,null);
INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES ('03.93.1_0','03.93.1','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE.','CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE',null,'0',null,'1',null,null,null,null,null,null,null,null,'CONTROLLO/PROGRAMMAZIONE DI NEUROSTIMOLATORE SPINALE',null,null,null,null,null);

--INSERT INTO FML.CVP (Cvp, Ntr, DescriptionNtr, Father, Children, ChildNo, Compatibility, Weight, Prescribing, Incompatibility, Inclusion, Notes, UniquePrescription, EncounterNo, Access, Rate, Stamp, Category, KrRules, Constraints, MMGPLS, AnnexNo) VALUES (null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

--INSERIMENTO DATI AtcCathegory (AtcSubCode, AtcSubCodeAtc, Description)


INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A',null,'Alimentary tract and metabolism');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B',null,'Blood and blood forming organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('C',null,'Cardiovascular system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('D',null,'Dermatologicals');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('G',null,'Genito-urinary system and sex hormones');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('H',null,'Systemic hormonal preparations, excluding sex hormones and insulins');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('J',null,'Antiinfectives for systemic use');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('L',null,'Antineoplastic and immunomodulating agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('M',null,'Musculo-skeletal system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('N',null,'Nervous system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('P',null,'Antiparasitic products, insecticides and repellents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('R',null,'	Respiratory system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('S',null,'Sensory organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('V',null,'Various');

INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01','A','Stomatological preparations');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02','A','Drugs for acid related disorders');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01','B','Antithrombotic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02','B','Antihemorrhagics');


INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AA','A01','Caries prophylactic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AB','A01','Anti-infectives and antiseptics for local oral treatment');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AA','A02','Magnesium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AB','A02','Aluminium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AA','B01','Vitamin K antagonists');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AB','B01','Heparin group');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AA','B02','Amino acids');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AB','B02','Proteinase inhibitors');
--INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('',null,'');

--INSERIMENTO DATI Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID)


INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('1','Rossa',null,null,'5');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('2','Rossa',null,null,'7');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('3','Bianca',null,null,'3');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('4','Elettronica',null,null,'10');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('5','Bianca',null,null,'1');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('6','Rossa',null,null,'4');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('7','Bianca',null,null,'6');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('8','Bianca','Elimina latte dalla dieta',null,'13');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('9','Rossa','Cure Termali',null,'14');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('10','Bianca','Bicarbonato',null,'15');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('11','Elettronica','Limone e peperoncino prima di colazione','Urgente','16');
--INSERT INTO FML.Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID) VALUES (null,null,null,null,null);

--INSERIMENTO DATI DayService (Code, Description)

INSERT INTO FML.DayService (Code, Description) VALUES ('D01','DaySGroup1');
INSERT INTO FML.DayService (Code, Description) VALUES ('D02','DaySGroup2');
INSERT INTO FML.DayService (Code, Description) VALUES ('D03','DaySGroup3');
INSERT INTO FML.DayService (Code, Description) VALUES ('D04','DaySGroup4');
INSERT INTO FML.DayService (Code, Description) VALUES ('D05','DaySGroup5');
INSERT INTO FML.DayService (Code, Description) VALUES ('D06','DaySGroup6');
--INSERT INTO FML.DayService (Code, Description) VALUES ('','');

--INSERIMENTO DATI Branch (Code, Description)


INSERT INTO FML.Branch (Code, Description) VALUES ('B01','BranchGropup1');
INSERT INTO FML.Branch (Code, Description) VALUES ('B02','BranchGropup2');
INSERT INTO FML.Branch (Code, Description) VALUES ('B03','BranchGropup3');
INSERT INTO FML.Branch (Code, Description) VALUES ('B04','BranchGropup4');
INSERT INTO FML.Branch (Code, Description) VALUES ('B05','BranchGropup5');
INSERT INTO FML.Branch (Code, Description) VALUES ('B06','BranchGropup6');

--INSERT INTO FML.Branch (Code, Description) VALUES ('','');

--INSERIMENTO DATI CLUster (Code, Description, Weight, PrescriptionRules)


INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('001','Gruppo1','4','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('002','Gruppo2','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('003','Gruppo3','2','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('004','Gruppo4','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('005','Gruppo5','3','Entro 45 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('006','Gruppo6','3','Entro 45 giorni');


--INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('','','','');

--INSERIMENTO DATI Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3)

INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('034208013','ACAMPOSATO 333MG 84 UNITA'' USO ORALE','CAMPRAL*84 cpr riv gastrores 33 mg','33.67',null,null,'BRUNO FARMACEUTICI SpA','H0A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('039716182','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'TECNIMEDE SOC.TECNICO-MED.S.A.','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('044155024','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'DOC GENERICI Srl','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('038835144','ACARBOSIO 100MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 100 mg','5.63',null,null,'MERCK SERONO SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('029532013','ACARBOSIO 100MG 40 UNITA'' USO ORALE','GLICOBASE*40 cpr 100 mg','8.04',null,null,'BAYER SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('026851016','ACARBOSIO 100MG 40 UNITA'' USO ORALE','GLICOBASE*40 cpr 100 mg','8.04',null,null,'BAYER SpA','H1A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('039716170','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 50 mg','5.63',null,null,'TECNIMEDE SOC.TECNICO-MED.S.A.','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('044155012','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARBOSIO*40 cpr 50 mg','5.63',null,null,'DOC GENERICI Srl','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('038835043','ACARBOSIO 50MG 40 UNITA'' USO ORALE','ACARPHAGE*40 cpr 50 mg','5.63',null,null,'MERCK SERONO SpA','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('026851028','ACARBOSIO 50MG 40 UNITA'' USO ORALE','GLUCOBAY*40 cpr 50 mg','8.04',null,null,'BAYER SpA','H1B',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('024155057','ACEBUTOLOLO 400MG 30 UNITA'' USO ORALE','SECTRAL*30 cpr 400 mg','10.71',null,null,'SANOFI SpA','B4A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('031842026','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','KAFENAC*os sosp polv 30 bust 100 mg','8.09',null,null,'ALMIRALL S.A.','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('032773032','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','AIRTAL*os sosp polv 30 bust 100 mg','8.14',null,null,'ALMIRALL S.A.','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('031220027','ACECLOFENAC 100MG 30 UNITA'' USO ORALE','GLADIO*os polv 30 bust 100 mg','8.33',null,null,'ABIOGEN PHARMA SpA','B5A',null,null,null);
INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES ('042403042','ACECLOFENAC 100MG 40 UNITA'' USO ORALE','ACECLOFENAC*40 cpr riv 100 mg','5.64',null,null,'ACCORD HEALTHCARE LIMITED','B5B',null,null,null);

--INSERT INTO FML.Medicine (AIC, Goup, NamePack, RetailPrice, ExFactoryPrice, MaxPrice, OwnerAIC, GroupCode, Transparency, Regionality, OxigenM3) VALUES (null,null,null,null,null,null,null,null,null,null,null);

--INSERIMENTO DATI SpecializationE (Specialization)


INSERT INTO FML.SpecializationE (Specialization) VALUES ('Allergologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anatomia Patologica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Andrologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anestesia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Angiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Audiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiochirurgia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chinesiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chiroplastica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia apparato digerente ed endoscopia dig.');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia della mano');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia generale');
--INSERT INTO FML.SpecializationE (Specialization) VALUES ('');

--INSERIMENTO DATI Exemption (ExemptionCode, Description)


INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E01','Malattia di Hansen');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E02','Malattia di Whipple');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E03','Malattia di Lyme');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E04','Tumore di Wilms');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E05','Retinoblastoma');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E06','Malattia di Cronkhite-Canada');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E07','Sindrome di Gardner');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E08','Poliposi Familiare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E09','Linfoangioleiomiomatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E10','Sindrome del nevo Basocellulare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E11','Neurofibromatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E12','Complesso Carney');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E13','Cancro non Poliposico Ereditario del Colon');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E14','Melanoma cutaneo familiare e/o multiplo');
--INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES (,);

--INSERIMENTO DATI Pathology (ICD9Code, ICD9CodePath, Description)
DELETE FROM FML.Pathology;

--PRIMA INSERISCO I QUATTRO 'PADRI'-------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Cholera',null,'001');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Paratyphoid',null,'002');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Salmonella',null,'003');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Shigella',null,'004');

--E POI INSERISCO I FIGLI-----------------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0010','Cholera','Cholera due to vibrio cholerae');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0011','Cholera','Cholera due to vibrio cholerae el tor');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0019','Cholera','Cholera unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0021','Paratyphoid','Paratyphoid A');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0022','Paratyphoid','Paratyphoid B');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0023','Paratyphoid','Paratyphoid C');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0029','Paratyphoid','Paratyphoid fever, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0030','Salmonella','Salmonella gastroenteritis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0031','Salmonella','Salmonella septicemia');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00320','Salmonella','Localized salmonella infection, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00321','Salmonella','meningitis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0041','Shigella','Shigella flexneri');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0043','Shigella','Shigella sonnei');
--INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES (null,null,'');

--POPOLAMENTO TABELLE RELATION

DELETE FROM FML.Acknowledge;
DELETE FROM FML.Analyzed;
DELETE FROM FML.Belong1;
DELETE FROM FML.Belong2;
DELETE FROM FML.Benefit;
DELETE FROM FML.Born;
DELETE FROM FML.Certificate;
DELETE FROM FML.Contain;
DELETE FROM FML.DoctorLive;
DELETE FROM FML.Due;
DELETE FROM FML.Execute;
DELETE FROM FML.GroupOf1;
DELETE FROM FML.GroupOf2;
DELETE FROM FML.Indicate1;
DELETE FROM FML.Indicate2;
DELETE FROM FML.Indicate3;
DELETE FROM FML.Indicate4;
DELETE FROM FML.JobR;
DELETE FROM FML.NoteR;
DELETE FROM FML.PatientLive;
DELETE FROM FML.PhotoR;
DELETE FROM FML.Register;
DELETE FROM FML.RelativeOf;
DELETE FROM FML.SpecializationR;
DELETE FROM FML.User;

--INSERIMENTO DATI Acknowledge (CounterID, ICD9Code)


INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('1','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('2','0029');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('3','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('7','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('9','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('10','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('11','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('13','00321');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('14','0031');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('15','00320');
--INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('','');

--INSERIMENTO DATI Analyzed (ResultID, CounterID)


INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('1','7');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('2','9');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('3','10');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('4','3');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('5','4');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('6','8');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('7','6');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('8','1');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('9','2');
INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('10','5');
--INSERT INTO FML.Analyzed (ResultID, CounterID) VALUES ('','');

--INSERIMENTO DATI Belong1 (Code, CodeB)


INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('001','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('002','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('004','B05');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('005','B02');
INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('006','B01');
--INSERT INTO FML.Belong1 (Code, CodeB) VALUES ('','');

--INSERIMENTO DATI Belong2 (Cvp, Code)


INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B03');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_6','B04');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.93.1_0','B03');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B02');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_4','B06');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_3','B01');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91_3','B06');
INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('03.91','B06');
--INSERT INTO FML.Belong2 (Cvp, Code) VALUES ('','');

--INSERIMENTO DATI Benefit (ExemptionCode, FiscalCode)


INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E11','GLVMRC75B03F205G');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E02','FRTFNC92M57M082E');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E04','FTNCLD97B57A906R');
INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('E06','LBRMRC67H03F205A');
--INSERT INTO FML.Benefit (ExemptionCode, FiscalCode) VALUES ('','');

--INSERIMENTO DATI Born (CityName, FiscalCode)


INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Monselice','SVLMHL73B57F205M');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Este','GLVMRC75B03F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','RZZNNA01B57F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','MNNSLV01B57A944W');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Spinea','FTNCLD97B57A906R');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Creazzo','BCCSFN97M17A944T');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Villorba','FRTFNC92M57M082E');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Zevio','TGLGCM86D11A944K');
--INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('','');

--INSERIMENTO DATI Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode(Doctor), FiscalCode2(Patient))


INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('1','2016-2-18 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('2','2016-3-24 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('3','2016-5-08 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('4','2016-6-01 17:25:06','',null,'MRNTTR69H03D612V','FRTFNC92M57M082E');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('5','2016-9-23 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('6','2016-12-07 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('7','2017-3-19 17:25:06','',null,'CVSRND64M15D969Y','SVLMHL73B57F205M');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('8','2017-4-05 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('9','2017-8-07 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('10','2018-5-01 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
--INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('',null,null,null,'','');

--INSERIMENTO DATI Contain (AIC, AtcSubCode)


INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('034208013','A02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('039716182','A01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('038835144','B01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('029532013','B01AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('039716170','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('038835043','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('026851028','B02AA');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('024155057','B02AB');
INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('031220027','A01AA');
--INSERT INTO FML.Contain (AIC, AtcSubCode) VALUES ('','');

--INSERIMENTO DATI DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)


INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','LBNLCN74D14D150X','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','CVSRND64M15D969Y','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Vittorio Veneto','BSTFRC76L57E897P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FRRMNL58D11L840P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Creazzo','CNTVCN62T18L840B','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','MRNTTR69H03D612V','Residenza',null,null,null);
--INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI Due (ICD9Code, CounterID)


INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0021','1');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0030','2');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0029','3');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0021','4');
INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('0030','5');
--INSERT INTO FML.Due (ICD9Code, CounterID) VALUES ('','');

--INSERIMENTO DATI Execute (FiscalCode, FiscalCode2, Description, ExDate)


INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','2/7/2016');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','PIPP','3/8/2016');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','ADP','25/2/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('FRRMNL58D11L840P','LBRMRC67H03F205A','ADP','29/3/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('MRNTTR69H03D612V','FRTFNC92M57M082E','PIPP','22/4/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','PIPP','1/6/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','11/9/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','RZZNNA01B57F205G','ADP','19/10/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('CVSRND64M15D969Y','SVLMHL73B57F205M','UVMD','20/12/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','UVMD','21/12/2017');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('LBNLCN74D14D150X','GLVMRC75B03F205G','ADP','3/2/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','MNNSLV01B57A944W','ADP','15/4/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('BSTFRC76L57E897P','RZZNNA01B57F205G','UVMD','17/4/2018');
INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('FRRMNL58D11L840P','LBRMRC67H03F205A','ADP','20/5/2018');
--INSERT INTO FML.Execute (FiscalCode, FiscalCode2, Description, ExDate) VALUES ('','','','');


--INSERIMENTO DATI GroupOf1 (Code, Cvp, Quantity)


INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1_2',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('001','02.93.1_3',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('002','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('002','02.93.1_2',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('003','02.93.1',null);
INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES ('003','03.8_0',null);
--INSERT INTO FML.GroupOf1 (Code, Cvp, Quantity) VALUES (null,null,null);

--INSERIMENTO DATI GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity)


INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D01','03.91_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D02','03.91_4',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','02.93.1_2',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','03.91_5',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D04','03.91_6',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D05','02.93.1_3',null,null);
INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES ('D06','02.93.1_3',null,null);
--INSERT INTO FML.GroupOf2 (Code, Cvp, MaxMultiplicity, Quantity) VALUES (null,null,null,null);

--INSERIMENTO DATI Indicate1 (AIC, CounterID, Quantity)


INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('039716182','2',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('039716182','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835144','2',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('029532013','5',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','7',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('029532013','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851028','6',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835043','6',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','1',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851028','11',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('038835043','10',null);
INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES ('026851016','8',null);
--INSERT INTO FML.Indicate1 (AIC, CounterID, Quantity) VALUES (null,null,null);


--INSERIMENTO DATI Indicate2 (Cvp, CounterID)


INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('02.93.1','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('02.93.1_3','2');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','7');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','10');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.8_0','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_3','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_3','5');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.91_5','6');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','2');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','4');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','10');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','1');
INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('03.93.1_0','5');
--INSERT INTO FML.Indicate2 (Cvp, CounterID) VALUES ('','');


--INSERIMENTO DATI Indicate3 (Code, CounterID)


INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','2');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','3');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('001','7');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('003','2');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('004','6');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('003','6');
INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('002','6');
--INSERT INTO FML.Indicate3 (Code, CounterID) VALUES ('','');

--INSERIMENTO DATI Indicate4 (CounterID, Code)


INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D02');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('1','D03');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('2','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D01');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D05');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D06');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('3','D02');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('4','D05');
INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('4','D03');
--INSERT INTO FML.Indicate4 (CounterID, Code) VALUES ('','');

--INSERIMENTO DATI JobR (Jobs, FiscalCode)


INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Geometra','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Muratore','GLVMRC75B03F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Architetto','SVLMHL73B57F205M');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Falegname','RZZNNA01B57F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Postino','MNNSLV01B57A944W');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FTNCLD97B57A906R');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Idraulico','BCCSFN97M17A944T');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FRTFNC92M57M082E');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','TGLGCM86D11A944K');
--INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('','');

--INSERIMENTO DATI NoteR (NoteID, Note, FiscalCode)


INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('1','Paziente da tenere sotto controllo','GLVMRC75B03F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('2','Controllare diabete spesso','RZZNNA01B57F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('3','Problemi familiari','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('4','Misurare sempre pressione','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('5','Sentire parenti','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('6','Burbero','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('7','Controllare spesso livello insulina','TGLGCM86D11A944K');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('8','Esami del sangue da fare','SVLMHL73B57F205M');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('9','Difficoltà nell''apprendimento','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('10','Problemi schiena','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('11','Contattare Familiari','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('12','Sentire precedente medico di base','FRTFNC92M57M082E');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('13','Trattamenti laser da fare','FRTFNC92M57M082E');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('','','');

--INSERIMENTO DATI PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)


INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','GLVMRC75B03F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Este','SVLMHL73B57F205M','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','RZZNNA01B57F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','MNNSLV01B57A944W','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FTNCLD97B57A906R','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','BCCSFN97M17A944T','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','FRTFNC92M57M082E','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Zevio','TGLGCM86D11A944K','Residenza',null,null,null);
--INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI PhotoR (PhotoID, PBlob, FiscalCode)


INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('1',null,'LBRMRC67H03F205A');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('2',null,'GLVMRC75B03F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('3',null,'SVLMHL73B57F205M');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('4',null,'RZZNNA01B57F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('5',null,'MNNSLV01B57A944W');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('6',null,'GLVMRC75B03F205G');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('7',null,'FTNCLD97B57A906R');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('8',null,'BCCSFN97M17A944T');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('9',null,'FRTFNC92M57M082E');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('10',null,'TGLGCM86D11A944K');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('11',null,'LBRMRC67H03F205A');
INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('12',null,'GLVMRC75B03F205G');
--INSERT INTO FML.PhotoR (PhotoID, PBlob, FiscalCode) VALUES ('',null,'');

--INSERIMENTO DATI Register (FiscalCode, FiscalCode2, TimeNumber, StartDate, EndDate)


INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','LBRMRC67H03F205A',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','GLVMRC75B03F205G',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','SVLMHL73B57F205M',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','RZZNNA01B57F205G',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','MNNSLV01B57A944W',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','FTNCLD97B57A906R',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','BCCSFN97M17A944T',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','FRTFNC92M57M082E',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM86D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM87D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM88D11A944K',DEFAULT,DEFAULT);
INSERT INTO FML.Register (FiscalCode, FiscalCode2, StartDate, EndDate) VALUES ('CNTVCN62T18L840B','TGLGCM89D11A944K',DEFAULT,DEFAULT);
--INSERT INTO FML.Register (FiscalCode, FiscalCode2, TimeNumber, StartDate, EndDate) VALUES (null,null,null,null,'null');


--INSERIMENTO DATI RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type)


INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('GLVMRC75B03F205G','MNNSLV01B57A944W',null,null,'Fratello');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('RZZNNA01B57F205G','BCCSFN97M17A944T',null,null,'Madre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('LBRMRC67H03F205A','MNNSLV01B57A944W',null,null,'Padre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('FRTFNC92M57M082E','TGLGCM86D11A944K',null,null,'Zia');
--INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('',null,null,'','');

--INSERIMENTO DATI SpecializationR (Specialization, FiscalCode)


INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Allergologia','LBNLCN74D14D150X');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Cardiochirurgia','CVSRND64M15D969Y');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Andrologia','BSTFRC76L57E897P');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Chiroplastica','FRRMNL58D11L840P');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Chirurgia generale','CNTVCN62T18L840B');
INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('Allergologia','MRNTTR69H03D612V');
--INSERT INTO FML.SpecializationR (Specialization, FiscalCode) VALUES ('','');

--INSERIMENTO DATI User (Username, Password, Email, FiscalCode)


INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('la29','34mte5','luc@libero.it','LBNLCN74D14D150X');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('arm44','ertino4','armcav@gmail.com','CVSRND64M15D969Y');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('fefe84','fede1985de','fed@libero.it','BSTFRC76L57E897P');
INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES ('vincenzoc','supersecret','vizo@libero.it','CNTVCN62T18L840B');

--INSERT INTO FML.User (Username, Password, Email, FiscalCode) VALUES (,,,);











 --File per impostare il db locale 
 
 --create a user that you want to use the database as: 
 CREATE ROLE webdb;
 
 --create the user for the web server to connect as: 
 CREATE ROLE webdbwebgui noinherit LOGIN PASSWORD 'webdb';
 
 --let webgui set role to neil: 
 GRANT webdb TO webdbwebgui;
 
 --add login 
 ALTER ROLE webdb WITH LOGIN;
 
 GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA fml TO webdb; 
 GRANT ALL PRIVILEGES ON schema fml to webdb; 
 GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA fml TO webdb;
 
  
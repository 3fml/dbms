 --File per impostare il db locale 
 
 --create a user that you want to use the database as: 
 CREATE ROLE webdb;
 
 --create the user for the web server to connect as: 
 CREATE ROLE webdbwebgui noinherit LOGIN PASSWORD 'webdb';
 
 --let webgui set role to neil: 
 GRANT webdb TO webdbwebgui;
 
 --add login 
 ALTER ROLE webdb WITH LOGIN;
 
 GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA fml TO webdb; 
 GRANT ALL PRIVILEGES ON schema fml to webdb; 
 GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA fml TO webdb;
 
  
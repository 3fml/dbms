# Lista cose da fare

### Interfaccia Grafica:
##### **NOTA:** I vari panelli e le varie grafiche si susseguono come con un albero che si dirama dalla radice (MainPanel) ed entra nei vari rami premento i bottoni sulla sinistra.

- **[Ricerca Avanzata]** Creare la grafica chiamata da "Ricerca Avanzata": 
Deve comprendenre la possibilit� di ricercare farmaci, malattie, cvp e un po' tutte le ricerche che si possono fare sul database e non riguardino uno specifico paziente.

- **[Cerca Paziente >  Imposta Dottore]** Creare la grafica chiamata da "Imposta Dottore":
Implementare la ricerca e selezione di un dottore da cui individuare il paziente.

- **[Cerca Paziente > Seleziona Paziete > Informazioni Paziete]**  Creare la grafica chiamata da "Informazioni Paziente":
Implementare una schermata dove � possibile vedere (magari anche in pi� pagine) tutte le informazioni anagrafiche legate al paziente.

- **[Cerca Paziente > Seleziona Paziete > Nuova Visita]** Creare la grafica chiamata da "Nuova Visita":
Gi� iniziato il pannello di controllo laterale, c'� da realizzare il panello d'azione di destra.

- **[Cerca Paziente > Seleziona Paziete > Nuova Visita > Prescizione]** Creare la grafica chiamata da "Prescizione":
Probabilmente sar� la stessa di "Crea Prescrizione" dove per� abbiamo gi� una visita a cui collegarla.

- **[Cerca Paziente > Seleziona Paziete > Nuova Visita > Misurazione]** Creare la grafica chiamata da "Misurazione"

- **[Cerca Paziente > Seleziona Paziete > Nuova Visita > Esame]** Creare la grafica chiamata da "Esane"

- **[Cerca Paziente > Seleziona Paziete > Nuova Prescrizione]** Creare la grafica chiamata da "Nuova Prescrizione":
Gi� iniziato il pannello di controllo laterale, c'� da realizzare il panello d'azione di destra.

- **[Cerca Paziente > Seleziona Paziete > Ricerca...]** Creare la grafica chiamata da "Ricerca...":
Gi� iniziato il pannello di controllo laterale, c'� da realizzare il panello d'azione di destra.

- **[Cerca Paziente > Seleziona Paziete > Ricerca... > Visita]** Creare la grafica chiamata da "Visita"

- **[Cerca Paziente > Seleziona Paziete > Ricerca... > Prescizione]** Creare la grafica chiamata da "Prescizione"

- **[Cerca Paziente > Seleziona Paziete > Ricerca... > Esame]** Creare la grafica chiamata da "Esame"

- **[Cerca Paziente > Seleziona Paziete > Ricerca... > Certificato]** Creare la grafica chiamata da "Certificato"

### Interazione con il Database:
##### **NOTA:** Non � ancora stata realizzata alcun tipo di interazione tra l'interfaccia e le classi jdbc. 
Quindi in pratica per ogni menu, sottomenu, bottone, pannello o altro, bisogna realizzare la relativa operazione sul database. 
Per brevit� non riporto i punti (quando saranno meno li metteremo per tenerne traccia)
#
#
### Classi JDBC
##### **NOTA:** Al momento non sono presenti punti, una volta finita l'interfaccia avremo pi� chiaro i metodi mancanti da implementare.
## Database Management Systems Project of 3FML group

In particular this repository regard the fourth homework and the final project of the course.
Here a rough list of the different parts:

1. **SQL** direcotry contain the physical schema and the random-generate file with the patients that populate the schema
2. **CommandMe** directory containt a maven java project with a very basic interface and the jdbc classes for accessing the Database

---

## Updates

Cause the frequent commits, this file may be found not updated with the content of the repository.

---

## Components of the group:

1. Agostini Francesco
2. Cavaliere Francesco
3. Fabbrizio Francesco Maria
4. Gnesotto Marco
5. Zanetti Luca

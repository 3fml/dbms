--INSERIMENTO DATI Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS)
DELETE FROM FML.Patient;

INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('LBRMRC67H03F205A','Marco','Alberti','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('GLVMRC75B03F205G','Marco','Galvari','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('SVLMHL73B57F205M','Michela','Savilli','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('RZZNNA01B57F205G','Anna','Rozzulla','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('MNNSLV01B57A944W','Silvia','Mannati','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FTNCLD97B57A906R','Claudia','Fatin','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('BCCSFN97M17A944T','Buccio','Stefano','m',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('FRTFNC92M57M082E','Francesca','Fertona','f',null,null,null,null,null,null,null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM86D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1986-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM87D11A944K','Giacomo','Tiglio','m',null,null,null,null,null,'1987-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM88D11A944K','Giacomo','Tiglioso','m',null,null,null,null,null,'1988-04-11',null);
INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('TGLGCM89D11A944K','Giacomo','Tigliatto','m',null,null,null,null,null,'1989-04-11',null);

--INSERT INTO FML.Patient (FiscalCode, Name, Surname, Gender, Telephone, Cellphone, email, userStatus, Nationality, DoB, ULSS) VALUES ('','','','',,,,,,,);

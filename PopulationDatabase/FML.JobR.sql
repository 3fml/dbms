--INSERIMENTO DATI JobR (Jobs, FiscalCode)
DELETE FROM FML.JobR;

INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Geometra','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','LBRMRC67H03F205A');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Muratore','GLVMRC75B03F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Architetto','SVLMHL73B57F205M');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Falegname','RZZNNA01B57F205G');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Postino','MNNSLV01B57A944W');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FTNCLD97B57A906R');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Idraulico','BCCSFN97M17A944T');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Impiegato','FRTFNC92M57M082E');
INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('Ingegnere','TGLGCM86D11A944K');
--INSERT INTO FML.JobR (Jobs, FiscalCode) VALUES ('','');


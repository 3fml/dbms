--INSERIMENTO DATI City (CityName, Provname, Name, MetropolCityCode)
DELETE FROM FML.City;

INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Monselice','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Este','Padova','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Dolo','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Spinea','Venezia','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Bassano del Grappa','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Vittorio Veneto','Belluno','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Asiago','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Creazzo','Vicenza','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villadose','Rovigo','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Zevio','Verona','Italia',null);
INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('Villorba','Treviso','Italia',null);
--INSERT INTO FML.City (CityName, Provname, Name, MetropolCityCode) VALUES ('','',''null);

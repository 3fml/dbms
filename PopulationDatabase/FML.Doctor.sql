--INSERIMENTO DATI Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo)
DELETE FROM FML.Doctor;

INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('LBNLCN74D14D150X','IT49996995991',null,null,'V4764','15487','Luciano','Albino','m','0492568','3325698445','luc@libero.it',null);

INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CVSRND64M15D969Y','IT19939955999',null,null,'R3737','12368','Armando','Cavestro','m',04955587,3751426548,'armcav@gmail.com',null);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('BSTFRC76L57E897P','IT12339755990',null,null,'F4857','18725','Federica','Busetta','f',04988722,3256497595,'fefe@outlook.it',null);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('FRRMNL58D11L840P','IT74339754090',null,null,'T4846','14247','Manuela','Ferrari','f',0498887132,3295587864,'manfe@gmail.com',null);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('CNTVCN62T18L840B','IT74329751091',null,null,'E8773','19822','Vincenzo','Cantone','m',049256687,3226486472,'vizo@libero.it',null);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('MRNTTR69H03D612V','IT34123758071',null,null,'A0628','16982','Ettore','Marino','m',049871654,3275511422,'mari@outlook.it',null);
INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES ('GCMMLO67H03D511V','IT34456290948',0800,'VI','A0682','16998','Giacomo','Melito','m',049458654,3202311422,'meli@outlook.it',null);



--INSERT INTO FML.Doctor (FiscalCode, VATnumber, ASL, District, RegionalCode, OrderNo, Name, Surname, Gender, Telephone, Cellphone, Email, PatientNo) VALUES (,,,,,,,,,,,,);


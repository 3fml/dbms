--INSERIMENTO DATI Measure (CounterID, ValueMeasured, Descriptor)
DELETE FROM FML.Measure;

INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','74.0','Peso');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','175.2','Altezza');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('1','24.11','BMI');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('3','56.74','Circonferenza Vita');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','87','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','0','Aritmia');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('5','100','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','65','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('7','102','Valore del Glucosio nel sangue');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','98','Pressione Massima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','55','Pressione Minima');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','68','Frequenza Cardiaca');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','0.9','INR');
INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('10','59.8','Peso');
--INSERT INTO FML.Measure (CounterID, ValueMeasured, Descriptor) VALUES ('','','');

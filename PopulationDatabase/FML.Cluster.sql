--INSERIMENTO DATI CLUster (Code, Description, Weight, PrescriptionRules)
DELETE FROM FML.Cluster;

INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('001','Gruppo1','4','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('002','Gruppo2','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('003','Gruppo3','2','Entro 30 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('004','Gruppo4','3','Entro 60 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('005','Gruppo5','3','Entro 45 giorni');
INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('006','Gruppo6','3','Entro 45 giorni');


--INSERT INTO FML.Cluster (Code, Description, Weight, PrescriptionRules) VALUES ('','','','');


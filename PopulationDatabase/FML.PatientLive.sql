--INSERIMENTO DATI PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)
DELETE FROM FML.PatientLive;

INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','GLVMRC75B03F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Este','SVLMHL73B57F205M','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','RZZNNA01B57F205G','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','MNNSLV01B57A944W','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FTNCLD97B57A906R','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','BCCSFN97M17A944T','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','FRTFNC92M57M082E','Residenza',null,null,null);
INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Zevio','TGLGCM86D11A944K','Residenza',null,null,null);
--INSERT INTO FML.PatientLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI Acknowledge (CounterID, ICD9Code)
DELETE FROM FML.Acknowledge;

INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('1','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('2','0029');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('3','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('7','0021');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('9','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('10','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('11','0030');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('13','00321');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('14','0031');
INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('15','00320');

--INSERT INTO FML.Acknowledge (CounterID, ICD9Code) VALUES ('','');

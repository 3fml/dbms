--INSERIMENTO DATI Pathology (ICD9Code, ICD9CodePath, Description)
DELETE FROM FML.Pathology;

--PRIMA INSERISCO I QUATTRO 'PADRI'-------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Cholera',null,'001');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Paratyphoid',null,'002');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Salmonella',null,'003');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('Shigella',null,'004');

--E POI INSERISCO I FIGLI-----------------------
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0010','Cholera','Cholera due to vibrio cholerae');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0011','Cholera','Cholera due to vibrio cholerae el tor');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0019','Cholera','Cholera unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0021','Paratyphoid','Paratyphoid A');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0022','Paratyphoid','Paratyphoid B');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0023','Paratyphoid','Paratyphoid C');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0029','Paratyphoid','Paratyphoid fever, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0030','Salmonella','Salmonella gastroenteritis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0031','Salmonella','Salmonella septicemia');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00320','Salmonella','Localized salmonella infection, unspecified');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('00321','Salmonella','meningitis');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0041','Shigella','Shigella flexneri');
INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES ('0043','Shigella','Shigella sonnei');
--INSERT INTO FML.Pathology (ICD9Code, ICD9CodePath, Description) VALUES (null,null,'');

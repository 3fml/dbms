--INSERIMENTO DATI NoteR (NoteID, Note, FiscalCode)
DELETE FROM FML.NoteR;

INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('1','Paziente da tenere sotto controllo','GLVMRC75B03F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('2','Controllare diabete spesso','RZZNNA01B57F205G');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('3','Problemi familiari','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('4','Misurare sempre pressione','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('5','Sentire parenti','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('6','Burbero','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('7','Controllare spesso livello insulina','TGLGCM86D11A944K');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('8','Esami del sangue da fare','SVLMHL73B57F205M');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('9','Difficoltà nell''apprendimento','LBRMRC67H03F205A');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('10','Problemi schiena','FTNCLD97B57A906R');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('11','Contattare Familiari','BCCSFN97M17A944T');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('12','Sentire precedente medico di base','FRTFNC92M57M082E');
INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('13','Trattamenti laser da fare','FRTFNC92M57M082E');
--INSERT INTO FML.NoteR (NoteID, Note, FiscalCode) VALUES ('','','');

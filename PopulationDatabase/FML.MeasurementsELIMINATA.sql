--INSERIMENTO DATI Measurements (ValueMeasured, Descriptor)
DELETE FROM FML.Measurements;

INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('74.0','Peso');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('175.2','Altezza');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('24.11','BMI');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('56.74','Circonferenza Vita');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('87','Frequenza Cardiaca');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('0','Aritmia');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('100','Pressione Massima');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('65','Frequenza Cardiaca');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('102','Valore del Glucosio nel sangue');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('98','Pressione Massima');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('55','Pressione Minima');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('68','Frequenza Cardiaca');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('0.9','INR');
INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('59.8','Peso');

--INSERT INTO FML.Measurements (ValueMeasured, Descriptor) VALUES ('','');

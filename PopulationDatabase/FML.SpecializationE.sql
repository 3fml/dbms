--INSERIMENTO DATI SpecializationE (Specialization)
DELETE FROM FML.SpecializationE;

INSERT INTO FML.SpecializationE (Specialization) VALUES ('Allergologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anatomia Patologica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Andrologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Anestesia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Angiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Audiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiochirurgia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Cardiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chinesiologia');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chiroplastica');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia apparato digerente ed endoscopia dig.');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia della mano');
INSERT INTO FML.SpecializationE (Specialization) VALUES ('Chirurgia generale');
--INSERT INTO FML.SpecializationE (Specialization) VALUES ('');

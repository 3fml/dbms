--INSERIMENTO DATI Exemption (ExemptionCode, Description)
DELETE FROM FML.Exemption;

INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E01','Malattia di Hansen');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E02','Malattia di Whipple');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E03','Malattia di Lyme');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E04','Tumore di Wilms');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E05','Retinoblastoma');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E06','Malattia di Cronkhite-Canada');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E07','Sindrome di Gardner');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E08','Poliposi Familiare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E09','Linfoangioleiomiomatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E10','Sindrome del nevo Basocellulare');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E11','Neurofibromatosi');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E12','Complesso Carney');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E13','Cancro non Poliposico Ereditario del Colon');
INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES ('E14','Melanoma cutaneo familiare e/o multiplo');
--INSERT INTO FML.Exemption (ExemptionCode, Description) VALUES (,);



--INSERIMENTO DATI AtcCathegory (AtcSubCode, AtcSubCodeAtc, Description)
DELETE FROM FML.AtcCategory;

INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A',null,'Alimentary tract and metabolism');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B',null,'Blood and blood forming organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('C',null,'Cardiovascular system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('D',null,'Dermatologicals');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('G',null,'Genito-urinary system and sex hormones');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('H',null,'Systemic hormonal preparations, excluding sex hormones and insulins');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('J',null,'Antiinfectives for systemic use');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('L',null,'Antineoplastic and immunomodulating agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('M',null,'Musculo-skeletal system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('N',null,'Nervous system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('P',null,'Antiparasitic products, insecticides and repellents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('R',null,'	Respiratory system');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('S',null,'Sensory organs');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('V',null,'Various');

INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01','A','Stomatological preparations');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02','A','Drugs for acid related disorders');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01','B','Antithrombotic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02','B','Antihemorrhagics');


INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AA','A01','Caries prophylactic agents');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A01AB','A01','Anti-infectives and antiseptics for local oral treatment');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AA','A02','Magnesium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('A02AB','A02','Aluminium compounds');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AA','B01','Vitamin K antagonists');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B01AB','B01','Heparin group');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AA','B02','Amino acids');
INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('B02AB','B02','Proteinase inhibitors');

--INSERT INTO FML.AtcCategory (AtcSubCode, AtcSubCodeAtc, Description) VALUES ('',null,'');


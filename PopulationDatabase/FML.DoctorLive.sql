--INSERIMENTO DATI DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode)
DELETE FROM FML.DoctorLive;

INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','LBNLCN74D14D150X','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Monselice','CVSRND64M15D969Y','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Vittorio Veneto','BSTFRC76L57E897P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Dolo','FRRMNL58D11L840P','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Creazzo','CNTVCN62T18L840B','Residenza',null,null,null);
INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES ('Villorba','MRNTTR69H03D612V','Residenza',null,null,null);
--INSERT INTO FML.DoctorLive (CityName, FiscalCode, ResidencyDomicile, Street, CivicNumber, PostalCode) VALUES (null,null,null,null,null,null);

--INSERIMENTO DATI Nation
DELETE FROM FML.Nation;

INSERT INTO FML.Nation VALUES ('Italia');
INSERT INTO FML.Nation VALUES ('Francia');
INSERT INTO FML.Nation VALUES ('Germania');
INSERT INTO FML.Nation VALUES ('Russia');
INSERT INTO FML.Nation VALUES ('Brasile');
INSERT INTO FML.Nation VALUES ('Corea del Nord');
INSERT INTO FML.Nation VALUES ('Cina');
INSERT INTO FML.Nation VALUES ('Ucraina');
INSERT INTO FML.Nation VALUES ('Spagna');
INSERT INTO FML.Nation VALUES ('Slovenia');
INSERT INTO FML.Nation VALUES ('Austria');
INSERT INTO FML.Nation VALUES ('Svizzera');
INSERT INTO FML.Nation VALUES ('Finlandia');
INSERT INTO FML.Nation VALUES ('Portogallo');

--INSERT INTO FML.Nation VALUES ('');

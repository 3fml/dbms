--INSERIMENTO DATI Born (CityName, FiscalCode)
DELETE FROM FML.Born;

INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Monselice','SVLMHL73B57F205M');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Este','GLVMRC75B03F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','RZZNNA01B57F205G');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Dolo','MNNSLV01B57A944W');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Spinea','FTNCLD97B57A906R');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Creazzo','BCCSFN97M17A944T');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Villorba','FRTFNC92M57M082E');
INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('Zevio','TGLGCM86D11A944K');
--INSERT INTO FML.Born (CityName, FiscalCode) VALUES ('','');


--INSERIMENTO DATI RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type)
DELETE FROM FML.RelativeOf;

INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('GLVMRC75B03F205G','MNNSLV01B57A944W',null,null,'Fratello');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('RZZNNA01B57F205G','BCCSFN97M17A944T',null,null,'Madre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('LBRMRC67H03F205A','MNNSLV01B57A944W',null,null,'Padre');
INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('FRTFNC92M57M082E','TGLGCM86D11A944K',null,null,'Zia');
--INSERT INTO FML.RelativeOf (FiscalCode, FiscalCode2, StartDate, EndDate, Type) VALUES ('',null,null,'','');

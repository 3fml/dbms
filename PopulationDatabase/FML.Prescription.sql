--INSERIMENTO DATI Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID)
DELETE FROM FML.Prescription;

INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('1','Rossa',null,null,'5');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('2','Rossa',null,null,'7');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('3','Bianca',null,null,'3');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('4','Elettronica',null,null,'10');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('5','Bianca',null,null,'1');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('6','Rossa',null,null,'4');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('7','Bianca',null,null,'6');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('8','Bianca','Elimina latte dalla dieta',null,'13');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('9','Rossa','Cure Termali',null,'14');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('10','Bianca','Bicarbonato',null,'15');
INSERT INTO FML.Prescription (CounterID,Color, Notes, Priority, VisitCounterID) VALUES ('11','Elettronica','Limone e peperoncino prima di colazione','Urgente','16');
--INSERT INTO FML.Prescription (CounterID, CounterID,Color, Notes, Priority, VisitCounterID) VALUES (null,null,null,null,null);

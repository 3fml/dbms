--INSERIMENTO DATI JobE (Jobs)
DELETE FROM FML.JobE;

INSERT INTO FML.JobE (Jobs) VALUES ('Muratore');
INSERT INTO FML.JobE (Jobs) VALUES ('Geometra');
INSERT INTO FML.JobE (Jobs) VALUES ('Ingegnere');
INSERT INTO FML.JobE (Jobs) VALUES ('Architetto');
INSERT INTO FML.JobE (Jobs) VALUES ('Falegname');
INSERT INTO FML.JobE (Jobs) VALUES ('Dottore');
INSERT INTO FML.JobE (Jobs) VALUES ('Insegnante');
INSERT INTO FML.JobE (Jobs) VALUES ('Professore');
INSERT INTO FML.JobE (Jobs) VALUES ('Bidello');
INSERT INTO FML.JobE (Jobs) VALUES ('Idraulico');
INSERT INTO FML.JobE (Jobs) VALUES ('Impresario');
INSERT INTO FML.JobE (Jobs) VALUES ('Banchiere');
INSERT INTO FML.JobE (Jobs) VALUES ('Impiegato');
INSERT INTO FML.JobE (Jobs) VALUES ('Postino');

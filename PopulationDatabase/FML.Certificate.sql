--INSERIMENTO DATI Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode(Doctor), FiscalCode2(Patient))
DELETE FROM FML.Certificate;

INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('1','2016-2-18 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('2','2016-3-24 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('3','2016-5-08 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('4','2016-6-01 17:25:06','',null,'MRNTTR69H03D612V','FRTFNC92M57M082E');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('5','2016-9-23 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('6','2016-12-07 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('7','2017-3-19 17:25:06','',null,'CVSRND64M15D969Y','SVLMHL73B57F205M');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('8','2017-4-05 17:25:06','',null,'MRNTTR69H03D612V','FTNCLD97B57A906R');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('9','2017-8-07 17:25:06','',null,'FRRMNL58D11L840P','LBRMRC67H03F205A');
INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('10','2018-5-01 17:25:06','',null,'LBNLCN74D14D150X','GLVMRC75B03F205G');
--INSERT INTO FML.Certificate (CertificateID, CertificateDate, Description, Document, FiscalCode, FiscalCode2) VALUES ('',null,null,null,'','');

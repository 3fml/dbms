--INSERIMENTO DATI Province (ProvName, Abbreviation, Name)
DELETE FROM FML.Province;

INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Padova','PD','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Venezia','VE','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Rovigo','RO','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Verona','VR','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Treviso','TV','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Vicenza','VI','Italia');
INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('Belluno','BL','Italia');
--INSERT INTO FML.Province (ProvName, Abbreviation, Name) VALUES ('','','');

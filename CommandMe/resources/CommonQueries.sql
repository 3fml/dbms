SELECT * FROM  fml.patient WHERE UPPER(name || ' ' || surname) LIKE UPPER(?) 
    AND  UPPER(name || ' ' || surname) LIKE UPPER(?) 
ORDER BY name ASC;


WITH sub AS ( 
        SELECT Pr.*, V.visittimestamp, D.name, D.surname, D.fiscalcode FROM  fml.visit AS V 
        INNER JOIN  fml.prescription AS Pr ON Pr.visitcounterid = V.counterid 
        INNER JOIN  fml.doctor AS D ON D.fiscalcode = V.fiscalcode2 
        WHERE ? = V.fiscalcode 
        ) 
SELECT * FROM (SELECT *, NULL AS description, 'Custom' AS type, NULL AS id FROM sub 
        UNION 
        SELECT sub.*, cvp.descriptionNtr, 'cvp', cvp.cvp 
        FROM sub NATURAL JOIN  fml.indicate2 AS I2 NATURAL JOIN  fml.cvp 
        UNION 
        SELECT sub.*, cluster.description, 'cluster', cluster.code 
        FROM sub 
        NATURAL JOIN  fml.indicate3 AS I3 
        NATURAL JOIN  fml.cluster 
        UNION 
        SELECT sub.*, dayservice.description, 'dayservice', dayservice.code 
        FROM sub 
        NATURAL JOIN  fml.indicate4 AS I4 
        NATURAL JOIN  fml.dayservice 
        UNION 
        SELECT sub.*, medicine.namepack, 'medicine', medicine.aic 
        FROM sub 
        NATURAL JOIN  fml.indicate1 AS I1 
        NATURAL JOIN  fml.medicine 
        ORDER BY visittimestamp DESC ) AS U 
 WHERE  ? < U.visittimestamp::date 
    AND ? >  U.visittimestamp::date;


SELECT * FROM   fml.pathology As Pa 
NATURAL JOIN   fml.acknowledge AS Ak 
NATURAL JOIN   fml.visit AS V 
WHERE ? = V.fiscalcode 
    AND  ? < visittimestamp::date 
    AND ? >  visittimestamp::date 
ORDER BY visittimestamp DESC;


SELECT V.*, D.name, D.surname FROM  fml.visit As V 
INNER JOIN  fml.doctor AS D ON V.fiscalcode2 = D.fiscalcode 
WHERE ? = V.fiscalcode 
    AND  ? < V.visittimestamp::date 
    AND ? >  V.visittimestamp::date 
ORDER BY V.visittimestamp DESC;


SELECT Ex.*, D.name, D.surname, D.fiscalcode FROM  fml.execute AS Ex 
NATURAL JOIN  fml.doctor AS D 
WHERE ? = Ex.fiscalcode2 
    AND  ? < Ex.exdate 
    AND ? > Ex.exdate 
ORDER BY Ex.exdate DESC; 

SELECT EX.* FROM   fml.examresults AS EX 
WHERE ? = EX.fiscalcode 
    AND  ? < EX.examdate 
    AND ? > EX.examdate 
ORDER BY EX.examdate DESC; 

SELECT P.*, Reg.startdate, Reg.enddate, Reg.timenumber FROM   fml.register AS Reg 
INNER JOIN  fml.patient AS P ON P.fiscalcode = Reg.fiscalcode2 
WHERE ? = Reg.fiscalcode AND Reg.enddate IS NULL 
ORDER BY P.surname ASC; 

    INSERT INTO FML.Visit (CounterID, VisitNotes, VisitTimestamp, FiscalCode, FiscalCode2) VALUES ('1',null,'2016-05-12 17:25:06','LBRMRC67H03F205A','FRRMNL58D11L840P');






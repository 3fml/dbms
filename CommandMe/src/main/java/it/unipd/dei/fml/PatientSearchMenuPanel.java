package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class PatientSearchMenuPanel extends BackAndHomePanel {

	private PatientSearchMenuFrame myFrame;

	/**
	 * Create the panel.
	 */
	public PatientSearchMenuPanel(final MainWindowHDC parent) {
		super(parent);
		myFrame = new PatientSearchMenuFrame();
		super.setFrame(myFrame);

		JButton btnNewButton = new JButton("Visita");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});

		JLabel lblCerca = new JLabel("Cerca:");
		add(lblCerca, "2, 1");
		add(btnNewButton, "2, 2");

		JButton btnNewButton_1 = new JButton("Prescrizione");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		add(btnNewButton_1, "2, 4");

		JButton btnNewButton_2 = new JButton("Esame");
		add(btnNewButton_2, "2, 6");

		JButton btnNewButton_3 = new JButton("Certificato");
		btnNewButton_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});
		add(btnNewButton_3, "2, 8");
	}

	@Override
	public void clean() {
		// System.out.println(this.toString().substring(0, 30)+"\n");

	}

}

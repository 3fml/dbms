package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class PatientInformationPanel extends PanelWithPatient {

	private AllVisitOfPatientPanel allVisitOfPatientPanel = null;
	private PatientInformationFrame myFrame;

	public PatientInformationPanel() {
		this(null, new PatientRow());
	}

	/**
	 * PANNELLO CONTENENTE TUTTI I COMANDI PER VEDERE LE VARIE INFORMAZIONI SUL
	 * PAZIENTE.
	 */
	public PatientInformationPanel(final MainWindowHDC parent, final PatientRow patient) {
		super(parent, patient);
		myFrame = new PatientInformationFrame(this);
		super.setFrame(myFrame);

		JButton btnVisiteFatte = new JButton("Visite Fatte");
		btnVisiteFatte.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(allVisitOfPatientPanel = new AllVisitOfPatientPanel(parent, patient));
			}
		});
		add(btnVisiteFatte, "2, 2");

		JButton btnPrescrizioniOrdinate = new JButton("Prescrizioni Ordinate");
		add(btnPrescrizioniOrdinate, "2, 4");

		JButton btnMedicinePrescritte = new JButton("Medicine Prescritte");
		add(btnMedicinePrescritte, "2, 6");

		JButton btnNote = new JButton("Note");
		add(btnNote, "2, 8");

	}

	@Override
	public void clean() {

		if (allVisitOfPatientPanel != null) {
			allVisitOfPatientPanel.clean();
			allVisitOfPatientPanel = null;
		}
	}
}

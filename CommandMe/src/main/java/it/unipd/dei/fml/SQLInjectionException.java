package it.unipd.dei.fml;

/**
 * Exception throwed when there is the risck of SQL injection
 */
public class SQLInjectionException extends Exception {
	/**
	 * Constructor
	 */
	public SQLInjectionException() {
	}

	/**
	 * Store a message with the exception
	 *
	 * @param message
	 */
	public SQLInjectionException(String message) {
		super(message);
	}
}
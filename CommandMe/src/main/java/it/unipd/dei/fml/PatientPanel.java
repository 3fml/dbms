package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class PatientPanel extends PanelWithPatient {

	private BackAndHomePanel backOnlyPanel = null;
	private PatientInformationPanel patientInformationPanel = null;
	private PatientSearchMenuPanel patientSearchMenuPanel = null;
	private JButton btnNuovaPrescrizione;
	private NewVisitPanel newVisitPanel = null;
	private NewPrescriptionPanel newPrescriptionPanel = null;
	private PatientFrame myFrame;

	public PatientPanel() {
		this(null, new PatientRow());
	}

	/**
	 * Pannello con le azioni mostrate all'avvio della finestra principale.
	 * 
	 */
	public PatientPanel(final MainWindowHDC parent, final PatientRow patient) {
		super(parent, patient);
		myFrame = new PatientFrame(this);
		super.setFrame(myFrame);

		JLabel lblNewLabel = new JLabel("Menu Paziente:");
		lblNewLabel.setVerticalTextPosition(SwingConstants.TOP);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		this.add(lblNewLabel, "2, 1");

		btnNuovaPrescrizione = new JButton("Nuova Prescrizione");
		btnNuovaPrescrizione.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(newPrescriptionPanel = new NewPrescriptionPanel(parent, patient));
			}
		});

		JButton btnNuovaVisita = new JButton("Nuova Visita");
		btnNuovaVisita.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(newVisitPanel = new NewVisitPanel(parent, patient));
			}
		});

		JButton btnInformazioniPaziente = new JButton("Informazioni Paziente");
		btnInformazioniPaziente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(patientInformationPanel = new PatientInformationPanel(parent, patient));
			}
		});
		this.add(btnInformazioniPaziente, "2, 2, fill, fill");
		this.add(btnNuovaVisita, "2, 4, fill, fill");
		this.add(btnNuovaPrescrizione, "2, 6, fill, fill");

		JButton btnRicerca = new JButton("Ricerca...");
		btnRicerca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(patientSearchMenuPanel = new PatientSearchMenuPanel(parent));
			}
		});
		add(btnRicerca, "2, 8");

	}

	@Override
	public void clean() {
		// System.out.println(this.toString().substring(0, 30)+"\n");
		if (backOnlyPanel != null) {
			backOnlyPanel.clean();
			backOnlyPanel = null;
		}

		if (patientSearchMenuPanel != null) {
			patientSearchMenuPanel.clean();
			patientSearchMenuPanel = null;
		}
		if (newVisitPanel != null) {
			newVisitPanel.clean();
			newVisitPanel = null;
		}

		if (newPrescriptionPanel != null) {
			newPrescriptionPanel.clean();
			newPrescriptionPanel = null;
		}

		if (patientInformationPanel != null) {
			patientInformationPanel.clean();
			patientInformationPanel = null;
		}

	}

}

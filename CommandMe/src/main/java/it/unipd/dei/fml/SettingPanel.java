package it.unipd.dei.fml;

public class SettingPanel extends BackAndHomePanel {

	private SettingFrame myFrame;

	public SettingPanel(final MainWindowHDC parent) {
		super(parent);
		myFrame = new SettingFrame();
		super.setFrame(myFrame);
	}
}

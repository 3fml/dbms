package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JLabel;

public class NewPatientPanel extends BackAndHomePanel {
	private JButton btnSalva;
	private NewPatientFrame myFrame;

	/**
	 * Create the panel.
	 */
	public NewPatientPanel(final MainWindowHDC parent) {
		super(parent);

		btnSalva = new JButton("Salva");
		btnSalva.setEnabled(false);
		btnSalva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					myFrame.salva();
					clean();
					parent.goBack();
				} catch (Exception e1) {
					//Donothing
				}
				
			}
		});
		
		JLabel lblInserimentoNuovoPaziente = new JLabel("Inserimento Nuovo Paziente:");
		add(lblInserimentoNuovoPaziente, "2, 1");
		btnSalva.setBackground(new Color(60, 179, 113));
		add(btnSalva, "2, 2");
		myFrame = new NewPatientFrame(this);
		super.setFrame(myFrame);
	}

	public void activateSalva(boolean b) {
		btnSalva.setEnabled(b);

	}
}

package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class NewPrescriptionPanel extends PanelWithPatient {

	private NewPrescriptionFrame myFrame;
	private Visit visit;

	public NewPrescriptionPanel() {
		this(null, new PatientRow());
	}

	/**
	 * Create the panel.
	 */
	public NewPrescriptionPanel(final MainWindowHDC parent, PatientRow patient) {
		super(parent, patient);
		myFrame = new NewPrescriptionFrame();
		visit = Visit.getIstance();
		super.setFrame(myFrame);

		JButton btnSalva = new JButton("Salva");
		btnSalva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.salva();
				clean();
				parent.goBack();
			}
		});
		
		JLabel lblInserimentoPrescrizione = new JLabel("Inserimento Prescrizione:");
		add(lblInserimentoPrescrizione, "2, 1");

		btnSalva.setBackground(new Color(60, 179, 113));
		add(btnSalva, "2, 2");
	}

}

package it.unipd.dei.fml;

public class AdvanceSearchPanel extends BackAndHomePanel {
	private AdvanceSearchFrame myFrame;

	public AdvanceSearchPanel(final MainWindowHDC parent) {
		super(parent);
		myFrame = new AdvanceSearchFrame();
		super.setFrame(myFrame);
	}

}

package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class NewUserWindowHDC extends JFrame {

	private JPanel contentPane;
	private JPanel panel_1;
	private JPanel panel;
	private JLabel label;
	private JTextField textField;
	private JLabel label_1;
	private JTextField textField_1;
	private JLabel label_2;
	private JTextField textField_2;
	private JLabel label_3;
	private JTextField textField_3;
	private JLabel label_4;
	private JTextField textField_4;
	private JLabel label_5;
	private JTextField textField_5;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel label_8;
	private JTextField textField_6;
	private JLabel label_9;
	private JTextField textField_7;
	private JLabel label_10;
	private JComboBox comboBox;
	private JFormattedTextField formattedTextField;
	private JFormattedTextField formattedTextField_1;
	private JLabel label_11;
	private JTextField textField_8;
	private JLabel label_12;
	private JTextField textField_9;
	private JLabel label_13;
	private JTextField textField_10;
	private JLabel lblNCivico;
	private JTextField textField_11;
	private JPanel panel_2;
	private JLabel label_15;
	private JComboBox comboBox_1;
	private JTextField textField_12;
	private JLabel label_16;
	private JTextField textField_13;
	private JLabel label_17;
	private JTextField textField_14;
	private JTextField textField_15;
	private JLabel label_18;
	private JLabel label_19;
	private JButton button;
	private JButton btnAnnulla;
	private JButton btnNewButton;
	private Component parent;

	/**
	 * Finestra per la registrazione di un nuovo utente
	 * 
	 */
	public NewUserWindowHDC(final Component parent) {
		this.parent = parent;

		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		setTitle("Nuovo Utente");
		setLocationByPlatform(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 696);
		contentPane = new JPanel();
		contentPane.setAutoscrolls(true);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dati Anagrafici",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 130, 371, 295);
		contentPane.add(panel_1);

		label_3 = new JLabel("Nome:");
		label_3.setBounds(188, 21, 171, 14);
		panel_1.add(label_3);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(188, 35, 171, 20);
		panel_1.add(textField_3);

		label_4 = new JLabel("Cognome:");
		label_4.setBounds(10, 21, 167, 14);
		panel_1.add(label_4);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(10, 35, 167, 20);
		panel_1.add(textField_4);

		label_5 = new JLabel("Codice Fiscale:");
		label_5.setBounds(10, 66, 238, 14);
		panel_1.add(label_5);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(10, 80, 238, 20);
		panel_1.add(textField_5);

		label_6 = new JLabel("Genere:");
		label_6.setBounds(258, 65, 100, 14);
		panel_1.add(label_6);

		label_7 = new JLabel("Telefono:");
		label_7.setBounds(10, 111, 167, 14);
		panel_1.add(label_7);

		label_8 = new JLabel("Cellulare:");
		label_8.setBounds(187, 111, 171, 14);
		panel_1.add(label_8);

		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(10, 218, 263, 20);
		panel_1.add(textField_6);

		label_9 = new JLabel("Indirizzo:");
		label_9.setBounds(10, 203, 263, 14);
		panel_1.add(label_9);

		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(10, 172, 348, 20);
		panel_1.add(textField_7);

		label_10 = new JLabel("Email:");
		label_10.setBounds(10, 157, 348, 14);
		panel_1.add(label_10);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "--Seleziona--", "Maschio", "Femmina", "Altro" }));
		comboBox.setBounds(258, 80, 100, 20);
		panel_1.add(comboBox);

		formattedTextField = new JFormattedTextField();
		formattedTextField.setBounds(10, 126, 167, 20);
		panel_1.add(formattedTextField);

		formattedTextField_1 = new JFormattedTextField();
		formattedTextField_1.setBounds(187, 126, 171, 20);
		panel_1.add(formattedTextField_1);

		label_11 = new JLabel("Città:");
		label_11.setBounds(10, 249, 167, 14);
		panel_1.add(label_11);

		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(10, 264, 167, 20);
		panel_1.add(textField_8);

		label_12 = new JLabel("CAP:");
		label_12.setBounds(187, 249, 46, 14);
		panel_1.add(label_12);

		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(187, 264, 86, 20);
		panel_1.add(textField_9);

		label_13 = new JLabel("Provincia:");
		label_13.setBounds(283, 249, 75, 14);
		panel_1.add(label_13);

		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(283, 264, 75, 20);
		panel_1.add(textField_10);

		lblNCivico = new JLabel("N. Civico:");
		lblNCivico.setBounds(284, 203, 75, 14);
		panel_1.add(lblNCivico);

		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(284, 218, 75, 20);
		panel_1.add(textField_11);

		panel = new JPanel();
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dati d'accesso",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 11, 371, 115);
		contentPane.add(panel);

		label = new JLabel("*Nome Utente:");
		label.setAlignmentY(Component.TOP_ALIGNMENT);
		label.setBounds(10, 24, 349, 14);
		panel.add(label);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(11, 38, 348, 20);
		panel.add(textField);

		label_1 = new JLabel("*Passowrd:");
		label_1.setBounds(10, 69, 166, 14);
		panel.add(label_1);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(10, 84, 166, 20);
		panel.add(textField_1);

		label_2 = new JLabel("*Ripeti Passowrd:");
		label_2.setBounds(183, 69, 175, 15);
		panel.add(label_2);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(183, 84, 175, 20);
		panel.add(textField_2);

		panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dati Medici",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(10, 436, 371, 159);
		contentPane.add(panel_2);

		label_15 = new JLabel("Specializzazione:");
		label_15.setBounds(10, 21, 263, 14);
		panel_2.add(label_15);

		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(10, 35, 252, 20);
		panel_2.add(comboBox_1);

		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(272, 127, 86, 20);
		panel_2.add(textField_12);

		label_16 = new JLabel("Num Ordine:");
		label_16.setBounds(272, 112, 86, 14);
		panel_2.add(label_16);

		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(272, 81, 86, 20);
		panel_2.add(textField_13);

		label_17 = new JLabel("Codice Regione:");
		label_17.setBounds(272, 66, 86, 14);
		panel_2.add(label_17);

		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(10, 81, 252, 20);
		panel_2.add(textField_14);

		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(10, 127, 252, 20);
		panel_2.add(textField_15);

		label_18 = new JLabel("Distretto:");
		label_18.setBounds(10, 112, 252, 14);
		panel_2.add(label_18);

		label_19 = new JLabel("ASL:");
		label_19.setBounds(10, 66, 252, 14);
		panel_2.add(label_19);

		button = new JButton("Aggiungi");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(272, 34, 86, 23);
		panel_2.add(button);

		btnNewButton = new JButton("Conferma");
		btnNewButton.setBackground(new Color(60, 179, 113));
		btnNewButton.setBounds(149, 615, 111, 31);
		contentPane.add(btnNewButton);

		btnAnnulla = new JButton("Annulla");
		btnAnnulla.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		btnAnnulla.setBounds(270, 614, 111, 32);
		contentPane.add(btnAnnulla);
	}
}

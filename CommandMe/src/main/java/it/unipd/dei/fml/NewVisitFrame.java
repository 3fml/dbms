package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerDateModel;
import javax.swing.SpringLayout;
import javax.swing.border.EtchedBorder;

public class NewVisitFrame extends JPanel {
	private NewVisitPanel myPanel;
	private Visit visit = null;
	private JTextArea textArea;
	private JSpinner oraSpinner;
	private JSpinner dataSpinner;
	private JLabel lblNessunaPatologiaInserita;
	private JLabel lblNessunaMisurazioneInserita;
	private JLabel lblNessunaPrescrizioneInserita;
	private JLabel lblNessunEsameInserito;
	private Connection connection;

	public NewVisitFrame(NewVisitPanel panel) {
		myPanel = panel;
		visit = Visit.getIstance();
		SpringLayout springLayout = new SpringLayout();
		setLayout(springLayout);
		connection = MainWindowHDC.getIstance().getConnection();

		JLabel lblOraEData = new JLabel("Ora e Data:");
		springLayout.putConstraint(SpringLayout.NORTH, lblOraEData, 3, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblOraEData, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblOraEData, 80, SpringLayout.WEST, this);
		add(lblOraEData);
		oraSpinner = new JSpinner();
		springLayout.putConstraint(SpringLayout.NORTH, oraSpinner, 0, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, oraSpinner, 86, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, oraSpinner, 145, SpringLayout.WEST, this);
		oraSpinner.setModel(new SpinnerDateModel());
		oraSpinner.setEditor(new JSpinner.DateEditor(oraSpinner, "HH:mm"));
		add(oraSpinner);

		dataSpinner = new JSpinner();
		springLayout.putConstraint(SpringLayout.NORTH, dataSpinner, 0, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, dataSpinner, 155, SpringLayout.WEST, this);
		dataSpinner.setModel(new SpinnerDateModel());
		dataSpinner.setEditor(new JSpinner.DateEditor(dataSpinner, "dd/MM/yyyy"));
		add(dataSpinner);

		JLabel lblNote = new JLabel("Note:");
		springLayout.putConstraint(SpringLayout.NORTH, lblNote, 31, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNote, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblNote, 80, SpringLayout.WEST, this);
		add(lblNote);

		textArea = new JTextArea();
		springLayout.putConstraint(SpringLayout.NORTH, textArea, 31, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, textArea, 86, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.SOUTH, textArea, 258, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, textArea, -7, SpringLayout.EAST, this);
		textArea.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(textArea);

		JLabel lblPatologie = new JLabel("Patologie:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPatologie, 273, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblPatologie, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblPatologie, 80, SpringLayout.WEST, this);
		add(lblPatologie);

		lblNessunaPatologiaInserita = new JLabel("Nessuna patologia inserita");
		springLayout.putConstraint(SpringLayout.NORTH, lblNessunaPatologiaInserita, 271, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNessunaPatologiaInserita, 86, SpringLayout.WEST, this);
		lblNessunaPatologiaInserita.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(lblNessunaPatologiaInserita);

		JButton btnNewButton = new JButton("Modifica");
		springLayout.putConstraint(SpringLayout.EAST, lblNessunaPatologiaInserita, -7, SpringLayout.WEST, btnNewButton);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 269, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -7, SpringLayout.EAST, this);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myPanel.newPathology();
			}
		});
		add(btnNewButton);

		JLabel lblMisurazioni = new JLabel("Misurazioni:");
		springLayout.putConstraint(SpringLayout.NORTH, lblMisurazioni, 307, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblMisurazioni, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblMisurazioni, 80, SpringLayout.WEST, this);
		add(lblMisurazioni);

		lblNessunaMisurazioneInserita = new JLabel("Nessuna misurazione inserita");
		springLayout.putConstraint(SpringLayout.NORTH, lblNessunaMisurazioneInserita, 305, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNessunaMisurazioneInserita, 86, SpringLayout.WEST, this);
		lblNessunaMisurazioneInserita.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(lblNessunaMisurazioneInserita);

		JButton button_1 = new JButton("Modifica");
		springLayout.putConstraint(SpringLayout.EAST, lblNessunaMisurazioneInserita, -7, SpringLayout.WEST, button_1);
		springLayout.putConstraint(SpringLayout.NORTH, button_1, 303, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, button_1, -7, SpringLayout.EAST, this);
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myPanel.newMeasure();
			}
		});
		add(button_1);

		JLabel lblPrescrizioni = new JLabel("Prescrizioni:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPrescrizioni, 341, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblPrescrizioni, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblPrescrizioni, 80, SpringLayout.WEST, this);
		add(lblPrescrizioni);

		lblNessunaPrescrizioneInserita = new JLabel("Nessuna prescrizione inserita");
		springLayout.putConstraint(SpringLayout.NORTH, lblNessunaPrescrizioneInserita, 339, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNessunaPrescrizioneInserita, 86, SpringLayout.WEST, this);
		lblNessunaPrescrizioneInserita.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(lblNessunaPrescrizioneInserita);

		JButton button = new JButton("Modifica");
		springLayout.putConstraint(SpringLayout.EAST, lblNessunaPrescrizioneInserita, -7, SpringLayout.WEST, button);
		springLayout.putConstraint(SpringLayout.NORTH, button, 337, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, button, -7, SpringLayout.EAST, this);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myPanel.newPrescription();
			}
		});
		add(button);

		JLabel lblEsami = new JLabel("Esami:");
		springLayout.putConstraint(SpringLayout.NORTH, lblEsami, 375, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblEsami, 6, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, lblEsami, 80, SpringLayout.WEST, this);
		add(lblEsami);

		lblNessunEsameInserito = new JLabel("Nessun esame inserito");
		springLayout.putConstraint(SpringLayout.NORTH, lblNessunEsameInserito, 373, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNessunEsameInserito, 86, SpringLayout.WEST, this);
		lblNessunEsameInserito.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(lblNessunEsameInserito);

		JButton button_2 = new JButton("Modifica");
		springLayout.putConstraint(SpringLayout.EAST, lblNessunEsameInserito, -7, SpringLayout.WEST, button_2);
		springLayout.putConstraint(SpringLayout.NORTH, button_2, 371, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, button_2, -7, SpringLayout.EAST, this);
		button_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myPanel.newExam();
			}
		});
		add(button_2);

	}

	public Visit getVisit() {
		return visit;
	}

	public void saveVisit() {
		if (textArea.getText().length() > 0) {
			visit = Visit.getIstance();
			visit.notes = textArea.getText();
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mmdd/MM/yyyy");
			SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm");
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");

			String time = formatter1.format((Date) oraSpinner.getValue());
			System.out.println(formatter1.format((Date) oraSpinner.getValue()));
			String date = formatter2.format((Date) dataSpinner.getValue());
			try {
				visit.timestamp = java.sql.Timestamp.from((formatter.parse(time + date).toInstant()));
			} catch (ParseException e) {
				visit.timestamp = null;
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
		}

		updateDB();
		Visit.dismissIstance();

	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		updateUI();
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public void updateDB() {

		try {
			// create visit
			PatientRow pr = myPanel.getPatient();
			Patient p = new Patient(connection);
			int visitId = p.createVisit(pr.get("fiscalcode"), visit.notes, visit.timestamp);

			// create prescriptions
			if (visit.prescription.present)
				p.createPrescription(visit.prescription.color, visit.prescription.notes, visit.prescription.priority,
						visitId);

			// create prescriptions
			if (visit.pathology.size() > 0)
				for (int i = 0; i < visit.pathology.size(); i++)
					p.createPathology(visitId, visit.pathology.get(i).icd9Code);

		} catch (SQLException | SQLInjectionException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateUI() {
		if (visit != null) {

			lblNessunaPatologiaInserita.setText("Nessuna patologia inserita");
			lblNessunaPrescrizioneInserita.setText("Nessuna prescrizione inserita");
			lblNessunaMisurazioneInserita.setText("Nessuna misurazione inserita");
			lblNessunEsameInserito.setText("Nessun esame inserito");

			if (visit.pathology.size() > 0) {
				lblNessunaPatologiaInserita.setText("");
				for (int i = 0; i < visit.pathology.size(); i++)
					lblNessunaPatologiaInserita
							.setText(lblNessunaPatologiaInserita.getText() + visit.pathology.get(i).description + "; ");
			}

			if (visit.prescription.present) {
				lblNessunaPrescrizioneInserita.setText("");
				lblNessunaPrescrizioneInserita
						.setText(lblNessunaPrescrizioneInserita.getText() + visit.prescription.notes + "; ");
			}

			if (visit.measure.size() > 0) {
				lblNessunaMisurazioneInserita.setText("");
				for (int i = 0; i < visit.measure.size(); i++)
					lblNessunaMisurazioneInserita
							.setText(lblNessunaMisurazioneInserita.getText() + visit.measure.get(i).descriptor + "; ");
			}

			if (visit.exam.present) {
				lblNessunEsameInserito.setText("");
				lblNessunEsameInserito.setText(lblNessunEsameInserito.getText() + visit.exam.notes + "; ");
			}
		}
	}
}

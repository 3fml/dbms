package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class NewMeasureFrame extends JPanel {
	private JFormattedTextField textField;
	private JTextArea textArea;
	private JComboBox comboBox;

	public NewMeasureFrame() {
		setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(40dlu;default)"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("right:default:grow"),
						FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, }));

		JLabel lblAggiungiMisura = new JLabel("Misura:");
		add(lblAggiungiMisura, "2, 2, left, default");

		comboBox = new JComboBox();
		add(comboBox, "4, 2, fill, default");

		new Thread() {
			@Override
			public void run() {
				try {
					Entity e = new Entity(MainWindowHDC.getIstance().getConnection(), "measure");

					String[] result = e.getMeasureValues();

					comboBox.setModel(new DefaultComboBoxModel(result));
					comboBox.setSelectedIndex(-1);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				} catch (SQLInjectionException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				}
			}
		}.start();

		JLabel lblValore = new JLabel("Valore:");
		add(lblValore, "2, 4, left, default");

		NumberFormat integerFieldFormatter = NumberFormat.getNumberInstance();
		integerFieldFormatter.setMaximumFractionDigits(4);
		integerFieldFormatter.setGroupingUsed(false);

		textField = new JFormattedTextField(integerFieldFormatter);
		add(textField, "4, 4, fill, default");
		textField.setColumns(10);

		JButton btnAggiungi = new JButton("Aggiungi");
		btnAggiungi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() > -1)
					textArea.append((String) comboBox.getSelectedItem() + ": " + textField.getText() + "\n");
			}
		});
		add(btnAggiungi, "4, 6");

		JLabel lblMisureAggiunte = new JLabel("Misure Aggiunte:");
		add(lblMisureAggiunte, "2, 8, left, top");

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(textArea, "4, 8, fill, fill");

		Visit visit = Visit.getIstance();
		if (visit.measure.size() > 0)
			for (int i = 0; i < visit.measure.size(); i++)
				textArea.setText(textArea.getText() + visit.measure.get(i).descriptor + ": "
						+ visit.measure.get(i).value + "\n");
	}

	public void removeEverything() {
		textArea.setText("");
		Visit visit = Visit.getIstance();
		visit.measure = new ArrayList<>(0);
	}

	public void removeLast() {
		String text = textArea.getText();
		if (text.lastIndexOf("\n") > 0)
			text = text.substring(0, text.lastIndexOf("\n")); // tolgo il primo "a capo"
		if (text.lastIndexOf("\n") > 0)
			text = text.substring(0, text.lastIndexOf("\n") + 1); // tolgo l'ultima riga
		else
			text = ""; // tolgo l'ultima riga
		textArea.setText(text);
		Visit visit = Visit.getIstance();
		if (visit.pathology.size() > 0)
			visit.measure.remove(visit.measure.size() - 1);
	}

	public void salva() {
		if (textArea.getText().length() > 2) {
			Visit visit = Visit.getIstance();
			String[] lines = textArea.getText().split("\\r?\\n");
			for (String line : lines) {
				Visit.Measure mes = visit.new Measure();
				mes.descriptor = line.split(": ")[0];
				mes.value = Float.parseFloat(line.split(": ")[1]);
				visit.measure.add(mes);
			}
		}
	}

}

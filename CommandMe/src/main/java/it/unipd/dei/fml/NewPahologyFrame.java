package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class NewPahologyFrame extends JPanel {

	private Connection connection;
	private JTextArea textArea;

	public NewPahologyFrame() {

		connection = MainWindowHDC.getIstance().getConnection();

		setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(39dlu;default):grow"),
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.UNRELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.UNRELATED_GAP_ROWSPEC,}));
		
				JLabel lblSelezionaNuovaPatologia = new JLabel("Seleziona nuova patologia:");
				add(lblSelezionaNuovaPatologia, "2, 2, right, default");
		
				final JComboBox comboBox = new JComboBox();
				
						comboBox.setEditable(true);
						add(comboBox, "4, 2, fill, default");
		
				JButton btnAggiungi = new JButton("Aggiungi");
				btnAggiungi.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (comboBox.getSelectedIndex() > 0)
							textArea.append((String) comboBox.getSelectedItem() + "\n");
					}
				});
				add(btnAggiungi, "6, 2");

		JLabel lblPatologieSelezionate = new JLabel("Patologie selezionate:");
		add(lblPatologieSelezionate, "2, 4, left, top");

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(textArea, "4, 4, 3, 1, fill, fill");

		Visit visit = Visit.getIstance();
		if (visit.pathology.size() > 0)
			for (int i = 0; i < visit.pathology.size(); i++)
				textArea.setText(textArea.getText() + visit.pathology.get(i).icd9Code + " - "
						+ visit.pathology.get(i).description + "\n");

		new Thread() {
			@Override
			public void run() {
				try {
					Entity e = new Entity(connection, "pathology");
					ArrayList<Row> rows = e.getTable();
					String[] result = new String[rows.size() + 1];
					result[0] = "--Seleziona--";
					for (int i = 0; i < rows.size(); i++)
						result[i + 1] = rows.get(i).get("icd9code") + " - " + rows.get(i).get("description");
					comboBox.setModel(new DefaultComboBoxModel(result));
					comboBox.setSelectedIndex(0);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				} catch (SQLInjectionException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				}
			}
		}.start();

		JLabel lblCecaConIcd = new JLabel("Ceca con ICD9:");
		add(lblCecaConIcd, "2, 6");

		JLabel label = new JLabel("<Nessuna>");
		add(label, "4, 6");

		JButton button = new JButton("...");
		button.setEnabled(false);
		add(button, "6, 6");
	}

	public void removeEverything() {
		textArea.setText("");
		Visit visit = Visit.getIstance();
		visit.pathology = new ArrayList<>(0);
	}

	public void removeLast() {
		String text = textArea.getText();
		if (text.lastIndexOf("\n") > 0)
			text = text.substring(0, text.lastIndexOf("\n")); // tolgo il primo "a capo"
		if (text.lastIndexOf("\n") > 0)
			text = text.substring(0, text.lastIndexOf("\n") + 1); // tolgo l'ultima riga
		else
			text = ""; // tolgo l'ultima riga
		textArea.setText(text);
		Visit visit = Visit.getIstance();
		if (visit.pathology.size() > 0)
			visit.pathology.remove(visit.pathology.size() - 1);
	}

	public void salva() {
		if (textArea.getText().length() > 2) {
			Visit visit = Visit.getIstance();
			String[] lines = textArea.getText().split("\\r?\\n");
			for (String line : lines) {
				Visit.Pathology pat = visit.new Pathology();
				pat.icd9Code = line.split(" - ")[0];
				pat.description = line.split(" - ")[1];
				visit.pathology.add(pat);
			}
		}
	}
}

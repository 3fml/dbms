package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MainPanel extends VoidPanel {

	private PatientSearchPanel patientSearchPanel;
	private NewPatientPanel newPatientPanel;
	private SettingPanel SettingPanel;
	private SettingPanel settingPanel;

	private MainFrame myFrame;

	/**
	 * Pannello con le azioni mostrate all'avvio della finestra principale.
	 * 
	 */
	public MainPanel(final MainWindowHDC parent) {
		super(parent);
		myFrame = new MainFrame(parent);
		super.setFrame(myFrame);

		JButton btnCercaPaziente = new JButton("Cerca Paziente");
		btnCercaPaziente.addActionListener(new ActionListener() {
			/**
			 * Ogni volta che si modifica l'interfaccia mi sembra di aver capito che bisogna
			 * farlo in un Thread apposito.
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				// Ho spostato la creazione dei figli qui perchè altrimenti mi crea tutto a
				// cascata all'avvio
				parent.setView(patientSearchPanel = new PatientSearchPanel(parent));
			}
		});

		JLabel lblNewLabel = new JLabel("Menu Azioni:");
		lblNewLabel.setVerticalTextPosition(SwingConstants.TOP);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		this.add(lblNewLabel, "2, 1");
		this.add(btnCercaPaziente, "2, 2, fill, fill");

		JButton btnImpostazioni = new JButton("Impostazioni");
		btnImpostazioni.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(SettingPanel = new SettingPanel(parent));
			}
		});

		JButton btnRicercaAvanzata = new JButton("Ricerca Avanzata");
		btnRicercaAvanzata.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(settingPanel = new SettingPanel(parent));
			}
		});

		JButton btnNuovoPaziente = new JButton("Nuovo Paziente");
		btnNuovoPaziente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(newPatientPanel = new NewPatientPanel(parent));
			}
		});
		add(btnNuovoPaziente, "2, 4");
		this.add(btnRicercaAvanzata, "2, 6, fill, fill");
		this.add(btnImpostazioni, "2, 18, fill, fill");

		JButton btnEsci = new JButton("Esci");
		btnEsci.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnEsci.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.dispose();
			}
		});
		btnEsci.setBackground(new Color(178, 34, 34));
		this.add(btnEsci, "2, 20, fill, fill");

	}

	/**
	 * Metodo che a cascata chiama tutti i discendenti e resetta tutta l'interfaccia
	 * allo stato di default (perdendo le operazioni fatte fino ad ora)
	 */
	@Override
	public void clean() {
		// System.out.println(this.toString().substring(15, 40) + "\n");
		if (patientSearchPanel != null) {
			patientSearchPanel.clean();
			patientSearchPanel = null;
		}
	}

}

package it.unipd.dei.fml;

public class AllVisitOfPatientPanel extends PanelWithPatient {

	private AllVisitOfPatientFrame myFrame;
	private MainWindowHDC parent;
	private BackAndHomePanel backOnlyPanel = null;

	/**
	 * Create the panel.
	 */
	public AllVisitOfPatientPanel() {
		this(null, new PatientRow());
	}

	public AllVisitOfPatientPanel(final MainWindowHDC parent, final PatientRow patient) {
		super(parent, patient);
		myFrame = new AllVisitOfPatientFrame(parent, this);
		super.setFrame(myFrame);

	}

}

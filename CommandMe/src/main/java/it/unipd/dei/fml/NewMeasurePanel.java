package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class NewMeasurePanel extends PanelWithPatient {

	private NewMeasureFrame myFrame;
	private Visit visit;

	public NewMeasurePanel() {
		this(null, null);
	}

	public NewMeasurePanel(final MainWindowHDC parent, PatientRow patient) {
		super(parent, patient);
		myFrame = new NewMeasureFrame();
		super.setFrame(myFrame);
		visit = visit;

		JButton button = new JButton("Salva");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.salva();
				clean();
				parent.goBack();
			}
		});

		JButton btnCancellaUltimo = new JButton("Cancella ultimo");
		btnCancellaUltimo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.removeLast();
			}
		});
		
		JLabel lblInserimentoMisurazione = new JLabel("Inserimento Misurazione:");
		add(lblInserimentoMisurazione, "2, 1");
		add(btnCancellaUltimo, "2, 2");

		JButton btnCancellaTutto = new JButton("Cancella tutto");
		btnCancellaTutto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.removeEverything();
			}
		});
		add(btnCancellaTutto, "2, 4");
		button.setBackground(new Color(60, 179, 113));
		add(button, "2, 6");
	}

}

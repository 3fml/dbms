package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class BackAndHomePanel extends VoidPanel {

	/**
	 * Pannello di controllo con solo i tasti Indietro e Home. Questo pannello non
	 * ha alcun "frame" associato.
	 * 
	 */
	public BackAndHomePanel(final MainWindowHDC parent) {
		super(parent);

		JButton backButton = new JButton("Indietro");
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clean();
				parent.goBack();
			}
		});
		backButton.setIconTextGap(6);
		backButton.setIcon(new ImageIcon("resources\\back16.png"));
		backButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		backButton.setBackground(new Color(220, 20, 60));
		add(backButton, "2, 18");

		JButton homeButton = new JButton("Home");
		homeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.goHome();
			}
		});
		homeButton.setIconTextGap(6);
		homeButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		homeButton.setBackground(new Color(220, 20, 60));
		add(homeButton, "2, 20");

	}

	@Override
	public void clean() {
		// System.out.println(this.toString().substring(0, 30)+"\n"); //NOP
	}

}

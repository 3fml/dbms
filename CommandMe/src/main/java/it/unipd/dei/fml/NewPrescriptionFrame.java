package it.unipd.dei.fml;

import java.awt.Color;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class NewPrescriptionFrame extends JPanel {
	private JTextArea textArea;
	private JComboBox comboBox_1;
	private JComboBox comboBox;

	/**
	 * Create the panel.
	 */
	public NewPrescriptionFrame() {
		setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow(10)"), FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.UNRELATED_GAP_ROWSPEC, }));

		JLabel lblPrescrizioneNumero = new JLabel("Prescrizione Numero:");
		add(lblPrescrizioneNumero, "2, 2");

		JLabel label = new JLabel("0");
		add(label, "4, 2");

		JLabel lblColore = new JLabel("Tipo prescrizione:");
		add(lblColore, "2, 4, left, default");

		comboBox = new JComboBox();
		comboBox.setModel(
				new DefaultComboBoxModel(new String[] { "Ricetta Bianca", "Ricetta Rossa", "Ricetta Elettronica" }));
		add(comboBox, "4, 4, fill, default");

		JLabel lblPriorit = new JLabel("Priorità:");
		add(lblPriorit, "2, 6, left, default");

		comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "Bassa", "Media", "Alta", "Urgente" }));
		add(comboBox_1, "4, 6, fill, default");

		JLabel lblNote = new JLabel("Note:");
		add(lblNote, "2, 8, left, top");

		textArea = new JTextArea();
		textArea.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(textArea, "4, 8, 5, 1, fill, fill");

		JLabel lblInserisciCvp = new JLabel("Inserisci CVP:");
		add(lblInserisciCvp, "2, 10");

		JLabel label_1 = new JLabel("<Nessuno>");
		label_1.setBackground(new Color(255, 255, 255));
		add(label_1, "4, 10, 3, 1");

		JButton btnAggiungi = new JButton("Aggiungi");
		btnAggiungi.setEnabled(false);
		add(btnAggiungi, "8, 10");

		JLabel lblSelezionaMedicinale = new JLabel("Seleziona Medicinale:");
		add(lblSelezionaMedicinale, "2, 12");

		JLabel label_2 = new JLabel("<Nessuno>");
		label_2.setBackground(new Color(255, 255, 255));
		add(label_2, "4, 12, 3, 1");

		JButton button = new JButton("Aggiungi");
		button.setEnabled(false);
		add(button, "8, 12");

	}

	public void salva() {
		if (textArea.getText().length() > 0) {
			Visit visit = Visit.getIstance();
			visit.prescription.notes = textArea.getText();
			visit.prescription.priority = (String) comboBox_1.getSelectedItem();
			visit.prescription.color = ((String) comboBox.getSelectedItem()).substring(8);
			visit.prescription.present = true;
		}
	}

}

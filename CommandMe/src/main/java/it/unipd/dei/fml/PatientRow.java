package it.unipd.dei.fml;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 */
public class PatientRow extends Row {

	private String fiscalcode;
	private String name;
	private String surname;
	private String gender;
	private String telephone;
	private String cellphone;
	private String email;
	private String userstatus;
	private String nationality;
	private Date dob;
	private String ulss;

	public PatientRow() {
		super(11);
		ArrayList<String> cname = new ArrayList<>(11);
		cname.add("fiscalcode");
		cname.add("name");
		cname.add("surname");
		cname.add("gender");
		cname.add("telephone");
		cname.add("cellphone");
		cname.add("email");
		cname.add("userstatus");
		cname.add("nationality");
		cname.add("dob");
		cname.add("ulss");
		try {
			super.setColumnNames(cname);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructor that retrieve the metadata and the values from the current row
	 * pointe by a ResultSet initialize and setted correctly (ResultSet.next() has
	 * to be executed at least once before running this constructor)
	 *
	 * @param rs
	 *            ResultSet object setted correctly
	 * @throws SQLException
	 */
	public PatientRow(ResultSet rs) throws SQLException {
		super(rs);
		fiscalcode = get("fiscalcode");
		name = get("name");
		surname = get("surname");
		gender = get("gender");
		telephone = get("telephone");
		cellphone = get("cellphone");
		email = get("email");
		userstatus = get("userstatus");
		nationality = get("nationality");
		if (get("dob") != null)
			dob = Date.valueOf(get("dob"));
		else
			dob = null;
		ulss = get("ulss");
	}

	public PatientRow(Row r) {
		super(r);
		fiscalcode = r.get("fiscalcode");
		name = r.get("name");
		surname = r.get("surname");
		gender = r.get("gender");
		telephone = r.get("telephone");
		cellphone = r.get("cellphone");
		email = r.get("email");
		userstatus = r.get("userstatus");
		nationality = r.get("nationality");
		if (r.get("dob") != null)
			dob = Date.valueOf(r.get("dob"));
		if (get("dob") != null)
			ulss = r.get("ulss");
	}

	@Override
	public PatientRow clone() {
		return new PatientRow(this);
	}

	public String getCellphone() {
		return cellphone;
	}

	public Date getDob() {
		return dob;
	}

	public String getEmail() {
		return email;
	}

	public String getFiscalcode() {
		return fiscalcode;
	}

	public String getGender() {
		return gender;
	}

	public String getName() {
		return name;
	}

	public String getNationality() {
		return nationality;
	}

	public String getSurname() {
		return surname;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getUlss() {
		return ulss;
	}

	public String getUserstatus() {
		return userstatus;
	}

	/**
	 * Set the valued at the position corresponding with the column name equals to
	 * columnLabel to the value "value"
	 *
	 * @param columnLabel
	 *            Column name of the value
	 * @param value
	 *            value to be setted
	 * @throws IllegalArgumentException
	 *             if a column that does not belong to Patient is inserted
	 */
	@Override
	public void set(String columnLabel, String value) throws IllegalArgumentException {
		switch (columnLabel) {
		case "fiscalcode":
			fiscalcode = value;
			break;
		case "name":
			name = value;
			break;
		case "surname":
			surname = value;
			break;
		case "gender":
			gender = value;
			break;
		case "telephone":
			telephone = value;
			break;
		case "cellphone":
			cellphone = value;
			break;
		case "email":
			email = value;
			break;
		case "userstatus":
			userstatus = value;
			break;
		case "nationality":
			nationality = value;
			break;
		case "ulss":
			ulss = value;
			break;
		case "dob":
			if (value == null)
				dob = null;
			else
				dob = Date.valueOf(value);
			break;
		default:
			throw new IllegalArgumentException("Column does not belong to Patient");
		}
		super.set(columnLabel, value);
	}

	public void setCellphone(String cellphone) {
		set("cellphone", cellphone);
		this.cellphone = cellphone;
	}

	public void setDob(Date dob) {
		set("dob", dob.toString());
		this.dob = dob;
	}

	public void setEmail(String email) {
		set("email", email);
		this.email = email;
	}

	public void setFiscalcode(String fiscalcode) {
		set("fiscalcode", fiscalcode);
		this.fiscalcode = fiscalcode;
	}

	public void setGender(String gender) {
		set("gender", gender);
		this.gender = gender.toLowerCase();
	}

	public void setName(String name) {
		set("name", name);
		this.name = name;
	}

	public void setNationality(String nationality) {
		set("nationality", nationality);
		this.nationality = nationality;
	}

	public void setSurname(String surname) {
		set("surname", surname);
		this.surname = surname;
	}

	public void setTelephone(String telephone) {
		set("telephone", telephone);
		this.telephone = telephone;
	}

	public void setUlss(String ulss) {
		set("ulss", ulss);
		this.ulss = ulss;
	}

	public void setUserstatus(String userstatus) {
		set("userstatus", userstatus);
		this.userstatus = userstatus;
	}

}

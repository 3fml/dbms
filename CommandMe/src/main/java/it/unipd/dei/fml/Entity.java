package it.unipd.dei.fml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

/**
 * Generic class for handling all the operation with the fml database. In order
 * to be as generic as possible, some operation are not very confortable, for
 * most used tables and operations they should be implemented specific classes
 * that extend this one and specilize it.
 */
class Entity {
	final static String SCHEMA_NAME = "fml";
	protected String entityName;
	protected Connection connection;

	protected boolean filterDate = false;
	protected Date filterStartDate;
	protected Date filterEndDate;
	private Row rowMetadata;

	/**
	 * Constructor, it needs:
	 *
	 * @param con
	 *            connection in order to accessing the database
	 * @param entity
	 *            table name that this class represent
	 * @throws SQLException
	 * @throws SQLInjectionException
	 */
	public Entity(Connection con, String entity) throws SQLException, SQLInjectionException {
		connection = con;
		if (!check(entity))
			throw new SQLInjectionException("Possible risk of SQL injection");
		entityName = entity;
		genRowMetadata(); // generate the row metadata container
	}

	/**
	 * Check if the table name is in the schema
	 *
	 * @return true if it find the table, false otherwise
	 * @throws SQLException
	 */
	private boolean check(String tablename) throws SQLException {
		ResultSet rs = null;
		boolean result = false;
		try {
			DatabaseMetaData md = connection.getMetaData();
			rs = md.getTables(null, SCHEMA_NAME, tablename, new String[] { "TABLE" });
			result = rs.next();
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return result;
	}

	/**
	 * Control if a specific Row could belong to the table. It does not control the
	 * values but only the types and names of the columns
	 *
	 * @param row
	 *            the row that is going to be checked
	 * @return true if the Row type could be coherent with the Table columns
	 */
	public boolean checkRowBelong2Table(Row row) {
		boolean result = true;
		for (int i = 0; i < rowMetadata.size(); i++)
			result = (result && (rowMetadata.getTypes().get(i).equalsIgnoreCase(row.getTypes().get(i)))
					&& (rowMetadata.getColumnNames().get(i).equalsIgnoreCase(row.getColumnNames().get(i))));
		return result;
	}

	/**
	 * Very powerful (and risky) method that permits to create a new Row of whatever
	 * table in the schema. This do permit to set some entries as DEFAULT, passing a
	 * Row with default ArrayList setted.
	 *
	 * @param info
	 *            the row that is going to be inserted
	 * @throws SQLException
	 */
	public void createGenericInstance(Row info) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + "." + entityName + " VALUES " + questionMarks(info) + ";";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			for (int i = 0; i < info.size(); i++)
				pstmn.setString(i + 1, info.get(i));
			pstmn.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Check if a row exist in the represented table
	 *
	 * @return true if exists, false if not
	 * @throws SQLException
	 */
	public boolean doExists(Row r) throws SQLException {
		boolean result = false;
		String sql = "SELECT COUNT(*) FROM  " + SCHEMA_NAME + "." + entityName + " WHERE ";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			for (int i = 0; i < r.size() - 1; i++)
				if (r.get(i) == null)
					sql += r.getColumnNames().get(i) + " IS NULL  AND ";
				else
					sql += r.getColumnNames().get(i) + " = ?  AND ";
			if (r.get(r.size() - 1) == null)
				sql += r.getColumnNames().get(r.size() - 1) + " IS NULL  AND ";
			else
				sql += r.getColumnNames().get(r.size() - 1) + " = ?  AND ";

			st = connection.prepareStatement(sql);
			int k = 1;
			for (int i = 0; i < r.size(); i++)
				if (r.get(i) != null)
					st.setString(k++, r.get(i));
			rs = st.executeQuery();

			if (rs.next())
				result = (rs.getInt(1) == 1);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				st = null;
			}
		}

		return result;
	}

	private void genRowMetadata() throws SQLException {
		if (rowMetadata == null)
			rowMetadata = new Row();
		ResultSet rs = null;
		try {
			DatabaseMetaData metadata = connection.getMetaData();
			rs = metadata.getColumns(null, SCHEMA_NAME, entityName, null);
			ArrayList<String> names = new ArrayList<>(getColumnCount());
			ArrayList<String> types = new ArrayList<>(getColumnCount());
			while (rs.next()) {
				types.add(rs.getString("TYPE_NAME"));
				names.add(rs.getString("COLUMN_NAME"));
			}
			rowMetadata.setColumnNames(names);
			rowMetadata.setTypes(types);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
	}

	/**
	 * Return the number of the column of the tabel represented by the object
	 *
	 * @return ineger number of the columns
	 * @throws SQLException
	 */
	public int getColumnCount() throws SQLException {
		final String sql = "SELECT COUNT(COLUMN_NAME) " + "FROM INFORMATION_SCHEMA.COLUMNS " + "WHERE "
				+ "TABLE_CATALOG = current_catalog " + "AND TABLE_SCHEMA = ? " + "AND TABLE_NAME = ? ;";
		ResultSet rs = null;
		PreparedStatement pst = null;
		int count = -1;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, SCHEMA_NAME);
			pst.setString(2, entityName);
			rs = pst.executeQuery();
			if (rs.next())
				count = rs.getInt(1);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}
		return count;
	}

	/**
	 * Return the list of column names of the table represeted
	 *
	 * @return list of column names of the table represeted
	 * @throws SQLException
	 */
	public ArrayList<String> getColumsName() throws SQLException {
		ResultSet rs = null;
		ArrayList<String> names = new ArrayList<>();
		try {
			DatabaseMetaData metadata = connection.getMetaData();
			rs = metadata.getColumns(null, SCHEMA_NAME, entityName, null);
			while (rs.next())
				names.add(rs.getString("COLUMN_NAME"));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return names;
	}

	/**
	 * Return the list of column types of the table represeted
	 *
	 * @return list of column types of the table represeted
	 * @throws SQLException
	 */
	public ArrayList<String> getColumsTypes() throws SQLException {
		ResultSet rs = null;
		ArrayList<String> types = new ArrayList<>();
		try {
			DatabaseMetaData metadata = connection.getMetaData();
			rs = metadata.getColumns(null, SCHEMA_NAME, entityName, null);
			while (rs.next())
				types.add(rs.getString("TYPE_NAME"));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return types;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the DoctorRow
	 * that is the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the DoctorRow
	 * @throws SQLException
	 */
	protected DoctorRow getDoctorRowFromQuery(ResultSet rs) throws SQLException {
		DoctorRow result = null;
		if (rs.next())
			result = new DoctorRow(rs);
		return result;
	}

	/**
	 * Return the list of Tables that "point" to the PrimaryKey of this table. This
	 * method is useful in order to explore the database schema
	 *
	 * @return the list of Tables that point to this Table
	 * @throws SQLException
	 */
	public ArrayList<Entity> getEntitiesThatPoitToMe() throws SQLException, SQLInjectionException {
		ResultSet rs = null;
		ArrayList<Entity> results = new ArrayList<>(1);
		try {
			DatabaseMetaData meta = connection.getMetaData();
			rs = meta.getExportedKeys(null, SCHEMA_NAME, entityName);
			results = new ArrayList<>(1);
			while (rs.next())
				results.add(new Entity(connection, rs.getString("FKTABLE_NAME")));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}

		return results;
	}

	/**
	 * Retrieve the names of the columns that contain the Foreingn Keys
	 *
	 * @return the list of column names
	 * @throws SQLException
	 */
	public ArrayList<String> getForeingKeyColumnNames() throws SQLException {
		ResultSet rs = null;
		ArrayList<String> fksNames = new ArrayList<>();
		try {
			DatabaseMetaData meta = connection.getMetaData();
			rs = meta.getImportedKeys(null, SCHEMA_NAME, entityName);
			while (rs.next())
				fksNames.add(rs.getString("FKCOLUMN_NAME"));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return fksNames;
	}

	public String[] getMeasureValues() throws SQLException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String[] result = null;
		try {

			String sql = "SELECT enum_range(NULL:: measurementsdescriptor); ";

			pst = connection.prepareStatement(sql);

			rs = pst.executeQuery();
			Array a = null;
			if (rs.next())
				a = rs.getArray("enum_range");
			result = (String[]) a.getArray();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the list of
	 * DoctorRow that are the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the list of DoctorRow
	 * @throws SQLException
	 */
	protected ArrayList<DoctorRow> getMultipleDoctorRowsFromQuery(ResultSet rs) throws SQLException {
		ArrayList<DoctorRow> result = new ArrayList<>();

		while (rs.next())
			result.add(new DoctorRow(rs));
		return result;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the list of
	 * PatientRow that are the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the list of PatientRow
	 * @throws SQLException
	 */
	protected ArrayList<PatientRow> getMultiplePatientRowsFromQuery(ResultSet rs) throws SQLException {
		ArrayList<PatientRow> result = new ArrayList<>();

		while (rs.next())
			result.add(new PatientRow(rs));
		return result;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the list of
	 * Rows that are the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the list of Rows
	 * @throws SQLException
	 */
	protected ArrayList<Row> getMultipleRowsFromQuery(ResultSet rs) throws SQLException {
		ArrayList<Row> result = new ArrayList<>();

		while (rs.next())
			result.add(new Row(rs));
		return result;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the
	 * PatientRow that is the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the PatientRow
	 * @throws SQLException
	 */
	protected PatientRow getPatientRowFromQuery(ResultSet rs) throws SQLException {
		PatientRow result = null;
		if (rs.next())
			result = new PatientRow(rs);
		return result;
	}

	/**
	 * Return the list of Tables "pointed" by the ForeignKeys of this table. This
	 * method is useful in order to explore the database schema
	 *
	 * @return the list of entityes tha represented the pointed Tables
	 * @throws SQLException
	 * @throws SQLInjectionException
	 */
	public ArrayList<Entity> getPointedEntities() throws SQLException, SQLInjectionException {
		ResultSet rs = null;
		ArrayList<Entity> results = new ArrayList<>(1);
		try {
			DatabaseMetaData meta = connection.getMetaData();
			rs = meta.getImportedKeys(null, SCHEMA_NAME, entityName);
			while (rs.next())
				results.add(new Entity(connection, rs.getString("PKTABLE_NAME")));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return results;
	}

	/**
	 * Retrieve the names of the columns that contain the Primary Key
	 *
	 * @return the list of column names
	 * @throws SQLException
	 */
	public ArrayList<String> getPrimaryKeyColumnNames() throws SQLException {
		ResultSet rs = null;
		ArrayList<String> pksNames = new ArrayList<>();
		try {
			DatabaseMetaData meta = connection.getMetaData();
			rs = meta.getPrimaryKeys(null, SCHEMA_NAME, entityName);
			while (rs.next())
				pksNames.add(rs.getString("COLUMN_NAME"));
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
		return pksNames;
	}

	/**
	 * Return a Row from the represented Table choosen randomly
	 *
	 * @return the random Row of the table "entityName"
	 * @throws SQLException
	 */
	public Row getRandomRow() throws SQLException {
		Statement st = null;
		ResultSet rs = null;
		Row result = null;
		try {
			st = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery("SELECT COUNT(*) FROM " + SCHEMA_NAME + "." + entityName);
			rs.next();
			int count = rs.getInt(1);
			rs.close();
			Random ran = new Random();
			rs = st.executeQuery("SELECT * FROM " + SCHEMA_NAME + "." + entityName);
			rs.absolute(ran.nextInt(count) + 1);
			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				st = null;
			}
		}
		return result;
	}

	/**
	 * Retrieve the row corresponding to the Primary Key given as parameter
	 *
	 * @param inputPKs
	 *            list of the Primary Keys
	 * @return the row if it has been founded, null otherwise
	 * @throws SQLException
	 */
	public Row getRow(ArrayList<String> inputPKs) throws SQLException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		Row result = null;
		try {
			ArrayList<String> pksNames = getPrimaryKeyColumnNames();

			if (pksNames.size() != inputPKs.size())
				return null;

			String sql = "SELECT * FROM " + SCHEMA_NAME + "." + entityName + " WHERE ";
			for (int i = 0; i < pksNames.size() - 1; i++)
				sql += pksNames.get(i) + " = ? AND ";

			sql += pksNames.get(pksNames.size() - 1) + " = ?; ";

			pst = connection.prepareStatement(sql);
			for (int i = 0; i < inputPKs.size(); i++)
				pst.setString(i + 1, inputPKs.get(i));
			rs = pst.executeQuery();

			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Given a ResultSet object with a query already executed, returns the Row that
	 * is the output of the query
	 *
	 * @param rs
	 *            ResultSet with query already executed
	 * @return the Row
	 * @throws SQLException
	 */
	protected Row getRowFromQuery(ResultSet rs) throws SQLException {
		Row result = null;
		if (rs.next())
			result = new Row(rs);
		return result;
	}

	public ArrayList<Row> getTable() throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT * FROM  " + SCHEMA_NAME + "." + entityName + ";";
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}
		return result;
	}

	/**
	 * Legacy method for testing some basics operations
	 *
	 * @throws SQLException
	 * @throws IOException
	 */
	public void newEntityBuilder() throws SQLException, IOException {
		ResultSet rs = null;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			DatabaseMetaData metadata = connection.getMetaData();
			rs = metadata.getColumns(null, SCHEMA_NAME, entityName, null);
			System.out.println(SCHEMA_NAME + " " + entityName + " | Number of columns: " + getColumnCount());
			ArrayList<String> parameters = new ArrayList<>();
			while (rs.next()) {
				System.out.println(
						"Insert " + rs.getString("COLUMN_NAME") + "( " + rs.getString("TYPE_NAME") + ") " + ": ");
				System.out.printf(">_");
				parameters.add(br.readLine());
			}
			System.out.println("Do you want to insert these values?");
			System.out.printf("(");
			for (int j = 0; j < parameters.size() - 1; j++)
				System.out.printf(parameters.get(j) + ", ");
			System.out.println(parameters.get(parameters.size() - 1) + ")");
			System.out.printf(">_ ");
			String yesOrNo = br.readLine();
			if (yesOrNo.equalsIgnoreCase("yes")) {
				String tempValues = "(";
				for (int i = 0; i < parameters.size() - 1; i++)
					tempValues = tempValues + "?,";
				tempValues += "?)";
				String SQL = "INSERT INTO " + SCHEMA_NAME + "." + entityName + " VALUES "
						+ questionMarks(parameters.size()) + ";";

				try {
					PreparedStatement pstmn = connection.prepareStatement(SQL);
					for (int i = 0; i < parameters.size(); i++)
						pstmn.setString(i + 1, parameters.get(i));
					pstmn.executeUpdate();
					System.out.println("Data should have been inserted");

				} catch (SQLException e) {
					System.out.printf("Qualcosa e' andato storto: %s%n", e.getMessage());
				}
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
			}
		}
	}

	/**
	 * Utility method that create a string like (?, ?, [...], ?) where the number of
	 * question mark correspond to the quantity parameter
	 *
	 * @param quatity
	 *            the number of question marks
	 * @return the requested string
	 */
	protected String questionMarks(int quantity) {
		String result = "(";
		for (int i = 0; i < quantity - 1; i++)
			result = result + "?,";
		result += "?)";
		return result;
	}

	/**
	 * Utility method that create a string like (?, ?, [...], ?) where the number of
	 * question mark correspond to the quantity parameter. It also check if some
	 * parameter need to be left as DEFAULT.
	 *
	 * @param info
	 *            the Row that specifies default values and number of parameters
	 * @return the requested string
	 */
	protected String questionMarks(Row info) {
		String result = "(";

		// It shouldn't be null, but in case it is, this case is handled
		if (info.getDefaults() == null)
			return questionMarks(info.size());

		for (int i = 0; i < info.size() - 1; i++)
			if (info.getDefaults().get(i))
				result = result + "DEFAULT,";
			else
				result = result + "?,";

		if (info.getDefaults().get(info.size() - 1))
			result = result + "DEFAULT)";
		else
			result = result + "?)";
		return result;
	}

	/**
	 * Reset all the filter previously setted for the queries
	 * 
	 * @return ture if there were filters setted, false otherwise
	 */
	public boolean resetFilters() {
		boolean result = false;
		// for now only date filter
		result = result || filterDate;
		filterDate = false;
		filterEndDate = null;
		filterStartDate = null;

		return result;
	}

	/**
	 * Set the date period to filter out the results of the queries. Note, only
	 * queries with a time or date columns will be filtered. If one of the date is
	 * passed null that will be ignored.
	 * 
	 * @param startDate
	 *            Starting date of the intervall
	 * @param endDate
	 *            Ending date of the intervall
	 */
	public void setFilterDate(Date startDate, Date endDate) throws IllegalAccessError {
		if ((startDate != null) && (endDate != null))
			if (startDate.getTime() > endDate.getTime())
				throw new IllegalArgumentException();

		if (filterDate = (startDate != null) || (endDate != null)) {
			filterEndDate = endDate;
			filterStartDate = startDate;
		}

	}

	/**
	 * Recieve a Row object and set the Types and Columns name with the metadata
	 * retrieved from the table represented.
	 *
	 * @param row
	 *            Row to be setted
	 * @throws SQLException
	 */
	public void setRowMetadata(Row row) throws SQLException {
		if (row == null)
			row = new Row();
		row.setColumnNames(rowMetadata.getColumnNames());
		row.setTypes(rowMetadata.getTypes());
	}

	/**
	 * Delete a row if the database constraints allow it. The row needs to have
	 * column name setted correctly
	 *
	 * @param r
	 *            the row that need to be deleted
	 * @return true if the row has been deleted, false otherwise
	 * @throws SQLException
	 */
	public boolean tryDelete(Row r) throws SQLException {
		String sql = "DELETE FROM  " + SCHEMA_NAME + "." + entityName + " WHERE ";
		PreparedStatement st = null;
		boolean result = false;
		try {
			for (int i = 0; i < r.size() - 1; i++)
				if (r.get(i) == null)
					sql += r.getColumnNames().get(i) + " IS NULL  AND ";
				else
					sql += r.getColumnNames().get(i) + " = ?  AND ";
			if (r.get(r.size() - 1) == null)
				sql += r.getColumnNames().get(r.size() - 1) + " IS NULL; ";
			else
				sql += r.getColumnNames().get(r.size() - 1) + " = ? ;";

			st = connection.prepareStatement(sql);
			int k = 1;
			for (int i = 0; i < r.size(); i++)
				if (r.get(i) != null)
					st.setString(k++, r.get(i));
			result = st.executeUpdate() > 0;

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				st = null;
			}
		}
		return result;
	}

}

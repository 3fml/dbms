package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class AllVisitOfPatientFrame extends JPanel {

	private AllVisitOfPatientPanel myPanel;
	private JPanel contentPane;
	private MainWindowHDC parent;
	private JTextField txtInserisciNomeEo;
	private JButton btnNewButton;
	private JScrollPane scrollPane;
	private JTable table;
	private Connection con;
	private PatientRow selectedPatient = null;
	private JLabel lblNewLabel;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")

	public AllVisitOfPatientFrame(final MainWindowHDC parent, final AllVisitOfPatientPanel panel) {
		this.parent = parent;
		myPanel = panel;
		con = parent.getConnection();

		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("150px"), FormSpecs.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("default:grow"), }));

		// STRINGA CON CODICE FISCALE PAZIENTE

		// ----------------------------------------------------

		Action enterAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.setRowCount(0);
						try {
							Patient p = new Patient(con);// Andrebbe usato Doctor, per cercare solo i pazienti di un
															// dottore, ma per ora cerco tra tutti i pazienti
							ArrayList<Row> result = p.searchVisits(panel.getPatient().getFiscalcode());

							// provare a mettere myPanel.getPatient().getFiscalcode()
							int i;
							for (i = 0; i < result.size(); i++) {
								Row currentRow = result.get(i);
								model.addRow(new Object[5]);

								if (currentRow.get(0) != null)
									model.setValueAt(currentRow.get(0), i, 0);
								else
									model.setValueAt("-", i, 0);

								if (currentRow.get(1) != null)
									model.setValueAt(currentRow.get(1), i, 1);
								else
									model.setValueAt("-", i, 1);
								if (currentRow.get(2) != null)
									model.setValueAt(currentRow.get(2), i, 2);
								else
									model.setValueAt("-", i, 2);

								if (currentRow.get(3) != null)
									model.setValueAt(currentRow.get(3), i, 3);
								else
									model.setValueAt("-", i, 3);
								if (currentRow.get(4) != null)
									model.setValueAt(currentRow.get(4), i, 4);
								else
									model.setValueAt("-", i, 4);

							}
							if (i == 0) {
								model.addRow(new Object[1]);
								model.setValueAt("Nessun risultato trovato", 0, 0);
							}
						} catch (SQLException | SQLInjectionException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
							e.printStackTrace();
						}
					}

				});
			}
		};

		// da eliminare perchè non faccio nessuna ricerca
		// txtInserisciNomeEo = new JTextField();
		// txtInserisciNomeEo.addActionListener(enterAction);
		// txtInserisciNomeEo.addFocusListener(new FocusAdapter() {
		// @Override
		// public void focusGained(FocusEvent e) {
		// if (txtInserisciNomeEo.getForeground() == Color.GRAY) {
		// // dato che sono pigro non creo un falg ma uso ciò che già ho
		// txtInserisciNomeEo.setText("");
		// txtInserisciNomeEo.setForeground(Color.BLACK);
		// }
		// }
		// });

		// txtInserisciNomeEo.setForeground(Color.GRAY);
		// txtInserisciNomeEo.setText("Inserisci nome e/o cognome");
		// this.add(txtInserisciNomeEo, "2, 2, fill, default");
		// txtInserisciNomeEo.setColumns(10);
		// ------------------------------------------------------------------------

		scrollPane = new JScrollPane();
		this.add(scrollPane, "2, 4, 3, 1, fill, fill");

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Numero Visita", "Note", "Data e Ora", "Cod.Fisc.Paziente", "Cod.Fisc.Dottore" }) {
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class, String.class };
			boolean[] columnEditables = new boolean[] { false, false, false, false, false };

			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// interazione in cui schiacciando sulla riga mi manda in un altra tabella
		// table.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseClicked(final MouseEvent mouseEvent) {
		// Point point = mouseEvent.getPoint();
		// final int row = table.getSelectedRow();
		// try {
		// if (mouseEvent.getButton() == MouseEvent.BUTTON1 && table.rowAtPoint(point)
		// != -1)
		// if (mouseEvent.getClickCount() == 1) {
		// panel.setSelectedPatient(selectPatient(row));
		// }
		// if (mouseEvent.getClickCount() == 2) {
		// parent.setView(new PatientPanel(parent, selectPatient(row)));
		// }
		// } catch (SQLException | SQLInjectionException e) {
		// JOptionPane.showMessageDialog(null, e.getMessage());
		// e.printStackTrace();
		// }
		// }
		// });

		TableColumnModel model = table.getColumnModel();
		model.getColumn(0).setPreferredWidth(120);
		model.getColumn(0).setMinWidth(50);
		model.getColumn(0).setMaxWidth(150);

		model.getColumn(1).setPreferredWidth(20);
		model.getColumn(1).setMinWidth(50);
		model.getColumn(1).setMaxWidth(150);

		model.getColumn(2).setPreferredWidth(50);
		model.getColumn(2).setMinWidth(120);
		model.getColumn(2).setMaxWidth(120);

		model.getColumn(3).setPreferredWidth(120);
		model.getColumn(3).setMinWidth(60);
		model.getColumn(3).setMaxWidth(100);

		model.getColumn(4).setPreferredWidth(120);
		model.getColumn(4).setMinWidth(150);
		model.getColumn(4).setMaxWidth(800);

		table.setGridColor(Color.GRAY);
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 11));
		scrollPane.setViewportView(table);

		scrollPane.setColumnHeaderView(lblNewLabel);
	}

	public PatientRow selectPatient(int row) throws SQLException, SQLInjectionException {
		return new Patient(con).getRow(new ArrayList<>(
				Arrays.asList(new String[] { (String) ((DefaultTableModel) table.getModel()).getValueAt(row, 2) })));

	}

}

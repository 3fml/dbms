package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class NewPathologyPanel extends PanelWithPatient {
	private NewPahologyFrame myFrame;
	private Visit visit;

	public NewPathologyPanel() {
		this(null, new PatientRow());
	}

	public NewPathologyPanel(final MainWindowHDC parent, PatientRow patient) {
		super(parent, patient);
		myFrame = new NewPahologyFrame();
		super.setFrame(myFrame);
		visit = Visit.getIstance();

		JButton button = new JButton("Salva");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.salva();
				clean();
				parent.goBack();
			}
		});

		JButton btnCancellaUltimo = new JButton("Cancella ultimo");
		btnCancellaUltimo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.removeLast();
			}
		});
		
		JLabel lblInserimentoPatologia = new JLabel("Inserimento Patologia:");
		add(lblInserimentoPatologia, "2, 1");
		add(btnCancellaUltimo, "2, 2");

		JButton btnCancellaTutto = new JButton("Cancella tutto");
		btnCancellaTutto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.removeEverything();
			}
		});
		add(btnCancellaTutto, "2, 4");
		button.setBackground(new Color(60, 179, 113));
		add(button, "2, 6");
	}

}

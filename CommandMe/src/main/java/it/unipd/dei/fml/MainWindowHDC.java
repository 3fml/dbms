package it.unipd.dei.fml;

import java.awt.Component;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class MainWindowHDC extends JFrame {

	private static MainWindowHDC istance = null;
	// Flag per gli switch:
	// - pro: semplice da riealizzare,
	// - contro: prolisso, ogni view ha bisogno del suo flag
	public final static int DEFAULT_VIEW = 1;
	public final static int BACK_VIEW = 2;

	public final static int PAZIENTE_RICERCA_VIEW = 1;
	public final static int PAZIENTE_VIEW = 11;

	public final static int PAZIENTE_INFORMAZIONI_VIEW = 111;
	public final static int RICERCA_AVANZATA_VIEW = 2;
	public final static int CERCA_FARMACO_VIEW = 3;
	public final static int IMPOSTAZIONI_VIEW = 4;

	public static MainWindowHDC createIstance(Component parent, Connection connection, DoctorRow doctor) {
		if (istance == null)
			return istance = new MainWindowHDC(parent, connection, doctor);
		else
			throw new IllegalStateException();
	}

	public static MainWindowHDC getIstance() {
		if (istance == null)
			throw new IllegalStateException();
		else
			return istance;
	}

	// Realizzazione equivalente con una Map
	// private final Map<Integer,ViewBox> viewComponentsMap = new
	// HashMap<Integer,ViewBox>();

	private JPanel contentPane;
	private Component parent;
	private Connection connection;

	// Panels:
	private MainPanel mainPanel;

	// Panel e Frame attualmente mostrati
	private VoidPanel currentPanel;
	// Panel e Frame prececdenti, non sempre vengono inseriti nello stack quindi li
	// memorizzo qui
	private VoidPanel precedentPanel;

	// Stack per permettere di tornare indietro
	private Stack<VoidPanel> precedentPanelStack = new Stack<>();
	private DoctorRow doctor;

	/**
	 * Finestra principale, fa da contenitore per un oggetto JPanel (sinistra) e un
	 * oggetto JFrame (destra). A seconda delle azioni i pannelli e i frame verranno
	 * cambiati mostrandoli però sempre in questa finestra
	 */
	private MainWindowHDC(Component parent, Connection connection, DoctorRow doctor) {
		this.parent = parent;
		this.connection = connection;
		this.doctor = doctor;
		this.setIconImage(new ImageIcon("resources\\icon.png").getImage());
		
		mainPanel = new MainPanel(this);
		precedentPanel = mainPanel;
		currentPanel = mainPanel;

		setTitle("Health Data Collection");
		setMinimumSize(new Dimension(850, 546));
		setSize(new Dimension(850, 546));

		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("130dlu"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(197dlu;min):grow"), },
				new RowSpec[] { RowSpec.decode("min:grow"), }));

		contentPane.add(mainPanel, "2, 1, fill, fill");
		contentPane.add(mainPanel.getFrame(), "4, 1, fill, fill");

		precedentPanelStack.push(mainPanel);
	}

	@Override
	public void dispose() {

		// Le cose da fare prima di chiudere

		super.dispose();
	}

	@Override
	public void finalize() {
		try {
			if (!connection.isClosed())
				connection.close();
		} catch (SQLException e) {
			connection = null;
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public DoctorRow getCurrentDoctor() {
		return doctor;
	}

	public void goBack() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				precedentPanel = currentPanel;
				currentPanel = precedentPanelStack.pop();
				updateView();
			}
		});
	}

	public void goHome() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				while (!precedentPanelStack.isEmpty())
					precedentPanelStack.pop();
				precedentPanelStack.push(mainPanel);
				precedentPanel = currentPanel;
				currentPanel = mainPanel;
				updateView();
				mainPanel.clean();
			}
		});

	}

	public void saveView() {
		precedentPanel = currentPanel;
		precedentPanelStack.push(currentPanel);
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void setView(final VoidPanel panel) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				saveView();
				currentPanel = panel;
				updateView();
			}
		});
	}

	public void updateView() {
		// Pannello
		precedentPanel.setVisible(false);
		contentPane.remove(precedentPanel);
		contentPane.add(currentPanel, "2, 1, fill, fill");
		currentPanel.setVisible(true);

		// Finestra di lavoro
		precedentPanel.getFrame().setVisible(false);
		contentPane.remove(precedentPanel.getFrame());
		contentPane.add(currentPanel.getFrame(), "4, 1, fill, fill");

		currentPanel.getFrame().setVisible(true);

		revalidate();
	};

}

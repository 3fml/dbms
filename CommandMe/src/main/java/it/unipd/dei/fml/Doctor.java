package it.unipd.dei.fml;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class that represent the "doctor" table, it has some of the most useful
 * funtionality in order to manage doctor related operations.
 */
class Doctor extends Entity {
	/**
	 * Constructor, it store the connection that will be used in this instace
	 *
	 * @param con
	 * @throws SQLException
	 * @throws SQLInjectionException
	 */
	public Doctor(Connection con) throws SQLException, SQLInjectionException {
		super(con, "doctor");
	}

	/**
	 * Recieve a fiscal code of a doctor (Primary Key) and return and object Row
	 * with all the information of the selected doctor.
	 *
	 * @param fiscalCode
	 *            of the desired doctor
	 * @return Row object with all the information of the doctor selected
	 * @throws SQLException
	 */
	public Row getDoctor(String fiscalCode) throws SQLException {
		ArrayList<String> wrapper = new ArrayList<>();
		wrapper.add(fiscalCode);
		return getRow(wrapper);
	}

	/**
	 * Return the domicile of a given doctor
	 *
	 * @param fiscalCode
	 *            of the doctor
	 * @return Row with domicile information (city name, province name, nation name
	 *         and addres informations)
	 * @throws SQLException
	 */
	public Row getDomicile(String fiscalCode) throws SQLException {
		String sql = "SELECT cityname, provname, name, L.* FROM  " + SCHEMA_NAME + ".doctorlive AS L " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".city AS C  " + "NATURAL JOIN " + SCHEMA_NAME + ".province AS P  " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".nation AS N  " + "WHERE ? = L.fiscalcode AND L.residencydomicile = 'Domicilio';";
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Return the residency of a given doctor
	 *
	 * @param fiscalCode
	 *            of the doctor
	 * @return Row with residency information (city name, province name, nation name
	 *         and addres informations)
	 * @throws SQLException
	 */
	public Row getResindency(String fiscalCode) throws SQLException {
		String sql = "SELECT cityname, provname, name, L.* FROM  " + SCHEMA_NAME + ".doctorlive AS L " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".city AS C  " + "NATURAL JOIN " + SCHEMA_NAME + ".province AS P  " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".nation AS N  " + "WHERE ? = L.fiscalcode AND L.residencydomicile = 'Residenza';";
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	public DoctorRow getRow(String fiscalCode) throws SQLException {
		ArrayList<String> tmp = new ArrayList<>(1);
		tmp.add(fiscalCode);
		return new DoctorRow(super.getRow(tmp));
	}

	/**
	 * List all the active patient of a given doctor
	 * 
	 * SQL Code:
	 * 
	 * SELECT P.*, Reg.startdate, Reg.enddate, Reg.timenumber FROM fml.register AS
	 * Reg INNER JOIN fml.patient AS P ON P.fiscalcode = Reg.fiscalcode2 WHERE ? =
	 * Reg.fiscalcode AND Reg.enddate IS NULL ORDER BY P.surname ASC;
	 *
	 * @param fiscalCode
	 *            of the desired doctor
	 * @return ArrayList with patients' information for each line
	 * @throws SQLException
	 */
	public ArrayList<Row> listPatients(String fiscalCode) throws SQLException {
		String sql = "SELECT P.*, Reg.startdate, Reg.enddate, Reg.timenumber FROM  " + SCHEMA_NAME + ".register AS Reg "
				+ "INNER JOIN " + SCHEMA_NAME + ".patient AS P ON P.fiscalcode = Reg.fiscalcode2 "
				+ "WHERE ? = Reg.fiscalcode AND Reg.enddate IS NULL " + "ORDER BY P.surname ASC; ";
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}
		return result;
	}

	/**
	 * Create a new doctor with the parametes recieved IMPORTANT: this method let
	 * you set also the patient counter of a doctor row, DO NOT do that, leave this
	 * operation to the Database triggers set that column as defaults --> true.
	 * (Unless you know exactly what you are doing)
	 *
	 * @param param
	 *            Row object with all the doctor informations
	 * @throws SQLException
	 */
	public void newDoctor(Row param) throws SQLException {
		createGenericInstance(param);
	}

	/**
	 * List all additional services made by a doctor
	 *
	 * @param fiscalCode
	 *            of the doctor
	 * @return ArrayList with additional services information for each row (plus
	 *         basic information about the patient)
	 * @throws SQLException
	 */
	public ArrayList<Row> searchAdditionalServices(String fiscalCode) throws SQLException {
		String sql = "SELECT AdSv.*, P.name, P.surname, P.fiscalcode FROM " + SCHEMA_NAME + ".execute AS Ex "
				+ "INNER JOIN " + SCHEMA_NAME
				+ ".additionalservices AS AdSv ON Ex.description = AdSv.description AND Ex.exdate = AdSv.asdate "
				+ "INNER JOIN " + SCHEMA_NAME + ".patient AS P ON Ex.fiscalcode2 = P.fiscalcode "
				+ "WHERE ? = Ex.fiscalcode; ";
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Given a String with the name and/or surname separated from a space, search
	 * all the doctor if there are matches.
	 *
	 * @param nameAndSurname
	 *            Name and/or surname (or part of them) of the doctor
	 * @return List of all the doctors that match those informations
	 * @throws SQLException
	 */
	public ArrayList<DoctorRow> searchDoctorByName(String nameAndSurname) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<DoctorRow> result = null;
		try {
			String[] splits = nameAndSurname.split(" ");
			String sql = "SELECT * FROM " + SCHEMA_NAME + ".doctor WHERE UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			for (int i = 0; i < splits.length - 1; i++)
				sql += "AND  UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			sql += ";";

			pst = connection.prepareStatement(sql);
			for (int i = 0; i < splits.length; i++)
				pst.setString(i + 1, "%" + splits[i] + "%");
			rs = pst.executeQuery();
			result = getMultipleDoctorRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Return the list of the patients' informations selected by name and/or
	 * surname. SQL code:
	 * 
	 * SELECT * FROM fml.patient WHERE UPPER(name || ' ' || surname) LIKE UPPER(?)
	 * AND UPPER(name || ' ' || surname) LIKE UPPER(?) AND fiscalcode = ? ORDER BY
	 * name ASC;
	 *
	 *
	 * @param nameAndSurname
	 *            contain the name and surname separated by a space, note that both
	 *            name and surname can contain multiple world inside them.
	 * @return the patients PatientRow list
	 * @throws SQLException
	 */
	public ArrayList<PatientRow> searchPatientByName(String nameAndSurname, String doctorFiscalCode)
			throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<PatientRow> result = null;
		try {
			String[] splits = nameAndSurname.split(" ");
			String sql = "SELECT * FROM " + SCHEMA_NAME + ".patient WHERE UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			for (int i = 0; i < splits.length - 1; i++)
				sql += "AND  UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			sql += " AND fiscalcode = ? ORDER BY name ASC;";

			pst = connection.prepareStatement(sql);
			for (int i = 0; i < splits.length; i++)
				pst.setString(i + 1, "%" + splits[i] + "%");
			pst.setString(splits.length + 1, doctorFiscalCode);
			rs = pst.executeQuery();
			result = getMultiplePatientRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Search fo all the prescription made by a doctor. Note that it list all the
	 * prescriprion information, plus a biref description of what has been
	 * prescribed, plus what type of thing has been prescribed (e.g. medicine, cvp)
	 * and also basic information about the patient
	 *
	 * @param fiscalCode
	 *            of the doctor
	 * @return the list of prescription related informations
	 * @throws SQLException
	 */
	public ArrayList<Row> searchPrescriptions(String fiscalCode) throws SQLException {
		String sql = "WITH sub AS ( " + "SELECT Pr.*, V.visittimestamp, P.name, P.surname, P.fiscalcode FROM "
				+ SCHEMA_NAME + ".visit AS V " + "INNER JOIN " + SCHEMA_NAME
				+ ".prescription AS Pr ON Pr.visitcounterid = V.counterid " + "INNER JOIN " + SCHEMA_NAME
				+ ".patient AS P ON P.fiscalcode = V.fiscalcode " + "WHERE ? = V.fiscalcode2 " + ") "
				+ "SELECT *, NULL AS description, 'Custom' AS type, NULL AS id FROM sub " + "UNION "
				+ "SELECT sub.*, cvp.descriptionNtr, 'cvp', cvp.cvp " + "FROM sub NATURAL JOIN " + SCHEMA_NAME
				+ ".indicate2 AS I2 NATURAL JOIN " + SCHEMA_NAME + ".cvp " + "UNION "
				+ "SELECT sub.*, cluster.description, 'cluster', cluster.code " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate3 AS I3 " + "NATURAL JOIN " + SCHEMA_NAME + ".cluster " + "UNION "
				+ "SELECT sub.*, dayservice.description, 'dayservice', dayservice.code " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate4 AS I4 " + "NATURAL JOIN " + SCHEMA_NAME + ".dayservice " + "UNION "
				+ "SELECT sub.*, medicine.namepack, 'medicine', medicine.aic " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate1 AS I1 " + "NATURAL JOIN " + SCHEMA_NAME + ".medicine;";
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();

			result = getMultipleRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Search for all the visit performed by a doctor
	 *
	 * @param fiscalCode
	 *            of the doctor
	 * @return the list of all the visit-related infromation
	 * @throws SQLException
	 */
	public ArrayList<Row> searchVisits(String fiscalCode) throws SQLException {
		String sql = "SELECT V.*, P.name, P.surname FROM " + SCHEMA_NAME + ".visit As V " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".patient AS P " + "WHERE ? = V.fiscalcode2; ";
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Store the domicile passed as parameters
	 *
	 * @param row
	 *            List of all the information
	 * @throws SQLException
	 */
	public void setDomicile(Row row) throws SQLException {
		row.set(3, "Domicilio");
		try {
			Entity temp = new Entity(connection, "doctorlive");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Store the domicile of a doctor setting each parameter one by one
	 *
	 * @param fiscalCode
	 *            fiscal code of the doctor
	 * @param place
	 *            object Place that represent the city, his province and the nation
	 * @param street
	 *            the street address
	 * @param civicNumber
	 *            the civic number
	 * @param cap
	 *            the ZIP code of the city
	 * @throws SQLException
	 */
	public void setDomicile(String fiscalCode, Place place, String street, String civicNumber, String cap)
			throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".doctorlive VALUES (?,?,?,?,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, place.getCity());
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, "Domicilio");
			pstmn.setString(4, street);
			pstmn.setString(5, civicNumber);
			pstmn.setString(6, cap);

			pstmn.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Store the Residency passed as parameters
	 *
	 * @param row
	 *            List of all the information
	 * @throws SQLException
	 */
	public void setResindecy(Row row) throws SQLException {
		row.set("residencydomicile", "Residenza");
		try {
			Entity temp = new Entity(connection, "doctorlive");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Store the residency of a doctor setting each parameter one by one
	 *
	 * @param fiscalCode
	 *            fiscal code of the doctor
	 * @param place
	 *            object Place that represent the city, his province and the nation
	 * @param street
	 *            the street address
	 * @param civicNumber
	 *            the civic number
	 * @param cap
	 *            the ZIP code of the city
	 * @throws SQLException
	 */
	public void setResindecy(String fiscalCode, Place place, String street, String civicNumber, String cap)
			throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".doctorlive VALUES (?,?,?,?,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, place.getCity());
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, "Residenza");
			pstmn.setString(4, street);
			pstmn.setString(5, civicNumber);
			pstmn.setString(6, cap);

			pstmn.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

}

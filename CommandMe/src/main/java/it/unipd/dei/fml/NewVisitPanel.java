package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;

public class NewVisitPanel extends PanelWithPatient {

	private NewPrescriptionPanel newPrescriptionPanel;
	private NewVisitFrame myFrame;
	private NewMeasurePanel newMeasurePanel;
	private NewExamPanel newExamPanel;
	private NewPathologyPanel newPathologyPanel;

	public NewVisitPanel() {
		this(null, new PatientRow());
	}

	/**
	 * PANNELLO CONTENENTE TUTTE LE INFORMAZIONI SUL PAZIENTE.
	 */
	public NewVisitPanel(final MainWindowHDC parent, final PatientRow patient) {
		super(parent, patient);
		myFrame = new NewVisitFrame(this);
		super.setFrame(myFrame);

		JLabel lblAggiungi = new JLabel("Aggiungi a Visita:");
		add(lblAggiungi, "2, 1");

		JButton btnNewButton_2 = new JButton("Misurazione");
		btnNewButton_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newMeasure();
			}
		});
		
				JButton btnPatologia = new JButton("Patologia");
				btnPatologia.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						newPathology();
					}
				});
				add(btnPatologia, "2, 2");
		add(btnNewButton_2, "2, 4");
		
				JButton btnNewButton = new JButton("Prescrizione");
				btnNewButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						newPrescription();
					}
				});
				add(btnNewButton, "2, 6");
		
				JButton btnNewButton_1 = new JButton("Esame");
				btnNewButton_1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						newExam();
					}
				});
				add(btnNewButton_1, "2, 8");

		JSeparator separator = new JSeparator();
		add(separator, "2, 9");

		JButton btnSalva = new JButton("Salva Visita");
		btnSalva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.saveVisit();
				clean();
				parent.goBack();
			}
		});

		btnSalva.setBackground(new Color(60, 179, 113));
		add(btnSalva, "2, 10");
	}

	@Override
	public void clean() {
		// System.out.println(this.toString().substring(0, 30)+"\n");

		if (newPrescriptionPanel != null) {
			newPrescriptionPanel.clean();
			newPrescriptionPanel = null;
		}
		if (newMeasurePanel != null) {
			newMeasurePanel.clean();
			newMeasurePanel = null;
		}
		if (newExamPanel != null) {
			newExamPanel.clean();
			newExamPanel = null;
		}
		if (newPathologyPanel != null) {
			newPathologyPanel.clean();
			newPathologyPanel = null;
		}
	}

	public void newExam() {
		parent.setView(newExamPanel = new NewExamPanel(parent, getPatient()));
	}

	public void newMeasure() {
		parent.setView(newMeasurePanel = new NewMeasurePanel(parent, getPatient()));
	}

	public void newPathology() {
		parent.setView(newPathologyPanel = new NewPathologyPanel(parent, getPatient()));
	}

	public void newPrescription() {
		parent.setView(newPrescriptionPanel = new NewPrescriptionPanel(parent, getPatient()));
	}

}

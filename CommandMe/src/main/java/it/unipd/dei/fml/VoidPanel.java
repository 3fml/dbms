package it.unipd.dei.fml;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 * Come funziona la questione "Pannelli di controllo" in questo programma:
 * Qualsiasi pannello di controllo eredita direttamente o indirettamente dalla
 * classe VoidPanel (questa classe). La classe VoidPanel è una classe astratta
 * che pone alcune regole per il suo utilizzo (anche se non tutte quelle
 * necessarie). - Il colore di sfondo viene scelto qui; - Ogni pannello di
 * controllo ha un suo pannello denominato (erroneamente per ragioni "storiche")
 * frame; - Il frame di ogni pannello è contenuto in myFrame e puoi essere
 * impostato e ottenuto con gli opportuni metodi setter e getter - Ogni Pannello
 * di controllo deve contenere il metodo clean che svolge la funzione di
 * preparare per il GarbageCollector tutti i sottopannelli generati.
 * 
 * Ci sono altre indicazioni per l'utilizzo delle classi figlie: Dato che non ho
 * trovato un modo per impostare myFrame con un frame specifico creato dalla
 * sottoclasse, direttamente dal costruttore di VoidPanel, nei costruttori delle
 * classi figle prima verrà chiamato il costruttore "super(parent)" e subito
 * dopo verrà creato il "frame" e impostato con setFrame(...); In questo modo
 * ogni classe figlia avrà il suo "frame" personale specializzato che però potrà
 * essere automaticamente restituito con i metodi getter e settere di VoidPanel
 * (che avendo come frame JPanel è generico e può contenere una qualunque sua
 * specializzazione). Esempio:
 * 
 * public PatientPanel(final MainWindowHDC parent) { super(parent); myFrame =
 * new PatientFrame(); super.setFrame(myFrame); [...]
 * 
 */
public abstract class VoidPanel extends JPanel {

	protected MainWindowHDC parent;

	private JPanel myFrame;

	public VoidPanel(MainWindowHDC parent) {
		this.parent = parent;
		myFrame = new JPanel();
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setBackground(new Color(170, 255, 230));

		setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("173px"), },
				new RowSpec[] { RowSpec.decode("27px"), RowSpec.decode("fill:42px"), FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("fill:42px"), FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:42px"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:42px"), FormSpecs.UNRELATED_GAP_ROWSPEC,
						RowSpec.decode("fill:42px"), FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("1dlu:grow"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("1dlu:grow"), FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("1px:grow"), FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:42px"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:42px"), RowSpec.decode("27px"), }));
	}

	/**
	 * Metodo importante per il corretto funzionamento della funzione di "go-back" e
	 * "go-home" in quanto permette di resettare a cascata tutti gli oggetti Panel
	 * istanziati.
	 */
	public abstract void clean();

	public JPanel getFrame() {
		return myFrame;
	}

	public void setFrame(JPanel frame) {
		myFrame = frame;
	}

}

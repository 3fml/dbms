package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class NewExamPanel extends PanelWithPatient {
	private NewExamFrame myFrame;
	private Visit visit;

	public NewExamPanel() {
		this(null, new PatientRow());
	}

	public NewExamPanel(final MainWindowHDC parent, PatientRow patient) {
		super(parent, patient);
		myFrame = new NewExamFrame();
		super.setFrame(myFrame);
		visit = Visit.getIstance();

		JButton btnSalva = new JButton("Salva");
		btnSalva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				myFrame.salva();
				clean();
				parent.goBack();
			}
		});
		
		JLabel lblMenuAzioni = new JLabel("Inserimento Esame:");
		add(lblMenuAzioni, "2, 1");
		btnSalva.setBackground(new Color(60, 179, 113));
		add(btnSalva, "2, 2");
	}
}

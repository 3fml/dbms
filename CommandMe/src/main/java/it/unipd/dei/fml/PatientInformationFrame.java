package it.unipd.dei.fml;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class PatientInformationFrame extends JPanel {

	private PatientInformationPanel myPanel;

	/**
	 * PANNELLO CONTENENTE TUTTE LE INFORMAZIONI SUL PAZIENTE.
	 */

	public PatientInformationFrame(PatientInformationPanel panel) {
		myPanel = panel;
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_ROWSPEC, }));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128)), "Recap Paziente:",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		this.add(panel_1, "2, 2, fill, fill");

		JLabel lblNome = new JLabel("Nome:");

		JLabel lblCognome = new JLabel("Cognome:");

		JLabel lblCodiceFiscale = new JLabel("Codice Fiscale:");

		JLabel lblEt = new JLabel("Età:");

		JLabel label = new JLabel(panel.getPatient().getName());

		JLabel label_1 = new JLabel(panel.getPatient().getSurname());

		JLabel label_2 = new JLabel(panel.getPatient().getFiscalcode());

		JLabel label_3 = new JLabel();
		JLabel label_5 = new JLabel();
		if (panel.getPatient().getDob() != null) {
			LocalDate date = panel.getPatient().getDob().toLocalDate();
			label_3.setText(Integer.toString(Period.between(date, LocalDate.now()).getYears()));
			label_5.setText(new SimpleDateFormat("dd-MM-yyyy").format(panel.getPatient().getDob()));
		} else {
			label_3.setText("-");
			label_5.setText("-");
		}

		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.UNRELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.UNRELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.UNRELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default"),
				FormSpecs.RELATED_GAP_ROWSPEC,}));
		panel_1.add(lblNome, "2, 2, fill, top");

		JLabel lblIndirizzo = new JLabel("Indirizzo:");
		panel_1.add(lblIndirizzo, "10, 2");
				
				JSeparator separator_1 = new JSeparator();
				separator_1.setOrientation(SwingConstants.VERTICAL);
				panel_1.add(separator_1, "14, 2, 1, 12");
		
				JLabel lblNumeroVisiteEffettuate = new JLabel("Numero Visite Effettuate:");
				panel_1.add(lblNumeroVisiteEffettuate, "16, 2");
		panel_1.add(lblCognome, "2, 4, fill, top");

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		panel_1.add(separator, "8, 2, 1, 12");

		JLabel lblNewLabel = new JLabel("Città:");
		panel_1.add(lblNewLabel, "10, 4");
		panel_1.add(lblCodiceFiscale, "2, 6, fill, top");

		JLabel lblNewLabel_1 = new JLabel("Telefono:");

		JLabel label_4 = new JLabel(panel.getPatient().getGender().equals("m") ? "Maschio" : "Femmina");
		panel_1.add(lblNewLabel_1, "10, 6");
		panel_1.add(lblEt, "2, 8, fill, top");
		panel_1.add(label, "4, 2, fill, top");
		panel_1.add(label_1, "4, 4, fill, top");
		panel_1.add(label_2, "4, 6, fill, top");
		panel_1.add(label_3, "4, 8, fill, top");
		
				JLabel lblFamiliari = new JLabel("Familiari:");
				panel_1.add(lblFamiliari, "16, 8");
		panel_1.add(label_5, "4, 12, fill, top");
		panel_1.add(label_4, "4, 10, fill, top");

		JLabel lblNewLabel_2 = new JLabel("Cellulare:");
		panel_1.add(lblNewLabel_2, "10, 8");

		JLabel lblGenere = new JLabel("Genere:");
		lblGenere.setVerticalTextPosition(SwingConstants.TOP);
		lblGenere.setVerticalAlignment(SwingConstants.TOP);
		panel_1.add(lblGenere, "2, 10");

		JLabel lblDataDiNascita = new JLabel("Data di nascita:");
		panel_1.add(lblDataDiNascita, "2, 12");

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128)), "Visite recenti:",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.add(panel_2, "2, 4, fill, fill");
	}

}

package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class NewExamFrame extends JPanel {
	private JTextArea textArea;
	private String filepath;
	private JFileChooser fc;
	private Object semaphore;
	private JLabel lblFilepaht;

	public NewExamFrame() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fc = new JFileChooser();
			}
		});

		setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("40dlu"), FormSpecs.UNRELATED_GAP_COLSPEC,
						ColumnSpec.decode("60dlu"), FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.DEFAULT_ROWSPEC, FormSpecs.UNRELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.UNRELATED_GAP_ROWSPEC, }));

		JLabel lblOraEData = new JLabel("Ora e Data:");
		add(lblOraEData, "2, 1");

		JSpinner oraSpinner = new JSpinner();
		oraSpinner.setModel(new SpinnerDateModel());
		oraSpinner.setEditor(new JSpinner.DateEditor(oraSpinner, "HH:mm"));
		add(oraSpinner, "4, 1");

		JSpinner dataSpinner = new JSpinner();
		dataSpinner.setModel(new SpinnerDateModel());
		dataSpinner.setEditor(new JSpinner.DateEditor(dataSpinner, "dd/MM/yyyy"));
		add(dataSpinner, "6, 1");

		JLabel lblNote = new JLabel("Dettagli:");
		add(lblNote, "2, 3");

		textArea = new JTextArea();
		textArea.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(textArea, "4, 3, 7, 3, fill, fill");

		JLabel lblAllegaFilePdf = new JLabel("Allega file PDF:");
		add(lblAllegaFilePdf, "2, 7");

		JButton btnAllega = new JButton("Allega...");
		btnAllega.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (JFileChooser.APPROVE_OPTION == fc.showOpenDialog(MainWindowHDC.getIstance())) {
							File file = fc.getSelectedFile();
							lblFilepaht.setText(file.getPath());
						}
					}
				});
			}
		});

		lblFilepaht = new JLabel("");
		add(lblFilepaht, "4, 7, 5, 1");
		add(btnAllega, "10, 7");

	}

	public void salva() {
		if (textArea.getText().length() > 0) {
			Visit visit = Visit.getIstance();
			visit.exam.present = true;
			visit.exam.notes = textArea.getText();
			visit.exam.filePath = filepath;
		}
	}

}

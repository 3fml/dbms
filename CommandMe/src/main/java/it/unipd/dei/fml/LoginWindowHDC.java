package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Prima classe lanciata, crea una finestra che gestisce il login e poi lancerà
 * le altre.
 * 
 */
public class LoginWindowHDC {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					LoginWindowHDC window = new LoginWindowHDC();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JFrame frmLogin;
	private JTextField userField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton avanzateButton;
	private JPanel panel;
	private JLabel hostLabel;
	private JTextField hostField;
	private JLabel portaLabel;
	private JTextField portaField;
	private JLabel dbLabel;
	private JTextField dbField;

	private JButton btnNewButton;
	private String server = "localhost";
	private String port = "5432";
	private String dbname = "dbms1718";
	private String user = "webdb";
	private String pswd = "webdb";
	private String schemaUser = "vincenzoc";
	private String schemaPswd = "supersecret";

	private final String SCHEMA_NAME = "FML";
	private JLabel lblUser;
	private JLabel lblServerPassword;

	

	/**
	 * Create the application.
	 */
	public LoginWindowHDC() {
		initialize();

		frmLogin.setLocationRelativeTo(null);
		frmLogin.setVisible(true);
	}
	
		

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		frmLogin = new JFrame();
		try {
			frmLogin.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("resources\\\\iconBIG.png")))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		frmLogin.setIconImage(new ImageIcon("resources\\icon.png").getImage());
		frmLogin.getContentPane().setEnabled(false);
		frmLogin.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		frmLogin.setLocationByPlatform(true);
		frmLogin.setResizable(false);
		frmLogin.setTitle("Health Data Collection - Login");
		frmLogin.setSize(330, 347); //347
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(new FormLayout(
				new ColumnSpec[] { ColumnSpec.decode("4dlu:grow"), ColumnSpec.decode("default:grow(2)"),
						ColumnSpec.decode("default:grow"), ColumnSpec.decode("4dlu:grow"), },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("max(20dlu;default)"),
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, RowSpec.decode("1dlu"),
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						RowSpec.decode("1dlu"), FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("21px"), FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("1dlu")/*qui*/, FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("20px"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:32px"), FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));

		JLabel userLabel = new JLabel("Nome Utente");
		userLabel.setFont(UIManager.getFont("Label.font"));
		frmLogin.getContentPane().add(userLabel, "2, 4");
		userLabel.setLabelFor(userField);

		userField = new JTextField(schemaUser);
		frmLogin.getContentPane().add(userField, "2, 6, 2, 1, fill, default");
		userField.setColumns(10);

		JLabel passwordLabel = new JLabel("Password");
		frmLogin.getContentPane().add(passwordLabel, "2, 8, 2, 1");

		passwordLabel.setLabelFor(passwordField);

		passwordField = new JPasswordField(schemaPswd);
		frmLogin.getContentPane().add(passwordField, "2, 10, 2, 1, fill, default");
		passwordField.setColumns(10);

		btnNewButton = new JButton("Nuovo Utente");

		frmLogin.getContentPane().add(btnNewButton, "2, 14, 2, 1");

		avanzateButton = new JButton("Avanzate");
		frmLogin.getContentPane().add(avanzateButton, "2, 16, 2, 1");

		panel = new JPanel();
		panel.setVisible(false);
		panel.setBorder(null);
		frmLogin.getContentPane().add(panel, "2, 18, 2, 1, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("212px:grow"),},
			new RowSpec[] {
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				RowSpec.decode("1dlu"),
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				RowSpec.decode("1dlu"),
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				RowSpec.decode("1dlu"),
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		lblUser = new JLabel("Server Username");
		lblUser.setFont(UIManager.getFont("Label.font"));
		panel.add(lblUser, "1, 1");
		
		final JTextField txtWebdb = new JTextField("webdb");
		txtWebdb.setForeground(Color.GRAY);
		txtWebdb.setColumns(10);
		panel.add(txtWebdb, "1, 3, fill, default");
		
		lblServerPassword = new JLabel("Server Password");
		panel.add(lblServerPassword, "1, 5");
		
		final JPasswordField pwdWebdb = new JPasswordField(pswd);
		pwdWebdb.setForeground(Color.GRAY);
		pwdWebdb.setColumns(10);
		panel.add(pwdWebdb, "1, 7, fill, default");

		hostLabel = new JLabel("Server");

		panel.add(hostLabel, "1, 9, fill, fill");

		hostField = new JTextField();

		hostField.setText(server);
		hostField.setForeground(Color.GRAY);
		hostField.setColumns(10);
		panel.add(hostField, "1, 11, fill, fill");

		portaLabel = new JLabel("Porta");
		panel.add(portaLabel, "1, 13, fill, fill");

		portaField = new JTextField();
		portaField.setText(port);
		portaField.setForeground(Color.GRAY);
		portaField.setColumns(10);
		panel.add(portaField, "1, 15, fill, fill");

		dbLabel = new JLabel("Nome Database");
		panel.add(dbLabel, "1, 17, fill, fill");

		dbField = new JTextField();
		dbField.setText(dbname);
		dbField.setForeground(Color.GRAY);
		dbField.setColumns(10);
		panel.add(dbField, "1, 19, fill, fill");

		loginButton = new JButton("Login");
		loginButton.setFont(UIManager.getFont("Button.font"));
		loginButton.setBackground(new Color(0, 255, 127));
		frmLogin.getContentPane().add(loginButton, "2, 22, 2, 1");

		// Listeners:
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							if (txtWebdb.getText() == "")
								user = "webdb";
							else
								user = txtWebdb.getText();
							if (hostField.getText() == "")
								server = "localhost";
							else
								server = hostField.getText();
							if (portaField.getText() == "")
								port = "5432";
							else
								port = portaField.getText();
							if (dbField.getText() == "")
								dbname = "dbms1718";
							else
								dbname = dbField.getText();
							if (userField.getText() == "")
								schemaUser = "vincenzoc";
							else
								schemaUser = userField.getText();

							Properties props = new Properties();
							props.setProperty("user", user);
							props.setProperty("password", new String(pwdWebdb.getPassword()));
							props.setProperty("stringtype", "unspecified");

							String url = "jdbc:postgresql://" + server + ":" + port + "/" + dbname;
							Connection con = DriverManager.getConnection(url, props);
							if (con.isValid(0)) {
								PreparedStatement pstmn = null;
								ResultSet rs = null;
								boolean check = false;
								String fiscalcode = null;
								try {
									String SQL = "SELECT password, fiscalcode FROM "+ SCHEMA_NAME + ".User WHERE username = ?";
									pstmn = con.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
									pstmn.setString(1, schemaUser);
									rs = pstmn.executeQuery();
									if (rs.next()) 
									{
										check = rs.getString(1).equals(new String(passwordField.getPassword()));
										fiscalcode = rs.getString(2);	
										System.out.println(fiscalcode+ " "+check);								
									}

									} catch (SQLException e) {
												throw e;
											} finally {
												try {
													if (pstmn != null)
														pstmn.close();
													if (rs != null)
														rs.close();
												} catch (SQLException e) {
													throw e;
												} finally {
													pstmn = null;
													rs = null;
												}
											}

								if (check)
								{
									
									DoctorRow result = null;
									try {
										Doctor d = new Doctor(con);
										result = d.getRow(fiscalcode);
										result.setUsername(schemaUser);
										
									} catch (SQLException | SQLInjectionException e) {
										e.printStackTrace();
									}
									MainWindowHDC frame = MainWindowHDC.createIstance(frmLogin, con, result);
									frame.setLocationRelativeTo(null);
									frame.setVisible(true);
								}
								else
								{
									JOptionPane.showMessageDialog(null, "Wrong credentials.");
									
								}	

							
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
							e.printStackTrace();
						}
					}
				});
				frmLogin.setVisible(false);
				frmLogin.dispose();
			}
		});

		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							NewUserWindowHDC frame = new NewUserWindowHDC(frmLogin);
							frame.setLocationRelativeTo(null);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				frmLogin.setVisible(false);
			}
		});

		avanzateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!panel.isVisible()) {
					// ridimensiono
					frmLogin.setSize(330, 580);
					// cambio altezza della riga con le impostazioni avanzate
					((FormLayout) frmLogin.getContentPane().getLayout()).setRowSpec(18, FormSpecs.DEFAULT_ROWSPEC);
					// mostro il pannello "Avanzate"
					panel.setVisible(true);
				} else {

					frmLogin.setSize(330, 347);
					((FormLayout) frmLogin.getContentPane().getLayout()).setRowSpec(18, RowSpec.decode("1dlu"));
					panel.setVisible(false);
				}

			}
		});
		
		txtWebdb.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtWebdb.getForeground() == Color.GRAY) {
					// dato che sono pigro non creo un falg ma uso ciò che già ho
					txtWebdb.setText("");
					txtWebdb.setForeground(Color.BLACK);
				}
			}
		});
		
		pwdWebdb.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (pwdWebdb.getForeground() == Color.GRAY) {
					// dato che sono pigro non creo un falg ma uso ciò che già ho
					pwdWebdb.setText("");
					pwdWebdb.setForeground(Color.BLACK);
				}
			}
		});

		hostField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (hostField.getForeground() == Color.GRAY) {
					// dato che sono pigro non creo un falg ma uso ciò che già ho
					hostField.setText("");
					hostField.setForeground(Color.BLACK);
				}
			}
		});

		portaField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (portaField.getForeground() == Color.GRAY) {
					portaField.setText("");
					portaField.setForeground(Color.BLACK);
				}
			}
		});

		dbField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (dbField.getForeground() == Color.GRAY) {
					dbField.setText("");
					dbField.setForeground(Color.BLACK);
				}
			}
		});

	}
}

package it.unipd.dei.fml;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Generic class for handling all the operation with Rows. In order to be as
 * generic as possible, some operation are not very confortable, for most used
 * tables and operations they should be implemented specific classes that extend
 * this one and specilize it.
 * <p>
 * It is basically an ArrayList with the values of a Row stored in String. With
 * the values are also stored the Columns name, and the corresponding original
 * type used in the database side.
 * <p>
 * It also permit the access of the values using the name of the relative
 * column.
 */
public class Row extends ArrayList<String> implements Cloneable {

	private int size = -1;
	private ArrayList<String> types;
	private ArrayList<String> names;
	private ArrayList<Boolean> defaults;
	private Map<String, Integer> label2index;

	/**
	 * Usually it used when the object will contain information to send to the
	 * database. It do not set anithing so it's importat to set metadata an values
	 * manually or using the metho setRowMetadata of the Entity class.
	 */
	public Row() {
		super();
		defaults = new ArrayList<>(size());
		label2index = null;
	}

	/**
	 * 
	 */
	public Row(int size) {
		super(size);
		defaults = new ArrayList<>(size());
		label2index = null;
	}

	/**
	 * Constructor that retrieve the metadata and the values from the current row
	 * pointe by a ResultSet initialize and setted correctly (ResultSet.next() has
	 * to be executed at least once before running this constructor)
	 *
	 * @param rs
	 *            ResultSet object setted correctly
	 * @throws SQLException
	 */
	public Row(ResultSet rs) throws SQLException {
		super();
		setValues(rs);
		setTypes(rs);
		setColumnNames(rs);
		defaults = new ArrayList<>(size());

	}

	/**
	 * Clone contructor
	 */
	public Row(Row r) {
		super(r);
		try {
			setColumnNames(r.getColumnNames());
			setDefaults(r.getDefaults());
			setTypes(r.getTypes());
		} catch (SQLException e) {
		} // non dovrebbe essere lanciata
		label2index = new HashMap<>(r.label2index);// posso accedere
	}

	@Override
	public Row clone() {
		return new Row(this);
	}

	/**
	 * Return the value corresponding to the "label" column name
	 *
	 * @param columnLabel
	 * @return
	 */
	public String get(String columnLabel) {
		return get(label2index.get(columnLabel));
	}

	/**
	 * Return the ArrayList of the column names
	 *
	 * @return
	 */
	public ArrayList<String> getColumnNames() {
		return names;
	}

	/**
	 * Return default array. Defaults array tell which column has to be considedered
	 * to be left to their default values when inserted in the database (so the
	 * corresponding values, if present is ignored)
	 *
	 * @return the list of boolean, true if left default, false if values is readed
	 */
	public ArrayList<Boolean> getDefaults() {
		if (defaults.size() != size()) {
			defaults = new ArrayList<>();
			for (int i = 0; i < size(); i++)
				defaults.add(false);
		}
		return defaults;
	}

	/**
	 * Return the ArrayList of SQL type of the stored values
	 *
	 * @return ArrayList of SQL type of the stored values
	 */
	public ArrayList<String> getTypes() {
		return types;
	}

	/**
	 * Set the valued at the position corresponding with the column name equals to
	 * columnLabel to the value "value"
	 *
	 * @param columnLabel
	 *            Column name of the value
	 * @param value
	 *            value to be setted
	 */
	public void set(String columnLabel, String value) {
		set(label2index.get(columnLabel), value);
	}

	/**
	 * Set the Column name ArrayList used for accessing data with labels
	 *
	 * @param newColumnNames
	 *            ArrayList with the new names
	 * @throws SQLException
	 */
	public void setColumnNames(ArrayList<String> newColumnNames) throws SQLException {
		names = new ArrayList<>(newColumnNames);
		label2index = new HashMap<>();
		size = newColumnNames.size();
		for (int i = 0; i < names.size(); i++)
			label2index.put(names.get(i), i);

		// check if the values correspond to the column, doesn't delete them, but in
		// case
		// add missing columns ad set them as NULL
		// if I have more values, those will be ignored, if less, will be added
		if (super.size() != names.size())
			for (int j = super.size(); j < names.size(); j++)
				add(null);
	}

	/**
	 * Set the columns name list and create the mapping object retriving the
	 * information from a ResultSet metadata. It also set size correctly.
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public void setColumnNames(ResultSet rs) throws SQLException {
		names = new ArrayList<>();
		label2index = new HashMap<>();
		int count = rs.getMetaData().getColumnCount();
		size = count;
		for (int i = 1; i <= count; i++) {
			String cName = rs.getMetaData().getColumnName(i);
			names.add(cName);
			label2index.put(cName, i - 1);
		}

		// check if the values correspond to the column, doesn't delete them, but in
		// case
		// add missing columns ad set them as NULL
		// if I have more values, those will be ignored, if less, will be added
		if (size() != names.size())
			for (int j = size(); j < names.size(); j++)
				add(null);
	}

	/**
	 * Set the default ArrayList containing the boolean flag (see getDefaults)
	 *
	 * @param info
	 *            ArrayList of Boolean, true if left default, false if values is
	 *            readed
	 */
	public void setDefaults(ArrayList<Boolean> info) {
		defaults = new ArrayList<>(info);
	}

	/**
	 * Set the SQL type of the stored values
	 *
	 * @param newTypes
	 *            ArrayList with the new types
	 * @throws SQLException
	 */
	public void setTypes(ArrayList<String> newTypes) throws SQLException {
		types = new ArrayList<>(newTypes);
	}

	/**
	 * Set the SQL type ArrayList corresponding to the stored values
	 *
	 * @param rs
	 *            ResultSet object from which getting the metadata
	 * @throws SQLException
	 */
	public void setTypes(ResultSet rs) throws SQLException {
		types = new ArrayList<>();
		int count = rs.getMetaData().getColumnCount();
		for (int i = 1; i <= count; i++)
			types.add(rs.getMetaData().getColumnTypeName(i));
	}

	/**
	 * Given a ResultSet pointing to a row, retrieve the values of the row and store
	 * them.
	 *
	 * @param rs
	 *            the setted ResultSet
	 * @throws SQLException
	 */
	public void setValues(ResultSet rs) throws SQLException {
		int count = rs.getMetaData().getColumnCount();
		for (int i = 1; i <= count; i++)
			try {
				super.set(i, rs.getString(i));
			} catch (IndexOutOfBoundsException e) {
				super.add(rs.getString(i));
			}
	}

	/**
	 * Override super.size(). When metadata are setted retunr the size corresponding
	 * to the metadata, otherwise return the normal super.size();
	 *
	 * @return seize coherent with the internal status of the object
	 */
	@Override
	public int size() {
		if (size > -1)
			return size;
		else
			return super.size();
	}

}

package it.unipd.dei.fml;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Represent a city within its province within its nation.
 */
public class Place {
	private static final String query = "SELECT Count(*)  FROM fml.city"
			+ " INNER JOIN fml.province ON (city.provname = province.provname) "
			+ " INNER JOIN fml.nation ON (province.name = nation.name)"
			+ " WHERE city.name = ? AND WHERE province.provname = ? AND WHERE nation.name = ?;";
	private String city;
	private String province;
	private String nation;

	/**
	 * Construcor
	 */
	public Place() {

	}

	/**
	 * Construct that set the parameters
	 *
	 * @param city
	 * @param province
	 * @param nation
	 */
	public Place(String city, String province, String nation) {
		this.city = city;
		this.nation = nation;
		this.province = province;
	}

	/**
	 * Check if a place is an italian city
	 *
	 * @param con
	 *            connection to the database
	 * @return true if italian false otherwise
	 */
	public boolean checkIfItalian(Connection con) {
		boolean find = false;
		try {
			java.sql.PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, city);
			stmt.setString(2, province);
			stmt.setString(3, nation);
			java.sql.ResultSet rs = stmt.executeQuery();
			if (rs.next())
				find = (rs.getInt(1) == 1) && nation.equals("Italia");
		} catch (SQLException e) {
			return false;
		}

		return find;
	}

	/**
	 * @return the city name
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the nation name
	 */
	public String getNation() {
		return nation;
	}

	/**
	 * @return the province name
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Set the city name
	 *
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Set the nation name
	 *
	 * @param nation
	 */
	public void setNation(String nation) {
		this.nation = nation;
	}

	/**
	 * Set the province name
	 *
	 * @param province
	 */
	public void setProvince(String province) {
		this.province = province;
	}
}

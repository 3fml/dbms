package it.unipd.dei.fml;

public class PanelWithPatient extends BackAndHomePanel {

	private PatientRow patient;

	public PanelWithPatient(MainWindowHDC parent, PatientRow patient) {
		super(parent);
		this.patient = patient;
	}

	public PatientRow getPatient() {
		return patient;
	}

	public void setPatient(PatientRow patient) {
		this.patient = patient;
	}

}

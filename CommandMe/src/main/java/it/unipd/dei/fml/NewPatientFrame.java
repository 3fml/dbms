package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class NewPatientFrame extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_9;
	private JTextField textField_3;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_17;
	private JSpinner dataSpinner;
	private int modifiedValues;
	/**
	 * @wbp.nonvisual location=-93,109
	 */
	private final JScrollPane scrollPane = new JScrollPane();
	private JFormattedTextField formattedTextField_1;
	private JFormattedTextField formattedTextField;
	private JTextField textField_15;
	private JTextField textField_16;
	private JComboBox comboBox;
	private NewPatientPanel myPanel;

	private ArrayList<JTextField> allTextField = new ArrayList<>(0);
	private boolean first = true;
	private JTextField textField_18;

	/**
	 * Create the panel.
	 */
	public NewPatientFrame(NewPatientPanel p) {
		myPanel = p;
		setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:min:grow"), }));

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setSize(new Dimension(10, 10));
		scrollPane_1.setBorder(null);
		add(scrollPane_1, "2, 2, fill, fill");

		JPanel panel = new JPanel();
		scrollPane_1.setViewportView(panel);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Dati Anagrafici",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("167px:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("61px:grow"),
				FormSpecs.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("15px:grow"), FormSpecs.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("76px:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { RowSpec.decode("7px"), RowSpec.decode("14px"), RowSpec.decode("20px"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("15px"), RowSpec.decode("20px"),
						FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("14px"), FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC,
						RowSpec.decode("20px"), FormSpecs.UNRELATED_GAP_ROWSPEC, RowSpec.decode("14px"),
						FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC, RowSpec.decode("20px"), FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:114px"),
						FormSpecs.RELATED_GAP_ROWSPEC, RowSpec.decode("fill:118px"), FormSpecs.UNRELATED_GAP_ROWSPEC,
						RowSpec.decode("fill:default"), FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC,
						RowSpec.decode("20px"), }));

		JLabel label = new JLabel("Nome:");
		panel.add(label, "4, 2, 5, 1, fill, top");

		textField = new JTextField();
		allTextField.add(textField);
		textField.setColumns(10);
		panel.add(textField, "4, 3, 5, 1, fill, top");

		JLabel label_1 = new JLabel("Cognome:");
		panel.add(label_1, "2, 2, fill, top");

		textField_1 = new JTextField();
		allTextField.add(textField_1);
		textField_1.setColumns(10);
		panel.add(textField_1, "2, 3, fill, top");

		JLabel label_2 = new JLabel("Codice Fiscale:");
		panel.add(label_2, "2, 5, 3, 1, fill, bottom");

		textField_2 = new JTextField();
		allTextField.add(textField_2);
		textField_2.setColumns(10);
		panel.add(textField_2, "2, 6, 3, 1, fill, top");

		JLabel label_3 = new JLabel("Genere:");
		panel.add(label_3, "6, 5, 3, 1, fill, top");

		JLabel lblLuogoDiNascita = new JLabel("Luogo di Nascita:");
		panel.add(lblLuogoDiNascita, "2, 8, fill, bottom");

		JLabel lblDataDiNascita = new JLabel("Data di Nascita:");
		panel.add(lblDataDiNascita, "4, 8");

		JLabel lblNazionalit = new JLabel("Nazionalità:");
		panel.add(lblNazionalit, "6, 8, 3, 1");

		textField_9 = new JTextField();
		allTextField.add(textField_9);
		textField_9.setColumns(10);
		panel.add(textField_9, "2, 10, fill, top");

		dataSpinner = new JSpinner();
		panel.add(dataSpinner, "4, 10, fill, default");
		dataSpinner.setModel(new SpinnerDateModel(new Date(-3599803L), null, null, Calendar.DAY_OF_MONTH));
		dataSpinner.setEditor(new JSpinner.DateEditor(dataSpinner, "dd/MM/yyyy"));

		textField_18 = new JTextField();
		allTextField.add(textField_18);
		textField_18.setColumns(10);
		panel.add(textField_18, "6, 10, 3, 1, fill, default");

		textField_4 = new JTextField();
		allTextField.add(textField_4);
		textField_4.setColumns(10);
		panel.add(textField_4, "2, 14, 7, 1, fill, top");

		JLabel label_7 = new JLabel("Email:");
		panel.add(label_7, "2, 12, 3, 1, fill, top");

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Femmina", "Maschio" }));
		comboBox.setSelectedIndex(-1);

		panel.add(comboBox, "6, 6, 3, 1, fill, top");

		JLabel label_17 = new JLabel("Telefono:");
		panel.add(label_17, "2, 16");

		JLabel lblCellulare = new JLabel("Cellulare:");
		panel.add(lblCellulare, "4, 16, 5, 1");

		NumberFormat integerFieldFormatter = NumberFormat.getNumberInstance();
		integerFieldFormatter.setMaximumFractionDigits(0);
		integerFieldFormatter.setGroupingUsed(false);

		formattedTextField_1 = new JFormattedTextField(integerFieldFormatter);
		panel.add(formattedTextField_1, "2, 18, fill, default");

		formattedTextField = new JFormattedTextField(integerFieldFormatter);
		panel.add(formattedTextField, "4, 18, 5, 1, fill, default");

		JLabel lblStatoCivile = new JLabel("Stato Civile:");
		panel.add(lblStatoCivile, "2, 20");

		JLabel lblLavoro = new JLabel("Lavoro:");
		panel.add(lblLavoro, "4, 20, 5, 1");

		textField_16 = new JTextField();
		allTextField.add(textField_16);
		textField_16.setColumns(10);
		panel.add(textField_16, "2, 22, fill, default");

		textField_15 = new JTextField();
		allTextField.add(textField_15);
		textField_15.setColumns(10);
		panel.add(textField_15, "4, 22, 5, 1, fill, default");

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Residenza:",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_1, "2, 24, 7, 2, fill, fill");
		panel_1.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));

		JLabel label_6 = new JLabel("Indirizzo:");
		panel_1.add(label_6, "2, 2");

		JLabel label_8 = new JLabel("N. Civico:");
		panel_1.add(label_8, "6, 2");

		textField_3 = new JTextField();
		// Not Mandatory allTextField.add(textField_3);
		panel_1.add(textField_3, "2, 4, 3, 1");
		textField_3.setColumns(10);

		textField_5 = new JTextField();
		// Not Mandatory allTextField.add(textField_5);
		panel_1.add(textField_5, "6, 4");
		textField_5.setColumns(10);

		JLabel label_11 = new JLabel("Città:");
		panel_1.add(label_11, "2, 6");

		JLabel label_10 = new JLabel("CAP:");
		panel_1.add(label_10, "4, 6");

		JLabel label_9 = new JLabel("Provincia:");
		panel_1.add(label_9, "6, 6");

		textField_8 = new JTextField();
		// Not Mandatory allTextField.add(textField_8);
		panel_1.add(textField_8, "2, 8");
		textField_8.setColumns(10);

		textField_7 = new JTextField();
		// Not Mandatory allTextField.add(textField_7);
		panel_1.add(textField_7, "4, 8");
		textField_7.setColumns(10);

		textField_6 = new JTextField();
		// Not Mandatory allTextField.add(textField_6);
		panel_1.add(textField_6, "6, 8");
		textField_6.setColumns(10);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Domicilio:",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_2, "2, 26, 7, 1, fill, fill");
		panel_2.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), FormSpecs.UNRELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.LABEL_COMPONENT_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));

		JLabel label_12 = new JLabel("Indirizzo:");
		panel_2.add(label_12, "2, 2");

		JLabel label_16 = new JLabel("N. Civico:");
		panel_2.add(label_16, "6, 2");

		textField_10 = new JTextField();
		// Not Mandatory allTextField.add(textField_10);
		textField_10.setColumns(10);
		panel_2.add(textField_10, "2, 4, 3, 1, fill, default");

		textField_14 = new JTextField();
		// Not Mandatory allTextField.add(textField_14);
		textField_14.setColumns(10);
		panel_2.add(textField_14, "6, 4, fill, default");

		JLabel label_13 = new JLabel("Città:");
		panel_2.add(label_13, "2, 6");

		JLabel label_14 = new JLabel("CAP:");
		panel_2.add(label_14, "4, 6");

		JLabel label_15 = new JLabel("Provincia:");
		panel_2.add(label_15, "6, 6");

		textField_11 = new JTextField();
		// Not Mandatory allTextField.add(textField_11);
		textField_11.setColumns(10);
		panel_2.add(textField_11, "2, 8, fill, default");

		textField_12 = new JTextField();
		// Not Mandatory allTextField.add(textField_12);
		textField_12.setColumns(10);
		panel_2.add(textField_12, "4, 8, fill, default");

		textField_13 = new JTextField();
		// Not Mandatory allTextField.add(textField_13);
		textField_13.setColumns(10);
		panel_2.add(textField_13, "6, 8, fill, default");

		JButton btnAggiungiParente = new JButton("Aggiungi Parente");
		btnAggiungiParente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btnAggiungiParente, "2, 28, fill, top");

		for (int i = 0; i < allTextField.size(); i++) {
			final JTextField tf = allTextField.get(i);
			tf.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					if (tf.getText().length() > 0)
						modifiedValues--;
				}

				@Override
				public void focusLost(FocusEvent e) {
					if (tf.getText().length() > 0)
						modifiedValues++;
					myPanel.activateSalva((modifiedValues == allTextField.size() + 1));

				}
			});

			tf.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (tf.getText().length() > 0)
						myPanel.activateSalva((modifiedValues == allTextField.size()));
				}
			});
		}

		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (first) {
					modifiedValues++;
					first = false;
				}
				myPanel.activateSalva((modifiedValues == allTextField.size() + 1));
				System.out.println(modifiedValues);
			}
		});

	}

	public void salva() throws Exception {
		try {
			// create visit
			Patient p = new Patient(MainWindowHDC.getIstance().getConnection());
			PatientRow prow = new PatientRow();
			prow.setFiscalcode(textField_2.getText());
			prow.setName(textField.getText());
			prow.setSurname(textField_1.getText());
			prow.setGender(((String) comboBox.getSelectedItem()).equals("Femmina") ? "f" : "m");
			prow.setEmail(textField_4.getText());
			prow.setCellphone(formattedTextField.getText());
			prow.setTelephone(formattedTextField_1.getText());
			prow.setDob(new java.sql.Date(((Date) dataSpinner.getValue()).getTime()));
			prow.setNationality(textField_18.getText());
			prow.setUserstatus("Vivo");
			p.createPatient(prow, MainWindowHDC.getIstance().getCurrentDoctor().getFiscalcode());

		} catch (SQLException | SQLInjectionException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
			throw e;
		}

	}
}

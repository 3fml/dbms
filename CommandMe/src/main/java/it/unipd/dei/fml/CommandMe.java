package it.unipd.dei.fml;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

// java -cp "e:\Università\DBMS\commandme\.output\;e:\Università\DBMS\libs\postgresql-42.2.2.jar" dbms.fml.commandme localhost:5533/dbms1718
// javac -d "e:\Università\DBMS\commandme\dbms\fml" -cp "e:\Università\DBMS\libs\postgresql-42.2.2.jar" "E:\Università\DBMS\commandme\dbms\fml\commandme.java" "E:\Università\DBMS\commandme\dbms\fml\Patient.java" "E:\Università\DBMS\commandme\dbms\fml\Entity.java"
public class CommandMe {

	/**
	 * Main, it's main purpose is to let us testing the classes develop in orde to
	 * manage the fml schema with jdbc. It accepts different command: - generate:
	 * generete a new row in a new table - exit: terminate this instance - query:
	 * Execute a query statement inserted by hand (risk of sql injection, it's only
	 * for us for testing purposes) - update: Execute a update statement inserted by
	 * hand (risk of sql injection, it's only for us for testing purposes) - common:
	 * common query tested for a specific fiscal code - test: Executes a serie of
	 * operation that create, search, delete rows in the tables of the schema, as
	 * the name suggest, it's purpose it to test if the develop classes functions as
	 * intended.
	 *
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IOException {

		String server = "";
		String port = "";
		String dbname = "";
		String user = "";
		String pswd = "";
		String url = "";

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.printf("Driver not found: %s%n", e.getMessage());
			System.exit(-1);
		}

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		long start;
		long end;
		try {

			String command = "";
			while (!command.equalsIgnoreCase("exit")) {

				if (command.equals("generate")) {
					System.out.println("The following operation will lead to generate random entries in the database.");
					System.out.println("Whitch entity do you want to create? ");
					System.out.printf(">_ ");
					Entity p = new Entity(con, br.readLine());
					p.newEntityBuilder();
				} else if (command.equals("help")) {
					System.out.println();
					System.out.println("Commands:");
					System.out.println();
					System.out.println("- generate: generete a new row in a new table");
					System.out.println();
					System.out.println("- connect: create the connection with the database");
					System.out.println();
					System.out.println("- exit: terminate this instance");
					System.out.println();
					System.out.println("- query: Execute a query statement inserted by hand (risk of sql injection,");
					System.out.println("  it's only for us for testing purposes)");
					System.out.println();
					System.out.println("- update: Execute a update statement inserted by hand (risk of sql injection,");
					System.out.println("  it's only for us for testing purposes)");
					System.out.println();
					System.out.println("- common: common query tested for a specific fiscal code ");
					System.out.println();
					System.out.println("- gui: start the graphic interface ");
					System.out.println();
					System.out.println("- test: Executes a serie of operation that create, search, delete rows in the");
					System.out.println(
							"  tables of the schema, as the name suggest, it's purpose it to test if the develop");
					System.out.println("  classes functions as intended.");
					System.out.println();

				} else if (command.equals("connect")) {
					if (args.length == 0) {
						System.out.println("Print \"exit\" everywhere when you want to exit. ");
						System.out.println("Insert server name: ");
						server = br.readLine();
						if (server.equals("exit"))
							System.exit(-1);
						System.out.println("Insert port number: ");

						port = br.readLine();
						if (port.equals("exit"))
							System.exit(-1);
						System.out.println("Insert database name: ");
						dbname = br.readLine();
						if (dbname.equals("exit"))
							System.exit(-1);
						System.out.println("Insert username: ");
						user = br.readLine();
						if (user.equals("exit"))
							System.exit(-1);
						System.out.println("Insert password: ");
						pswd = br.readLine();
						if (pswd.equals("exit"))
							System.exit(-1);
						url = "jdbc:postgresql://" + server + ":" + port + "/" + dbname;
					} else if (args.length == 1) {
						System.out.println("Insert username: ");
						user = br.readLine();
						if (user == "exit")
							System.exit(-1);
						System.out.println("Insert password: ");
						pswd = br.readLine();
						if (pswd == "exit")
							System.exit(-1);
						url = "jdbc:postgresql://" + args[0];
					} else if (args.length == 3) {
						if (pswd == "exit")
							System.exit(-1);
						url = "jdbc:postgresql://" + args[0];
						user = args[1];
						pswd = args[2];
					} else
						System.exit(-1);
					Properties props = new Properties();

					props.setProperty("user", user);
					props.setProperty("password", pswd);
					props.setProperty("stringtype", "unspecified");
					/*
					 * stringtype = String Specify the type to use when binding PreparedStatement
					 * parameters set via setString(). If stringtype is set to varchar (the
					 * default), such parameters will be sent to the server as varchar parameters.
					 * If stringtype is set to unspecified, parameters will be sent to the server as
					 * untyped values, and the server will attempt to infer an appropriate type.
					 */

					con = DriverManager.getConnection(url, props);

				} else if (command.equals("query")) {
					// Only for testing, very risky operation! (will be deleted)
					System.out.println("Insert sql select-code here: ");
					System.out.printf(">_ ");
					String commandString = br.readLine();
					stmt = con.createStatement();
					rs = stmt.executeQuery(commandString);
					while (rs.next()) {
						System.out.printf("| ");
						for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++)
							System.out.printf(rs.getString(i) + " | ");
						System.out.println();
					}
				} else if (command.equals("project"))
					EventQueue.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								new LoginWindowHDC();

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				else if (command.equals("update")) {
					// Only for testing, very risky operation! (will be deleted)
					System.out.println("Insert sql update-code here: ");
					System.out.printf(">_ ");
					String commandString = br.readLine();
					stmt = con.createStatement();
					int rowCount = stmt.executeUpdate(commandString);
					System.out.println("Modified " + rowCount + " rows");
				} else if (command.equals("random")) {
					System.out.println("Insert table name: ");
					System.out.printf(">_ ");
					Entity e = new Entity(con, br.readLine());
					ArrayList<String> ids = e.getRandomRow();
					for (int i = 0; i < ids.size(); i += 2) {
						System.out.printf(ids.get(i));
						System.out.println(" --> " + ids.get(i + 1));
					}

				} else if (command.equals("common")) {
					Patient patient = new Patient(con);
					Doctor doctor = new Doctor(con);

					patient.setFilterDate(new java.sql.Date(1), null);

					System.out.println(" ##---Common Queries---## ");
					printQuery(patient.searchPatientByName("Tigli Giac"));
					System.out.println(" ------- ");
					printQuery(patient.searchPrescriptions(("TGLGCM86D11A944K")));
					System.out.println(" ------- ");
					printQuery(patient.searchPathologies(("TGLGCM86D11A944K")));
					System.out.println(" ------- ");
					printQuery(patient.searchVisits(("TGLGCM86D11A944K")));
					System.out.println(" ------- ");
					printQuery(patient.searchAdditionalServices("TGLGCM86D11A944K"));
					System.out.println(" ------- ");
					printQuery(patient.searchExamsResults("TGLGCM86D11A944K"));
					System.out.println(" ------- ");
					printQuery(doctor.listPatients("CNTVCN62T18L840B"));
					System.out.println(" ##---End---## ");

				} else if (command.equals("test"))
					try {
						// create doctor
						Doctor doc = new Doctor(con);
						Row docRow = new Row();
						doc.setRowMetadata(docRow);
						docRow.set("fiscalcode", "1184677");
						docRow.set("name", "Doctor Name");
						docRow.set("surname", "Doc Surname");
						docRow.set("gender", "f");
						docRow.set("telephone", "0");
						docRow.set("cellphone", "0");
						// not setting or setting to null is the same
						docRow.set("email", null);
						docRow.set("vatnumber", "1");
						docRow.set("asl", "2");
						docRow.set("regionalcode", "1");
						docRow.set("orderno", "1");
						doc.newDoctor(docRow);

						// create Patient
						Patient pat = new Patient(con);
						PatientRow patrow = new PatientRow();
						pat.setRowMetadata(patrow);
						patrow.set("fiscalcode", "FiscalCode");
						patrow.set("name", "Name 1");
						patrow.set("surname", "Surname 1");
						patrow.set("gender", "m");
						patrow.set("telephone", "0");
						patrow.set("cellphone", "0");
						patrow.set("email", null);
						patrow.set("userstatus", "Deceduto");
						patrow.set("nationality", "Italiano");
						patrow.set("dob", null);
						patrow.set("ulss", "Patient1 ASL");
						pat.createPatient(patrow, docRow.get("fiscalcode"));
						// register againt to test this case scenario
						pat.registerToDoctor(docRow.get("fiscalcode"), patrow.get("fiscalcode"));
						// try searching patient by name/surname
						ArrayList<PatientRow> patientList = pat.searchPatientByName("Surname NaMe");

						// display results
						System.out.println("Searched: Surname NaMe");
						printQuery(patientList);

						// select the first (and only) patient
						Row currentPat = new Row();
						currentPat = patientList.get(0);
						pat.setRowMetadata(currentPat);

						// create visit
						int visitId = pat.createVisit(currentPat.get("fiscalcode"), "Test visit");

						// create pathology (this will not be done when the database
						// will be populated with all the lists)
						// Entity is very handy in handling this kind of non common operation
						Entity pathology = new Entity(con, "pathology");
						Row pathologyRow = new Row();
						pathology.setRowMetadata(pathologyRow);
						pathologyRow.set("icd9code", "RandomICD9Code");
						pathologyRow.set("icd9codepath", "RandomICD9Code");
						pathologyRow.set("description", "RandomICD9Code");
						pathology.createGenericInstance(pathologyRow);

						// connect the pathology to the visit previously created
						pat.createPathology(visitId, "RandomICD9Code");

						// Create 2 prescription, on with a visit, one with automatic visit creation
						int prescriptionID1 = pat.createPrescription("Elettronica", "Prescription Prova", "Urgente",
								visitId);
						int prescriptionID2 = pat.createPrescription(currentPat.get("fiscalcode"), "Elettronica",
								"Prescription Prova", "Urgente");

						// tries some common query for the patient
						printQuery(pat.searchPrescriptions(patrow.get("fiscalcode")));
						printQuery(pat.searchPathologies(patrow.get("fiscalcode")));
						printQuery(pat.searchVisits(patrow.get("fiscalcode")));

						// Delete what just created

						Entity prescription = new Entity(con, "prescription");

						ArrayList<String> pre1Row = new ArrayList<>();
						pre1Row.add("" + prescriptionID1);

						Row prescriptionRow1 = prescription.getRow(pre1Row);

						ArrayList<String> pre2Row = new ArrayList<>();
						pre2Row.add("" + prescriptionID2);

						Row prescriptionRow2 = prescription.getRow(pre2Row);

						prescription.tryDelete(prescriptionRow1);
						prescription.tryDelete(prescriptionRow2);

						Row ack = new Row();
						Entity ackonw = new Entity(con, "acknowledge");
						ackonw.setRowMetadata(ack);
						ack.set("counterid", "" + visitId);
						ack.set("icd9code", "RandomICD9Code");
						ackonw.tryDelete(ack);

						pathology.tryDelete(pathologyRow);

						Row visitRow = new Row();
						Entity visit = new Entity(con, "visit");
						visit.setRowMetadata(visitRow);

						ArrayList<Row> visitList = pat.searchVisits(currentPat.get("fiscalcode"));
						for (int i = 0; i < visitList.size(); i++) {
							if (Integer.parseInt(visitList.get(i).get("counterid")) == visitId)
								for (int j = 0; j < visitRow.size(); j++) // first lines are all from visit
									visitRow.set(j, visitList.get(i).get(j));
							for (int k = 0; k < visitList.get(i).size(); k++)
								System.out.println(
										visitList.get(i).getColumnNames().get(k) + " " + visitList.get(i).get(k));
							// if I know the columns label i can write them by hand, or I can print them
							// like this
						}
						visit.tryDelete(visitRow);

						ArrayList<String> pk = new ArrayList<>();
						pk.add("" + (visitId + 1));// I already know that it have been done after the first one
													// (otherwise i should seach it)
						visitRow = visit.getRow(pk);
						visit.tryDelete(visitRow);

						Entity register = new Entity(con, "register");
						Row registerRow = new Row();
						register.setRowMetadata(registerRow);

						ArrayList<Row> registerTable = register.getTable();
						for (int i = 0; i < registerTable.size(); i++) {
							System.out.println("Register parameters: ");

							for (int j = 0; j < registerTable.get(i).size(); j++)
								System.out.println(registerTable.get(i).getColumnNames().get(j) + " "
										+ registerTable.get(i).get(j));

							if ((registerTable.get(i).get("fiscalcode")
									.equals(pat.getCurrentDoctor(currentPat.get("fiscalcode")).get("fiscalcode")))
									&& (registerTable.get(i).get("fiscalcode2").equals(currentPat.get("fiscalcode")))) {
								registerRow = registerTable.get(i);

								for (int k = 0; k < registerRow.size(); k++)
									System.out.println(registerRow.get(k));

								if (register.tryDelete(registerRow))
									System.out.println("Eliminata");
							}

							System.out.println("---");
						}
						boolean passed = pat.tryDelete(currentPat);

						docRow = doc.getDoctor(docRow.get("fiscalcode"));

						passed = passed && doc.tryDelete(docRow);
						System.out.printf("Eliminato sia paziente che dottore?: %b ", passed);
						System.out.println("");

						// Let's inform the user if the test is passed or not
						if (passed)
							System.out.println("Test Passed!!");
						else
							System.out.println("Test Failed :(");

					} catch (SQLException e) {
						System.out.println("Test Failed");
						e.printStackTrace();
						System.out.printf("Database access error: %n");
						while (e != null) {
							System.out.printf("- Message: %s%n", e.getMessage());
							System.out.printf("- SQL status code: %s%n", e.getSQLState());
							System.out.printf("- SQL error code: %s%n", e.getErrorCode());
							System.out.printf("%n");
							e = e.getNextException();
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("Test Failed");
						System.out.printf("No directly database related error: %n");
						System.out.printf("- Message: %s%n", e.getMessage());
					}

				System.out.printf(">_ ");
				command = br.readLine();
			}

		} catch (SQLException e) {
			System.out.printf("Database access error: %n");
			e.printStackTrace();
			while (e != null) {
				System.out.printf("- Message: %s%n", e.getMessage());
				System.out.printf("- SQL status code: %s%n", e.getSQLState());
				System.out.printf("- SQL error code: %s%n", e.getErrorCode());
				System.out.printf("%n");
				e = e.getNextException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.printf("No directly database related error: %n");
			System.out.printf("- Message: %s%n", e.getMessage());
		} finally {
			try {
				if (rs != null) {
					start = System.currentTimeMillis();
					rs.close();
					end = System.currentTimeMillis();
					System.out.printf("Result set successfully closed in %,d milliseconds.%n", end - start);
				}
				if (stmt != null) {
					start = System.currentTimeMillis();
					stmt.close();
					end = System.currentTimeMillis();
					System.out.printf("Statement successfully closed in %,d milliseconds.%n", end - start);
				}
				if (con != null) {
					start = System.currentTimeMillis();
					con.close();
					end = System.currentTimeMillis();
					System.out.printf("Connection successfully closed in %,d milliseconds.%n", end - start);
				}
				System.out.printf("Resources successfully released.%n");
			} catch (SQLException e) {
				System.out.printf("Error while releasing resources:%n");
				e.printStackTrace();
				while (e != null) {
					System.out.printf(" - Message: %s%n", e.getMessage());
					System.out.printf(" - SQL status code: %s%n", e.getSQLState());
					System.out.printf(" - SQL error code: %s%n", e.getErrorCode());
					System.out.printf("%n");
					e = e.getNextException();
				}
			} finally {
				rs = null;
				stmt = null;
				con = null;
				System.out.printf("Resources released to the garbage collector.%n");
			}
		}
	}

	/**
	 * Print a query to the standard output, without any particular indentation.
	 *
	 * @param rowList
	 */
	private static void printQuery(ArrayList rowList) {
		for (int i = 0; i < rowList.size(); i++) {
			Row currentRow = (Row) rowList.get(i);
			for (int j = 0; j < currentRow.size(); j++)
				System.out.println(currentRow.getColumnNames().get(j) + ": " + currentRow.get(j));
			System.out.println("----");
		}
		System.out.println("/End query");
	}

}

package it.unipd.dei.fml;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class PatientSearchPanel extends BackAndHomePanel {

	private PatientPanel patientPanel = null;

	private MainWindowHDC parent;

	private PatientSearchFrame myFrame;

	private PatientRow selectedPatient;
	private JButton btnSelezionaPaziente;

	/**
	 * Pannello con le azioni mostrate all'avvio della finestra principale.
	 * 
	 */
	public PatientSearchPanel(final MainWindowHDC parent) {
		super(parent);
		myFrame = new PatientSearchFrame(parent, this);
		super.setFrame(myFrame);

		JLabel lblNewLabel = new JLabel("Menu Paziente:");
		lblNewLabel.setVerticalTextPosition(SwingConstants.TOP);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		this.add(lblNewLabel, "2, 1");

		JButton btnSetDottore = new JButton("Imposta Dottore");
		btnSetDottore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

		JButton btnNuovaRicerca = new JButton("Nuova Ricerca");
		btnNuovaRicerca.setEnabled(false);
		btnNuovaRicerca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

		btnSelezionaPaziente = new JButton("Seleziona Paziente");
		btnSelezionaPaziente.setEnabled(false);
		btnSelezionaPaziente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.setView(patientPanel = new PatientPanel(parent, selectedPatient));
			}
		});
		this.add(btnSelezionaPaziente, "2, 2, fill, fill");
		this.add(btnNuovaRicerca, "2, 4, fill, fill");
		this.add(btnSetDottore, "2, 6, fill, fill");
	}

	@Override
	public void clean() {
		// System.out.println(this.toString().substring(0, 30)+"\n");
		if (patientPanel != null) {
			patientPanel.clean();
			patientPanel = null;
		}
	}

	public void setCurrentPatient(PatientRow patient) {

	}

	// Se clicco una volta nella tabella attivo i bottoni ma non faccio nulla
	public void setSelectedPatient(PatientRow patient) {
		selectedPatient = patient;
		if (patient != null)
			btnSelezionaPaziente.setEnabled(true);
	}

	// Con il doppio click o il click sul bottone allora passo alla finestra
	// successiva
	/*
	 * public void patientSelected(PatientRow selectPatient) {
	 * parent.setView(patientPanel = new PatientPanel(parent, selectPatient)); }
	 */
}

package it.unipd.dei.fml;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 */
public class DoctorRow extends Row {

	private String fiscalcode;
	private String vatnumber;
	private String asl;
	private String district;
	private String regionalcode;
	private String orderno;
	private String name;
	private String surname;
	private String gender;
	private String telephone;
	private String cellphone;
	private String email;
	private int patientno;
	private String username;

	public DoctorRow() {
		super(11);
		ArrayList<String> cname = new ArrayList<>(11);
		cname.add("fiscalcode");
		cname.add("vatnumber");
		cname.add("asl");
		cname.add("district");
		cname.add("regionalcode");
		cname.add("orderno");
		cname.add("name");
		cname.add("surname");
		cname.add("gender");
		cname.add("telephone");
		cname.add("cellphone");
		cname.add("email");
		cname.add("patientno");
		try {
			super.setColumnNames(cname);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructor that retrieve the metadata and the values from the current row
	 * pointe by a ResultSet initialize and setted correctly (ResultSet.next() has
	 * to be executed at least once before running this constructor)
	 *
	 * @param rs
	 *            ResultSet object setted correctly
	 * @throws SQLException
	 */
	public DoctorRow(ResultSet rs) throws SQLException {
		super(rs);
		fiscalcode = get("fiscalcode");
		vatnumber = get("vatnumber");
		asl = get("asl");
		district = get("district");
		regionalcode = get("regionalcode");
		orderno = get("orderno");
		name = get("name");
		surname = get("surname");
		gender = get("gender");
		telephone = get("telephone");
		cellphone = get("cellphone");
		email = get("email");
		patientno = Integer.parseInt(get("patientno"));

	}

	public DoctorRow(Row r) {
		super(r);
		fiscalcode = r.get("fiscalcode");
		vatnumber = r.get("vatnumber");
		asl = r.get("asl");
		district = r.get("district");
		regionalcode = r.get("regionalcode");
		orderno = r.get("orderno");
		name = r.get("name");
		surname = r.get("surname");
		gender = r.get("gender");
		telephone = r.get("telephone");
		cellphone = r.get("cellphone");
		email = r.get("email");
		patientno = Integer.parseInt(r.get("patientno"));
	}

	@Override
	public DoctorRow clone() {
		return new DoctorRow(this);
	}

	public String getAsl() {
		return asl;
	}

	public String getCellphone() {
		return cellphone;
	}

	public String getDistrict() {
		return district;
	}

	public String getEmail() {
		return email;
	}

	public String getFiscalcode() {
		return fiscalcode;
	}

	public String getGender() {
		return gender;
	}

	public String getName() {
		return name;
	}

	public String getOrderno() {
		return orderno;
	}

	public int getPatientno() {
		return patientno;
	}

	public String getRegionalcode() {
		return regionalcode;
	}

	public String getSurname() {
		return surname;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getUsername() {
		return username;
	}

	public String getVatnumber() {
		return vatnumber;
	}

	/**
	 * Set the valued at the position corresponding with the column name equals to
	 * columnLabel to the value "value"
	 *
	 * @param columnLabel
	 *            Column name of the value
	 * @param value
	 *            value to be setted
	 * @throws IllegalArgumentException
	 *             if a column that does not belong to Doctor is inserted
	 */
	@Override
	public void set(String columnLabel, String value) throws IllegalArgumentException {
		switch (columnLabel) {
		case "fiscalcode":
			fiscalcode = value;
			break;
		case "vatnumber":
			vatnumber = value;
			break;
		case "asl":
			asl = value;
			break;
		case "district":
			district = value;
			break;
		case "regionalcode":
			regionalcode = value;
			break;
		case "orderno":
			orderno = value;
			break;
		case "name":
			name = value;
			break;
		case "surname":
			surname = value;
			break;
		case "gender":
			gender = value;
			break;
		case "telephone":
			telephone = value;
			break;
		case "cellphone":
			cellphone = value;
			break;
		case "email":
			email = value;
			break;
		case "patientno":
			patientno = Integer.parseInt(value);
			break;
		default:
			throw new IllegalArgumentException("Column does not belong to Doctor");
		}
		super.set(columnLabel, value);
	}

	public void setAsl(String asl) {
		set("asl", asl);
	}

	public void setCellphone(String cellphone) {
		set("cellphone", cellphone);
	}

	public void setDistrict(String district) {
		set("district", district);
	}

	public void setEmail(String email) {
		set("email", email);
	}

	public void setFiscalcode(String fiscalcode) {
		set("fiscalcode", fiscalcode);
	}

	public void setGender(String gender) {
		set("gender", gender);
	}

	public void setName(String name) {
		set("name", name);
	}

	public void setOrderno(String orderno) {
		set("orderno", orderno);
	}

	public void setPatientno(int patientno) {
		this.patientno = patientno;
	}

	public void setRegionalcode(String regionalcode) {
		set("regionalcode", regionalcode);
	}

	public void setSurname(String surname) {
		set("surname", surname);
	}

	public void setTelephone(String telephone) {
		set("telephone", telephone);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setVatnumber(String vatnumber) {
		set("vatnumber", vatnumber);
	}

}
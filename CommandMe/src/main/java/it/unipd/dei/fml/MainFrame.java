package it.unipd.dei.fml;

import java.awt.Font;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class MainFrame extends JPanel {

	private JPanel contentPane;
	private MainWindowHDC parent;
	private DoctorRow doctor;
	private JTextArea textArea;
	private JTextArea lblDbInfo;
	private JLabel lblPuoiSelezionareLoperazione;
	private JLabel lblNewLabel;
	private JLabel lblInformazioniDottore;

	/**
	 * Create the frame.
	 */
	public MainFrame(final MainWindowHDC parent) {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				//update doctor information
				try {
					String username = doctor .getUsername();
					Doctor d = new Doctor (parent.getConnection());
					doctor = d.getRow(doctor.getFiscalcode());
					doctor.setUsername(username);
					updateUI();
					
				} catch (SQLException | SQLInjectionException e1) {
					e1.printStackTrace();
				}
			}
				
		});
		this.parent = parent;
		setLayout(null);
		doctor = parent.getCurrentDoctor();

		lblPuoiSelezionareLoperazione = new JLabel(
				"Puoi selezionare l'operazione che intendi svolgere dal Menu laterale.");
		lblPuoiSelezionareLoperazione.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPuoiSelezionareLoperazione.setBounds(10, 11, 430, 14);
		add(lblPuoiSelezionareLoperazione);

		lblNewLabel = new JLabel("Informazioni Database:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 48, 267, 14);
		add(lblNewLabel);

		lblDbInfo = new JTextArea();
		add(lblDbInfo);
		
		lblInformazioniDottore = new JLabel("Informazioni Dottore:");
		lblInformazioniDottore.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInformazioniDottore.setBounds(10, 119, 267, 14);
		add(lblInformazioniDottore);
		
		textArea = new JTextArea();
		add(textArea);
		
		updateUI();

	}
	
	public void updateUI() {
		if (lblDbInfo != null) {
			lblDbInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
			try {
				DatabaseMetaData info;
				info = parent.getConnection().getMetaData();
				lblDbInfo.setText("Nome Database: " + info.getDatabaseProductName() + "\n" + "Versione: "
						+ info.getDatabaseProductVersion() + "\n");
			} catch (SQLException e) {
				lblDbInfo.setText("Nessuna informazione disponibile.");
				e.printStackTrace();
			}
			lblDbInfo.setBounds(20, 62, 355, 46);
			
			textArea.setText("Login effettuato come: " + doctor.getUsername() + "\n"
					+ "Nome: " + doctor.getName() + "\n" 
					+ "Cognome: " + doctor.getSurname() + "\n"
					+ "Cofice Fiscale: " + doctor.getFiscalcode() + "\n"
					+ "Numero cellulare: " + doctor.getCellphone() + "\n"
					+ "Numero telefono: " + doctor.getTelephone() + "\n"
					+ "Partita IVA: " + doctor.getVatnumber() + "\n"
					+ "Numero pazienti: " + doctor.getPatientno());
			textArea.setFont(new Font("Tahoma", Font.PLAIN, 11));
			textArea.setBounds(20, 133, 355, 116);
		}
		
	}
}

package it.unipd.dei.fml;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class PatientSearchFrame extends JPanel {

	private JPanel contentPane;
	private MainWindowHDC parent;
	private PatientSearchPanel panel;
	private JTextField txtInserisciNomeEo;
	private JButton btnNewButton;
	private JScrollPane scrollPane;
	private JTable table;
	private Connection con;
	private PatientRow selectedPatient = null;
	private JLabel lblNewLabel;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")
	public PatientSearchFrame(final MainWindowHDC parent, final PatientSearchPanel panel) {
		this.parent = parent;
		this.panel = panel;
		con = parent.getConnection();

		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("150px"), FormSpecs.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						RowSpec.decode("default:grow"), }));

		Action enterAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.setRowCount(0);
						try {
							Patient p = new Patient(con);// Andrebbe usato Doctor, per cercare solo i pazienti di un
															// dottore, ma per ora cerco tra tutti i pazienti
							ArrayList<PatientRow> result = p.searchPatientByName(txtInserisciNomeEo.getText());
							int i;
							for (i = 0; i < result.size(); i++) {
								PatientRow currentRow = result.get(i);
								model.addRow(new Object[5]);

								if (currentRow.getName() != null)
									model.setValueAt(currentRow.getName(), i, 0);
								else
									model.setValueAt("-", i, 0);

								if (currentRow.getSurname() != null)
									model.setValueAt(currentRow.getSurname(), i, 1);
								else
									model.setValueAt("-", i, 1);
								if (currentRow.getFiscalcode() != null)
									model.setValueAt(currentRow.getFiscalcode(), i, 2);
								else
									model.setValueAt("-", i, 2);

								if (currentRow.getDob() != null) {
									LocalDate date = currentRow.getDob().toLocalDate();
									model.setValueAt(Integer.toString(Period.between(date, LocalDate.now()).getYears()),
											i, 3);
								} else
									model.setValueAt("-", i, 3);

								Row residenza = p.getResindency(currentRow.getFiscalcode());
								if (residenza != null)
									model.setValueAt((residenza.get("street") != null ? residenza.get("street") + ", "
											: "Nessun indirizzo, ") + residenza.get("cityname"), i, 4);
								else
									model.setValueAt("-", i, 4);

							}
							if (i == 0) {
								model.addRow(new Object[1]);
								model.setValueAt("Nessun risultato trovato", 0, 0);
							}
						} catch (SQLException | SQLInjectionException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
							e.printStackTrace();
						}
					}

				});
			}
		};

		txtInserisciNomeEo = new JTextField();
		txtInserisciNomeEo.addActionListener(enterAction);
		txtInserisciNomeEo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtInserisciNomeEo.getForeground() == Color.GRAY) {
					// dato che sono pigro non creo un falg ma uso ciò che già ho
					txtInserisciNomeEo.setText("");
					txtInserisciNomeEo.setForeground(Color.BLACK);
				}
			}
		});

		txtInserisciNomeEo.setForeground(Color.GRAY);
		txtInserisciNomeEo.setText("Inserisci nome e/o cognome");
		this.add(txtInserisciNomeEo, "2, 2, fill, default");
		txtInserisciNomeEo.setColumns(10);

		btnNewButton = new JButton("Cerca");
		btnNewButton.addActionListener(enterAction);

		this.add(btnNewButton, "4, 2");

		scrollPane = new JScrollPane();
		this.add(scrollPane, "2, 4, 3, 1, fill, fill");

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Nome", "Cognome", "Cod. Fiscale", "Et\u00E0", "Residenza" }) {
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class, String.class };
			boolean[] columnEditables = new boolean[] { false, false, false, false, false };

			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent mouseEvent) {
				Point point = mouseEvent.getPoint();
				final int row = table.getSelectedRow();
				try {
					if (mouseEvent.getButton() == MouseEvent.BUTTON1 && table.rowAtPoint(point) != -1)
						if (mouseEvent.getClickCount() == 1)
							panel.setSelectedPatient(selectPatient(row));
					if (mouseEvent.getClickCount() == 2)
						parent.setView(new PatientPanel(parent, selectPatient(row)));
				} catch (SQLException | SQLInjectionException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				}
			}
		});

		TableColumnModel model = table.getColumnModel();
		model.getColumn(0).setPreferredWidth(80);
		model.getColumn(0).setMinWidth(50);
		model.getColumn(0).setMaxWidth(150);

		model.getColumn(1).setPreferredWidth(80);
		model.getColumn(1).setMinWidth(50);
		model.getColumn(1).setMaxWidth(150);

		model.getColumn(2).setPreferredWidth(120);
		model.getColumn(2).setMinWidth(120);
		model.getColumn(2).setMaxWidth(120);

		model.getColumn(3).setPreferredWidth(30);
		model.getColumn(3).setMinWidth(30);
		model.getColumn(3).setMaxWidth(30);

		model.getColumn(4).setPreferredWidth(150);
		model.getColumn(4).setMinWidth(150);
		model.getColumn(4).setMaxWidth(800);

		table.setGridColor(Color.GRAY);
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 11));
		scrollPane.setViewportView(table);

		scrollPane.setColumnHeaderView(lblNewLabel);
	}

	public PatientRow selectPatient(int row) throws SQLException, SQLInjectionException {
		return new Patient(con).getRow(new ArrayList<>(
				Arrays.asList(new String[] { (String) ((DefaultTableModel) table.getModel()).getValueAt(row, 2) })));

	}

}

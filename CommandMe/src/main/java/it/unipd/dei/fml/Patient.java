package it.unipd.dei.fml;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Class that represent the "patient" table, it has some of the most useful
 * funtionality in order to manage patient related operations.
 */
class Patient extends Entity {

	/**
	 * Constructor of a patient Entity Class, it needs:
	 *
	 * @param con
	 *            connection in order to accessing the database
	 * 
	 * @throws SQLException
	 * @throws SQLInjectionException
	 */
	public Patient(Connection con) throws SQLException, SQLInjectionException {
		super(con, "patient");
	}

	/**
	 * Create a new additional services entry for a patient with a given doctor. All
	 * the information are stored in the Row parameter
	 *
	 * @param info
	 *            Contain all the information that need to be inserted
	 * @throws SQLException
	 */
	public void createAddServs(Row info) throws SQLException {
		if (info.get(1) == null)
			info.set(1, getCurrentDoctor(info.get(1)).get(0));
		try {
			Entity temp = new Entity(connection, "additionalservices");
			temp.createGenericInstance(info);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Create a new additional services entry for a patient with a given doctor.
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param addServsID
	 *            id of the additional service type
	 * @throws SQLException
	 */
	public void createAddServs(String fiscalCode, String addServsID) throws SQLException {
		createAddServs(fiscalCode, getCurrentDoctor(fiscalCode).get(0), addServsID);
	}

	/**
	 * Create a new additional services entry for a patient with a given doctor.
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param doctorID
	 *            fiscal code of the doctor
	 * @param addServsID
	 *            id of the additional service type
	 * @throws SQLException
	 */
	public void createAddServs(String fiscalCode, String doctorID, String addServsID) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".execute VALUES (?,?,?, current_date);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, doctorID);
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, addServsID);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Insert new pathology.
	 *
	 * @param visitCounterId
	 *            visit when the path. is aknowledge
	 * @param pathologyCode
	 *            ICD9 code of the pathology
	 * @throws SQLException
	 */
	public String createPathology(int visitCounterId, String pathologyCode) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".acknowledge  (counterid,icd9code) VALUES (?,?);";
		PreparedStatement pstmn = null;
		ResultSet rs = null;
		String result = "";
		try {
			pstmn = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmn.setInt(1, visitCounterId);
			pstmn.setString(2, pathologyCode);
			pstmn.executeUpdate();
			rs = pstmn.getGeneratedKeys();
			if (rs.next())
				result = rs.getString("icd9code");

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
				rs = null;
			}
		}
		return result;
	}

	/*
	 * public ArrayList<Row> searchExamsResults(String fiscalCode, java.sql.Date
	 * startData, java.sql.Date endData)throws SQLException{ String sql
	 * ="SELECT EX.* FROM  "+schema+".examresults AS EX "+
	 * "WHERE ? = EX.fiscalcode AND ? > EX.examdate AND ? < EX.examdate; ";
	 * PreparedStatement st = connection.prepareStatement(sql);
	 * st.setString(1,fiscalCode); st.setDate(2, endData); st.setDate(3, startData);
	 * ResultSet rs = st.executeQuery();
	 * 
	 * ArrayList<Row> result = getMultipleRowsFromQuery(rs);
	 * 
	 * rs.close(); st.close();
	 * 
	 * return result; }
	 * 
	 * public ArrayList<Row> searchExamsResults(String fiscalCode, String startData,
	 * String endData)throws SQLException{ String sql
	 * ="SELECT EX.* FROM  "+schema+".examresults AS EX "+
	 * "WHERE ? = EX.fiscalcode AND ? > EX.examdate AND ? < EX.examdate; ";
	 * PreparedStatement st = connection.prepareStatement(sql);
	 * st.setString(1,fiscalCode); st.setString(2, endData); st.setString(3,
	 * startData); ResultSet rs = st.executeQuery(); ArrayList<Row> result =
	 * getMultipleRowsFromQuery(rs);
	 * 
	 * rs.close(); st.close();
	 * 
	 * return result; }
	 */

	/**
	 * Create a new row in the table "patient" stored in entityName
	 *
	 * @param param
	 *            row with the values that are going to be inserted
	 * @param doctorfiscalCode
	 *            fiscal code of the patient's doctor
	 * @throws SQLException
	 */
	public void createPatient(PatientRow param, String doctorfiscalCode) throws SQLException {
		createGenericInstance(param);
		registerToDoctor(doctorfiscalCode, param.get(0));
	}

	/**
	 * Create a new prescription and insert it in the database. IMPORTANT: this
	 * method let you set also the Primary Key of a prescription row, DO NOT do
	 * that, leave this operation to the Database triggers set that column as
	 * defaults --> true. (Unless you know exactly what you are doing)
	 * 
	 * @param info
	 *            all the information of the prescription
	 * @throws SQLException
	 */
	public void createPrescription(Row info) throws SQLException {
		try {
			Entity temp = new Entity(connection, "prescription");
			temp.createGenericInstance(info);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Create a new prescription and insert it in the schema
	 *
	 * @param color
	 *            color of the prescription
	 * @param notes
	 *            notes of the prescription written by the doctor
	 * @param priority
	 *            priority of the doctor
	 * @param visitcounterid
	 *            Id of the visit when the prescription is made
	 * @throws SQLException
	 */
	public int createPrescription(String color, String notes, String priority, int visitcounterid) throws SQLException {
		PreparedStatement pstmn = null;
		ResultSet rs = null;
		int result = -1;
		try {
			String SQL = "INSERT INTO " + SCHEMA_NAME + ".prescription VALUES (DEFAULT,?,?,?,?);";
			pstmn = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmn.setString(1, color);
			pstmn.setString(2, notes);
			pstmn.setString(3, priority);
			pstmn.setInt(4, visitcounterid);
			pstmn.executeUpdate();
			rs = pstmn.getGeneratedKeys();
			if (rs.next())
				result = rs.getInt(1);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
				rs = null;
			}
		}
		return result;
	}

	/**
	 * Create a new prescription and insert it in the schema, it automatically
	 * create also a visit
	 *
	 * @param color
	 *            color of the prescription
	 * @param notes
	 *            notes of the prescription written by the doctor
	 * @param priority
	 *            priority of the doctor
	 * @param visitcounterid
	 *            Id of the visit when the prescription is made
	 * @throws SQLException
	 */
	public int createPrescription(String fiscalCode, String color, String notes, String priority) throws SQLException {
		PreparedStatement pstmn = null;
		ResultSet rs = null;
		int result = -1;
		try {
			int visitcounterid = createVisit(fiscalCode, "Prescription: " + notes);
			String SQL = "INSERT INTO " + SCHEMA_NAME + ".prescription VALUES (DEFAULT,?,?,?,?);";
			pstmn = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmn.setString(1, color);
			pstmn.setString(2, notes);
			pstmn.setString(3, priority);
			pstmn.setInt(4, visitcounterid);
			pstmn.executeUpdate();
			rs = pstmn.getGeneratedKeys();
			if (rs.next())
				result = rs.getInt(1);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
				rs = null;
			}
		}
		return result;
	}

	/**
	 * Create a new visit and insert it in the database. IMPORTANT: this method let
	 * you set also the Primary Key of a visit row, DO NOT do that, leave this
	 * operation to the Database triggers set that column as defaults --> true.
	 * (Unless you know exactly what you are doing)
	 *
	 * @param info
	 *            all the information of the visit
	 * @throws SQLException
	 */
	public void createVisit(Row info) throws SQLException {
		if (info.get(5) == null)
			info.set(5, getCurrentDoctor(info.get(1)).get(0));
		try {
			Entity temp = new Entity(connection, "visit");
			temp.createGenericInstance(info);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Create a new visit and insert it in the schema. Note that this method will
	 * automatically set the current patient doctor as prescribing doctor
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param notes
	 *            notes of the visit written by the doctor
	 * @throws SQLException
	 */
	public int createVisit(String fiscalCode, String notes) throws SQLException {
		String doctorID = getCurrentDoctor(fiscalCode).get(0);
		return createVisit(fiscalCode, doctorID, notes);
	}

	/**
	 * Create a new visit and insert it in the schema.
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param doctorId
	 *            fiscal code of the doctor that makes the visit
	 * @param notes
	 *            notes of the visit written by the doctor
	 * @throws SQLException
	 */
	public int createVisit(String fiscalCode, String doctorId, String notes) throws SQLException {

		String SQL = "INSERT INTO " + SCHEMA_NAME + ".visit VALUES (DEFAULT,?, DEFAULT,?,?);";
		PreparedStatement pstmn = null;
		ResultSet rs = null;
		int result = -1;
		try {
			pstmn = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmn.setString(1, notes);
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, doctorId);
			pstmn.executeUpdate();
			rs = pstmn.getGeneratedKeys();
			if (rs.next())
				result = rs.getInt(1);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
				rs = null;
			}
		}
		return result;
	}

	/**
	 * Create a new visit and insert it in the schema.
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param doctorId
	 *            fiscal code of the doctor that makes the visit
	 * @param notes
	 *            notes of the visit written by the doctor
	 * @throws SQLException
	 */
	public int createVisit(String fiscalCode, String doctorId, String notes, Timestamp timestamp) throws SQLException {

		String SQL = "INSERT INTO " + SCHEMA_NAME + ".visit VALUES (DEFAULT,?, ?,?,?);";
		PreparedStatement pstmn = null;
		ResultSet rs = null;
		int result = -1;
		try {
			pstmn = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmn.setString(1, notes);
			pstmn.setTimestamp(2, timestamp);
			pstmn.setString(3, fiscalCode);
			pstmn.setString(4, doctorId);
			pstmn.executeUpdate();
			rs = pstmn.getGeneratedKeys();
			if (rs.next())
				result = rs.getInt(1);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
				rs = null;
			}
		}
		return result;
	}

	/**
	 * Create a new visit and insert it in the schema. Note that this method will
	 * automatically set the current patient doctor as prescribing doctor
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param notes
	 *            notes of the visit written by the doctor
	 * @param timestamp
	 *            istant of the visit
	 * @throws SQLException
	 */
	public int createVisit(String fiscalCode, String notes, Timestamp timestamp) throws SQLException {
		String doctorID = getCurrentDoctor(fiscalCode).get(0);
		return createVisit(fiscalCode, doctorID, notes, timestamp);
	}

	/**
	 * Return the row with the data of the current doctor of a patient. Current
	 * doctor is the one withoud a end date in the register table the link the two
	 * people
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @return the doctor data
	 * @throws SQLException
	 */
	public Row getCurrentDoctor(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			String sql = "SELECT D.* FROM  " + SCHEMA_NAME + ".register AS Reg " + "NATURAL JOIN " + SCHEMA_NAME
					+ ".doctor AS D " + "WHERE ? = Reg.fiscalcode2 AND reg.enddate IS NULL;";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Return the domicile of a patient
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return row with: - city - province - nation - fiscal code of the patient -
	 *         "Domicilio" - street - civic number - zip code
	 * @throws SQLException
	 */
	public Row getDomicile(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			String sql = "SELECT cityname, provname, name, L.* FROM  " + SCHEMA_NAME + ".patientlive AS L "
					+ "NATURAL JOIN " + SCHEMA_NAME + ".city AS C  " + "NATURAL JOIN " + SCHEMA_NAME
					+ ".province AS P  " + "NATURAL JOIN " + SCHEMA_NAME + ".nation AS N  "
					+ "WHERE ? = L.fiscalcode AND L.residencydomicile = 'Domicilio';";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Get the patient information corresponding to the fiscal code passed as
	 * parameter
	 *
	 * @param fiscalCode
	 *            the Primary Key of the patient
	 * @throws SQLException
	 */
	public Row getPatient(String fiscalCode) throws SQLException {
		ArrayList<String> wrapper = new ArrayList<>();
		wrapper.add(fiscalCode);
		return getRow(wrapper);
	}

	/**
	 * Return the place of birth of a patient
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return row with city, province and nation
	 * @throws SQLException
	 */
	public Row getPlaceOfBirth(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			String sql = "SELECT cityname, provname, name FROM  " + SCHEMA_NAME + ".born AS B " + "NATURAL JOIN "
					+ SCHEMA_NAME + ".city AS C  " + "NATURAL JOIN " + SCHEMA_NAME + ".province AS P  "
					+ "NATURAL JOIN " + SCHEMA_NAME + ".nation AS N  " + "WHERE ? = B.fiscalcode; ";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Return the residency of a patient
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return row with: - city - province - nation - fiscal code of the patient -
	 *         "Residenza" - street - civic number - zip code
	 * @throws SQLException
	 */
	public Row getResindency(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		Row result = null;
		try {
			String sql = "SELECT cityname, provname, name, L.* FROM  " + SCHEMA_NAME + ".patientlive AS L "
					+ "NATURAL JOIN " + SCHEMA_NAME + ".city AS C  " + "NATURAL JOIN " + SCHEMA_NAME
					+ ".province AS P  " + "NATURAL JOIN " + SCHEMA_NAME + ".nation AS N  "
					+ "WHERE ? = L.fiscalcode AND L.residencydomicile = 'Residenza';";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the row corresponding to the Primary Key given as parameter
	 *
	 * @param inputPKs
	 *            list of the Primary Keys
	 * @return the row if it has been founded, null otherwise
	 * @throws SQLException
	 */
	@Override
	public PatientRow getRow(ArrayList<String> inputPKs) throws SQLException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		PatientRow result = null;
		try {
			ArrayList<String> pksNames = getPrimaryKeyColumnNames();

			if (pksNames.size() != inputPKs.size())
				return null;

			String sql = "SELECT * FROM " + SCHEMA_NAME + "." + entityName + " WHERE ";
			for (int i = 0; i < pksNames.size() - 1; i++)
				sql += pksNames.get(i) + " = ? AND ";

			sql += pksNames.get(pksNames.size() - 1) + " = ?; ";

			pst = connection.prepareStatement(sql);
			for (int i = 0; i < inputPKs.size(); i++)
				pst.setString(i + 1, inputPKs.get(i));
			rs = pst.executeQuery();

			result = getPatientRowFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the list of all the doctor had by a patient
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the doctor row list
	 * @throws SQLException
	 */
	public ArrayList<DoctorRow> listDoctors(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<DoctorRow> result = null;
		try {
			String sql = "SELECT D.*, Reg.startdate, Reg.enddate, Reg.timenumber FROM  " + SCHEMA_NAME
					+ ".register AS Reg " + "INNER JOIN " + SCHEMA_NAME
					+ ".doctor AS D ON D.fiscalcode = Reg.fiscalcode " + "WHERE ? = Reg.fiscalcode2 ";
			/*
			 * if (filterDate) { /* what happen here is subtle: I want to considere endDate
			 * and startdate if they are not null, otherwise ignore them. So the possible
			 * scenarios are: - start and end NOT NULL: if the input interval intersect the
			 * stored one, return true - start NULL, end NOT NULL:
			 * 
			 * if (filterStartDate != null) sql +=
			 * " AND ( ? BETWEEN Reg.startdate AND Reg.enddate OR "; if (filterEndDate !=
			 * null) sql += " OR ? BETWEEN Reg.startdate AND Reg.enddate )"; }
			 */
			sql += ";";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getMultipleDoctorRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}
		return result;
	}

	/**
	 * Register a patient to a doctor IMPORTANT: this method let you set also the
	 * Primary Key of a register row, DO NOT do that, leave this operation to the
	 * Database triggers set that column as defaults --> true. (Unless you know
	 * exactly what you are doing)
	 * 
	 * @param row
	 *            the information that are going to be inserted
	 * @throws SQLException
	 */
	public void registerToDoctor(Row row) throws SQLException {
		try {
			Entity temp = new Entity(connection, "register");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Register a patient to a doctor, startDate is set to the current date, enddate
	 * is left null
	 *
	 * @param doctorfiscalCode
	 *            fiscal code of the doctor
	 * @param patientFiscalCode
	 *            fiscal code of the patient
	 * @param time
	 *            number of times that this patient has registered to this doctor
	 * @throws SQLException
	 */
	public void registerToDoctor(String doctorfiscalCode, String patientFiscalCode) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".register VALUES (?,?,DEFAULT, current_date , NULL );";

		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, doctorfiscalCode);
			pstmn.setString(2, patientFiscalCode);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}

	}

	/**
	 * Register a patient to a doctor It set also the time column, that is a
	 * Database job regurarly
	 *
	 * @param doctorfiscalCode
	 *            fiscal code of the doctor
	 * @param patientFiscalCode
	 *            fiscal code of the patient
	 * @param time
	 *            number of times that this patient has registered to this doctor
	 * @param startDate
	 *            starting date of the registration
	 * @param endDate
	 *            ending date of the registration
	 * @throws SQLException
	 */
	@Deprecated
	public void registerToDoctor(String doctorfiscalCode, String patientFiscalCode, java.sql.Date startDate,
			java.sql.Date endDate) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".register VALUES (?,?,DEFAULT,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, doctorfiscalCode);
			pstmn.setString(2, patientFiscalCode);
			pstmn.setDate(4, startDate);
			if (endDate == null)
				pstmn.setNull(5, java.sql.Types.DATE);
			else
				pstmn.setDate(4, endDate);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}

	}

	/**
	 * Retrieve the list of all the additional services of a patient If date filter
	 * is setted, it is applied to this query.
	 * 
	 * SQL Code:
	 * 
	 * SELECT Ex.*, D.name, D.surname, D.fiscalcode FROM fml.execute AS Ex NATURAL
	 * JOIN fml.doctor AS D WHERE ? = Ex.fiscalcode2 AND ? < Ex.exdate AND ? >
	 * Ex.exdate ORDER BY Ex.exdate DESC;
	 * 
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the additional services row list
	 * @throws SQLException
	 */
	public ArrayList<Row> searchAdditionalServices(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT Ex.*, D.name, D.surname, D.fiscalcode FROM " + SCHEMA_NAME + ".execute AS Ex "
					+ "NATURAL JOIN " + SCHEMA_NAME + ".doctor AS D " + "WHERE ? = Ex.fiscalcode2 ";
			if (filterDate) {
				if (filterStartDate != null)
					sql += " AND  ? < Ex.exdate ";
				if (filterEndDate != null)
					sql += " AND ? > Ex.exdate ";
			}
			sql += "ORDER BY Ex.exdate DESC; ";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);

			if (filterDate) {
				if (filterStartDate != null)
					pst.setDate(2, filterStartDate);
				if (filterEndDate != null)
					pst.setDate(3, filterEndDate);
			}

			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the list of all the Exam results of a patient. If date filter is
	 * setted, it is applied to this query.
	 * 
	 * SQL Code:
	 * 
	 * SELECT EX.* FROM fml.examresults AS EX WHERE ? = EX.fiscalcode AND ? <
	 * EX.examdate AND ? > EX.examdate ORDER BY EX.examdate DESC;
	 * 
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the exam result row list
	 * @throws SQLException
	 */
	public ArrayList<Row> searchExamsResults(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT EX.* FROM  " + SCHEMA_NAME + ".examresults AS EX " + "WHERE ? = EX.fiscalcode ";
			if (filterDate) {
				if (filterStartDate != null)
					sql += " AND  ? < EX.examdate ";
				if (filterEndDate != null)
					sql += " AND ? > EX.examdate ";
			}
			sql += "ORDER BY EX.examdate DESC; ";

			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);

			if (filterDate) {
				if (filterStartDate != null)
					pst.setDate(2, filterStartDate);
				if (filterEndDate != null)
					pst.setDate(3, filterEndDate);
			}

			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the list of all the pathologies of a patient If date filter is
	 * setted, it is applied to this query.
	 * 
	 * SQL Code:
	 * 
	 * SELECT * FROM fml.pathology As Pa NATURAL JOIN fml.acknowledge AS Ak NATURAL
	 * JOIN fml.visit AS V WHERE ? = V.fiscalcode AND ? < visittimestamp::date AND ?
	 * > visittimestamp::date ORDER BY visittimestamp DESC;
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the pathology row list
	 * @throws SQLException
	 */
	public ArrayList<Row> searchPathologies(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT * FROM  " + SCHEMA_NAME + ".pathology As Pa " + "NATURAL JOIN  " + SCHEMA_NAME
					+ ".acknowledge AS Ak " + "NATURAL JOIN  " + SCHEMA_NAME + ".visit AS V "
					+ "WHERE ? = V.fiscalcode ";

			if (filterDate) {
				if (filterStartDate != null)
					sql += " AND  ? < visittimestamp::date ";
				if (filterEndDate != null)
					sql += " AND ? >  visittimestamp::date ";
			}

			sql += "ORDER BY visittimestamp DESC;";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);

			if (filterDate) {
				if (filterStartDate != null)
					pst.setDate(2, filterStartDate);
				if (filterEndDate != null)
					pst.setDate(3, filterEndDate);
			}

			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Return the list of the patients' informations selected by name and/or
	 * surname. The Searching Scope is all the Database, for searching for a
	 * specific doctor see the homonym method in the class Doctor. SQL code:
	 * 
	 * SELECT * FROM fml.patient WHERE UPPER(name || ' ' || surname) LIKE UPPER(?)
	 * AND UPPER(name || ' ' || surname) LIKE UPPER(?) ORDER BY name ASC;
	 *
	 *
	 * @param nameAndSurname
	 *            contain the name and surname separated by a space, note that both
	 *            name and surname can contain multiple world inside them.
	 * @return the patients PatientRow list
	 * @throws SQLException
	 */
	public ArrayList<PatientRow> searchPatientByName(String nameAndSurname) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<PatientRow> result = null;
		try {
			String[] splits = nameAndSurname.split(" ");
			String sql = "SELECT * FROM " + SCHEMA_NAME + ".patient WHERE UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			for (int i = 0; i < splits.length - 1; i++)
				sql += "AND  UPPER(name || ' ' || surname) LIKE UPPER(?) ";
			sql += " ORDER BY name ASC;";

			pst = connection.prepareStatement(sql);
			for (int i = 0; i < splits.length; i++)
				pst.setString(i + 1, "%" + splits[i] + "%");
			rs = pst.executeQuery();
			result = getMultiplePatientRowsFromQuery(rs);

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Search for all the prescription of a patient, and list a brief description
	 * for each one. If date filter is setted, it is applied to this query.
	 * 
	 * SQL code: WITH sub AS ( SELECT Pr.*, V.visittimestamp, D.name, D.surname,
	 * D.fiscalcode FROM fml.visit AS V INNER JOIN fml.prescription AS Pr ON
	 * Pr.visitcounterid = V.counterid INNER JOIN fml.doctor AS D ON D.fiscalcode =
	 * V.fiscalcode2 WHERE ? = V.fiscalcode ) SELECT * FROM (SELECT *, NULL AS
	 * description, 'Custom' AS type, NULL AS id FROM sub UNION SELECT sub.*,
	 * cvp.descriptionNtr, 'cvp', cvp.cvp FROM sub NATURAL JOIN fml.indicate2 AS I2
	 * NATURAL JOIN fml.cvp UNION SELECT sub.*, cluster.description, 'cluster',
	 * cluster.code FROM sub NATURAL JOIN fml.indicate3 AS I3 NATURAL JOIN
	 * fml.cluster UNION SELECT sub.*, dayservice.description, 'dayservice',
	 * dayservice.code FROM sub NATURAL JOIN fml.indicate4 AS I4 NATURAL JOIN
	 * fml.dayservice UNION SELECT sub.*, medicine.namepack, 'medicine',
	 * medicine.aic FROM sub NATURAL JOIN fml.indicate1 AS I1 NATURAL JOIN
	 * fml.medicine ORDER BY visittimestamp DESC ) AS U WHERE ? <
	 * U.visittimestamp::date AND ? > U.visittimestamp::date;
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @return the list of prescription information rows
	 * @throws SQLException
	 */
	public ArrayList<Row> searchPrescriptions(String fiscalCode) throws SQLException {
		String sql = "WITH sub AS ( " + "SELECT Pr.*, V.visittimestamp, D.name, D.surname, D.fiscalcode FROM "
				+ SCHEMA_NAME + ".visit AS V " + "INNER JOIN " + SCHEMA_NAME
				+ ".prescription AS Pr ON Pr.visitcounterid = V.counterid " + "INNER JOIN " + SCHEMA_NAME
				+ ".doctor AS D ON D.fiscalcode = V.fiscalcode2 " + "WHERE ? = V.fiscalcode " + ") "
				+ "SELECT * FROM (SELECT *, NULL AS description, 'Custom' AS type, NULL AS id FROM sub " + "UNION "
				+ "SELECT sub.*, cvp.descriptionNtr, 'cvp', cvp.cvp " + "FROM sub NATURAL JOIN " + SCHEMA_NAME
				+ ".indicate2 AS I2 NATURAL JOIN " + SCHEMA_NAME + ".cvp " + "UNION "
				+ "SELECT sub.*, cluster.description, 'cluster', cluster.code " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate3 AS I3 " + "NATURAL JOIN " + SCHEMA_NAME + ".cluster " + "UNION "
				+ "SELECT sub.*, dayservice.description, 'dayservice', dayservice.code " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate4 AS I4 " + "NATURAL JOIN " + SCHEMA_NAME + ".dayservice " + "UNION "
				+ "SELECT sub.*, medicine.namepack, 'medicine', medicine.aic " + "FROM sub " + "NATURAL JOIN "
				+ SCHEMA_NAME + ".indicate1 AS I1 " + "NATURAL JOIN " + SCHEMA_NAME + ".medicine "
				+ "ORDER BY visittimestamp DESC ) AS U ";

		if (filterDate) {
			if (filterStartDate != null)
				sql += " WHERE  ? < U.visittimestamp::date ";
			if (filterEndDate != null)
				sql += " AND ? >  U.visittimestamp::date;";
		}

		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);

			if (filterDate) {
				if (filterStartDate != null)
					pst.setDate(2, filterStartDate);
				if (filterEndDate != null)
					pst.setDate(3, filterEndDate);
			}

			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the list of all the relatives (other patients) of a patient
	 *
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the relatives row list
	 * @throws SQLException
	 */
	public ArrayList<Row> searchRelatives(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT R.*, ROf.startdate, ROf.enddate, ROf.type FROM  " + SCHEMA_NAME + ".relativeof AS ROf "
					+ "INNER JOIN " + SCHEMA_NAME + ".patient AS R ON R.fiscalcode = ROf.fiscalcode2 "
					+ "WHERE ? = ROf.fiscalcode; ";
			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);
			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Retrieve the list of all the visit of a patient If date filter is setted, it
	 * is applied to this query.
	 * 
	 * SQL Code:
	 * 
	 * SELECT V.*, D.name, D.surname FROM fml.visit As V INNER JOIN fml.doctor AS D
	 * ON V.fiscalcode2 = D.fiscalcode WHERE ? = V.fiscalcode AND ? <
	 * V.visittimestamp::date AND ? > V.visittimestamp::date ORDER BY
	 * V.visittimestamp DESC;
	 * 
	 * @param fiscalCode
	 *            the fiscal code of the patient
	 * @return the visit row list
	 * @throws SQLException
	 */
	public ArrayList<Row> searchVisits(String fiscalCode) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pst = null;
		ArrayList<Row> result = null;
		try {
			String sql = "SELECT V.*, D.name, D.surname FROM " + SCHEMA_NAME + ".visit As V " + "INNER JOIN "
					+ SCHEMA_NAME + ".doctor AS D ON V.fiscalcode2 = D.fiscalcode " + "WHERE ? = V.fiscalcode ";

			if (filterDate) {
				if (filterStartDate != null)
					sql += " AND  ? < V.visittimestamp::date ";
				if (filterEndDate != null)
					sql += " AND ? >  V.visittimestamp::date ";
			}

			sql += "ORDER BY V.visittimestamp DESC;";

			pst = connection.prepareStatement(sql);
			pst.setString(1, fiscalCode);

			if (filterDate) {
				if (filterStartDate != null)
					pst.setDate(2, filterStartDate);
				if (filterEndDate != null)
					pst.setDate(3, filterEndDate);
			}

			rs = pst.executeQuery();
			result = getMultipleRowsFromQuery(rs);
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				rs = null;
				pst = null;
			}
		}

		return result;
	}

	/**
	 * Set the domicile fo a patient, all the information are stored in the row
	 * parameter
	 *
	 * @param row
	 *            contain all the information that are goig to be inserted
	 * @throws SQLException
	 */
	public void setDomicile(Row row) throws SQLException {
		row.set(3, "Domicilio");
		try {
			Entity temp = new Entity(connection, "patientlive");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Set the domicile fo a patient
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param place
	 *            city, province and nation of the domicile
	 * @param street
	 *            street address of the patient
	 * @param civicNumber
	 *            civic number
	 * @param cap
	 *            zip code of the domicile
	 * @throws SQLException
	 */
	public void setDomicile(String fiscalCode, Place place, String street, String civicNumber, String cap)
			throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".patientlive VALUES (?,?,?,?,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, place.getCity());
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, "Domicilio");
			pstmn.setString(4, street);
			pstmn.setString(5, civicNumber);
			pstmn.setString(6, cap);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Set the place of birth of a patient, all the information are stored in the
	 * row parameter
	 *
	 * @param row
	 *            contain all the information that are goig to be inserted
	 * @throws SQLException
	 */
	public void setPoB(Row row) throws SQLException {
		try {
			Entity temp = new Entity(connection, "born");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Set the place of birth of a patient
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param place
	 *            place of birth of the patient
	 * @throws SQLException
	 */
	public void setPoB(String fiscalCode, Place place) throws SQLException {

		String SQL = "INSERT INTO " + SCHEMA_NAME + ".born VALUES (?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, fiscalCode);
			pstmn.setString(2, place.getCity());

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Set a relationship between two patient
	 *
	 * @param row
	 *            information that are going to be inserted
	 * @throws SQLException
	 */
	public void setRelative(Row row) throws SQLException {
		try {
			Entity temp = new Entity(connection, "relativeof");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Set a relationship between two patient
	 *
	 * @param fiscalCode
	 *            fiscal code of a patient
	 * @param fiscalCode2
	 *            fiscal code of a doctor
	 * @param startDate
	 *            starting date of the relationship
	 * @param endDate
	 *            ending date of the relationship
	 * @param type
	 *            the kind of relation
	 * @throws SQLException
	 */
	public void setRelative(String fiscalCode, String fiscalCode2, java.sql.Date startDate, java.sql.Date endDate,
			String type) throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".relativeof VALUES (?,?,?,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, fiscalCode);
			pstmn.setString(2, fiscalCode2);
			pstmn.setDate(3, startDate);
			pstmn.setDate(4, endDate);
			pstmn.setString(5, type);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

	/**
	 * Set the residency fo a patient, all the information are stored in the row
	 * parameter
	 *
	 * @param row
	 *            contain all the information that are goig to be inserted
	 * @throws SQLException
	 */
	public void setResindecy(Row row) throws SQLException {
		row.set(3, "Residenza");
		try {
			Entity temp = new Entity(connection, "patientlive");
			temp.createGenericInstance(row);
		} catch (SQLInjectionException e) {
			// It never should can happen
		}
	}

	/**
	 * Set the residency fo a patient
	 *
	 * @param fiscalCode
	 *            fiscal code of the patient
	 * @param place
	 *            city, province and nation of the residency
	 * @param street
	 *            street address of the patient
	 * @param civicNumber
	 *            civic number
	 * @param cap
	 *            zip code of the residency
	 * @throws SQLException
	 */
	public void setResindecy(String fiscalCode, Place place, String street, String civicNumber, String cap)
			throws SQLException {
		String SQL = "INSERT INTO " + SCHEMA_NAME + ".patientlive VALUES (?,?,?,?,?,?);";
		PreparedStatement pstmn = null;
		try {
			pstmn = connection.prepareStatement(SQL);
			pstmn.setString(1, place.getCity());
			pstmn.setString(2, fiscalCode);
			pstmn.setString(3, "Residenza");
			pstmn.setString(4, street);
			pstmn.setString(5, civicNumber);
			pstmn.setString(6, cap);

			pstmn.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (pstmn != null)
					pstmn.close();
			} catch (SQLException e) {
				throw e;
			} finally {
				pstmn = null;
			}
		}
	}

}

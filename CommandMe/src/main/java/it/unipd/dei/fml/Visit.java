package it.unipd.dei.fml;

import java.util.ArrayList;

/**
 * Uso un singleton per rendere più facile la comunicazione tra le classi. Ho
 * bisogno solo di un'istanza alla volta di Visit, in questo modo ho per forza
 * solo una istanza e inoltre posso ottenerla senza necessariamente passarla
 * come parametro. Per creare una nuova istanza è necessario chiamare prima
 * dismissIstance()
 */
public class Visit {

	public class ExamResult {
		public boolean present;
		public String notes;
		public String filePath;
		public java.sql.Date date;

	}

	public class Measure {
		public String descriptor;
		public double value;

	}

	public class Pathology {
		public String icd9Code;
		public String description;

	}

	public class Prescription {
		public boolean present;
		public String notes;
		public String color;
		public String priority;
		public ArrayList<String> cvp = new ArrayList<>(0);
		public ArrayList<String> atc = new ArrayList<>(0);
	}

	private static Visit istance = null;

	public static void dismissIstance() {
		istance = null;
	}

	public static Visit getIstance() {
		if (istance == null)
			istance = new Visit();
		return istance;
	}

	public String notes;

	public java.sql.Timestamp timestamp;

	public ExamResult exam;

	public Prescription prescription;

	public ArrayList<Measure> measure = new ArrayList<>(0);

	public ArrayList<Pathology> pathology = new ArrayList<>(0);

	private Visit() {
		prescription = new Prescription();
		exam = new ExamResult();
	}

}
